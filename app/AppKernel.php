<?php

use Symfony\Component\HttpKernel\Kernel;
use Symfony\Component\Config\Loader\LoaderInterface;

class AppKernel extends Kernel
{

    public function registerBundles()
    {
        $bundles = array(
            new Symfony\Bundle\FrameworkBundle\FrameworkBundle(),
            new Symfony\Bundle\SecurityBundle\SecurityBundle(),
            new Symfony\Bundle\TwigBundle\TwigBundle(),
            new Symfony\Bundle\MonologBundle\MonologBundle(),
            new Symfony\Bundle\SwiftmailerBundle\SwiftmailerBundle(),
            new Symfony\Bundle\AsseticBundle\AsseticBundle(),
            new Doctrine\Bundle\DoctrineBundle\DoctrineBundle(),
            new Sensio\Bundle\FrameworkExtraBundle\SensioFrameworkExtraBundle(),
            new Mopa\Bundle\BootstrapBundle\MopaBootstrapBundle(),
            new Knp\Bundle\MenuBundle\KnpMenuBundle(),
            new HWI\Bundle\OAuthBundle\HWIOAuthBundle(),
            new Stof\DoctrineExtensionsBundle\StofDoctrineExtensionsBundle(),
          //  new Knp\Bundle\MarkdownBundle\KnpMarkdownBundle(),
            new Knp\Bundle\GaufretteBundle\KnpGaufretteBundle(),
            new Liip\ImagineBundle\LiipImagineBundle(),
            new Stfalcon\Bundle\TinymceBundle\StfalconTinymceBundle(),
           // new A2lix\TranslationFormBundle\A2lixTranslationFormBundle(),
            new Knp\Bundle\PaginatorBundle\KnpPaginatorBundle(),
            //  new Gremo\BuzzBundle\GremoBuzzBundle(),
            new FOS\JsRoutingBundle\FOSJsRoutingBundle(),
            new JMS\SerializerBundle\JMSSerializerBundle(),
            new Gregwar\CaptchaBundle\GregwarCaptchaBundle(),
            // API
     //       new FOS\RestBundle\FOSRestBundle(),
      //      new FOS\OAuthServerBundle\FOSOAuthServerBundle(),
       //     new Nelmio\ApiDocBundle\NelmioApiDocBundle(),

            // Vsw-system bundles
            new VswSystem\CoreBundle\VswSystemCoreBundle(),
            new VswSystem\CmsBundle\VswSystemCmsBundle(),
            new VswSystem\SecurityBundle\VswSystemSecurityBundle(),
            new VswSystem\AdminBundle\VswSystemAdminBundle(),
            new VswSystem\WidgetBundle\VswSystemWidgetBundle(),
        //    new VswSystem\ApiBundle\VswSystemApiBundle(),
         //   new VswSystem\OAuthBundle\VswSystemOAuthBundle(),
          //  new VswSystem\CampaignBundle\VswSystemCampaignBundle(),
            new VswSystem\CatalogBundle\VswSystemCatalogBundle(),

            new FM\ElfinderBundle\FMElfinderBundle(),
        );

        if (in_array($this->getEnvironment(), array('dev', 'test'))) {
            $bundles[] = new Symfony\Bundle\WebProfilerBundle\WebProfilerBundle();
            $bundles[] = new Sensio\Bundle\DistributionBundle\SensioDistributionBundle();
            $bundles[] = new Sensio\Bundle\GeneratorBundle\SensioGeneratorBundle();
        }

        return $bundles;
    }

    public function registerContainerConfiguration(LoaderInterface $loader)
    {
        $loader->load(__DIR__ . '/config/config_' . $this->getEnvironment() . '.yml');
    }

}
