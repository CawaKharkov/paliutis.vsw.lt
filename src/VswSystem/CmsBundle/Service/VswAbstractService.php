<?php

namespace VswSystem\CmsBundle\Service;


use VswSystem\CmsBundle\Service\Interfaces\VswServiceInterface;

abstract class VswAbstractService implements VswServiceInterface
{

    protected $cacheObject;
    protected $manager;


    /**
     * @param mixed $cacheObject
     */
    public function setCacheObject($cacheObject)
    {
        $this->cacheObject = $cacheObject;
    }

    /**
     * @return mixed
     */
    public function getCacheObject()
    {
        return $this->cacheObject;
    }

    /**
     * @param mixed $manager
     */
    public function setManager($manager)
    {
        $this->manager = $manager;
    }

    /**
     * @return mixed
     */
    public function getManager()
    {
        return $this->manager;
    }




} 