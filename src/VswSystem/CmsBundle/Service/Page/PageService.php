<?php


namespace VswSystem\CmsBundle\Service\Page;


use Doctrine\Common\Persistence\ObjectRepository;
use Symfony\Component\PropertyAccess\Exception\InvalidArgumentException;
use VswSystem\CmsBundle\Service\VswAbstractService;

class PageService extends VswAbstractService
{

    protected $pageRepository;

    public function __construct(ObjectRepository $pageRepository)
    {
        $this->pageRepository = $pageRepository;
    }

    /**
     * @return \Doctrine\Common\Persistence\ObjectRepository
     */
    public function getPageRepository()
    {
        return $this->pageRepository;
    }


    public function getPageByAlias($alias = null)
    {
        if (!$alias) {
            throw new InvalidArgumentException('Alias cannot be null');
        }

        return $this->getPageRepository()->findByAlias($alias);
    }

}