<?php

namespace VswSystem\CmsBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use VswSystem\AdminBundle\Controller\AbstractController as AdminAbstractController;

class DefaultController extends AdminAbstractController
{

    /**
     * @Route("/",name="homepage")
     * @Template()
     */
    public function indexAction()
    {
        $mainSlider = $this->getMediaManager()->getMainSlider();
        $events = $this->getContentManager()->getLastEvents(3);

        $widget = $this->getWidgetManager()->getHallWidget(null,'main');

        $line = $this->get('vswsystem.orm.service.widget.line')->find('name','main');
        $page = $this->get('vswsystem.orm.service.text_page')->getPageByAlias('apie_mus');
        $audit = $this->get('vswsystem.orm.service.text_page')->getPageByAlias('auditai');
        
        $bottomImage1 = $this->get('vswsystem.orm.service.layout_block')->find('name','bottom_image_1');
        $bottomImage2 = $this->get('vswsystem.orm.service.layout_block')->find('name','bottom_image_2');
        $bottomImage3 = $this->get('vswsystem.orm.service.layout_block')->find('name','bottom_image_3');

        $modal = $this->get('vswsystem.orm.service.layout_block')->find('name','modal_ad');
        //var_dump($modal );
        $citiesSettings =  $this->getSystemManager()->getSettingsGroupRepository()->findOneBy(['name' => 'cities']);

        return ['mainSlider'=> $mainSlider,'events' => $events,
            'widget' => $widget,'citiesSettings' => $citiesSettings,'line' =>$line,'widget' => $widget,
        'bottomImage1' => $bottomImage1,'bottomImage2' => $bottomImage2,'bottomImage3' => $bottomImage3,'modal' => $modal,'page' => $page, 'audit' => $audit];
    }


}
