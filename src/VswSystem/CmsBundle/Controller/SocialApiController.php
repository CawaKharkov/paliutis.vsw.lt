<?php

namespace VswSystem\CmsBundle\Controller;


use Facebook\FacebookRedirectLoginHelper;
use Facebook\FacebookRequest;
use Facebook\FacebookSession;
use Facebook\GraphUser;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;
use VswSystem\AdminBundle\Controller\AbstractController as AdminAbstractController;

/**
 * @Route("/socialApi")
 */
class SocialApiController extends AdminAbstractController
{

    /**
     * @Route("/lastestTweets",name="view_lastest_tweets")
     * @Template()
     */
    public function lastestTweetsAction(Request $request)
    {
        $consumerKey = 'RDSf2TLtCdrqsWVAQKnO701Np';
        $consumerSecret = 'WrAeiJFdgfCdEG3DwwdtLJ1n2K7l9nV5Ra5E9GdlLjMF35ZAZa';
        $accessToken = '249047509-k7qe5QBfOxO6Ki2Vmuo7KcyDB1lkccPaLOYM9Zbg';
        $accessTokenSecret = 'uUac9JXh4n22r2qCfwEIihwYFUqFhhpxw3E2pFAiSWrPU';

        $twitter = new \TwitterPhp\RestApi($consumerKey, $consumerSecret, $accessToken, $accessTokenSecret);

        /*
         * Connect as application
         * https://dev.twitter.com/docs/auth/application-only-auth
         */
        $connection = $twitter->connectAsApplication();

        /*
         * Collection of the most recent Tweets posted by the user indicated by the screen_name, without replies
         * https://dev.twitter.com/docs/api/1.1/get/statuses/user_timeline
         */
        $lastest = $connection->get('/statuses/user_timeline', array('screen_name' => 'CawaKharkov', 'exclude_replies' => 'true', 'count' => 2));

        return ['lastestTweets' => $lastest];

    }

    /**
     * @Route("/fbUser",name="gat_fb_user")
     * @Template()
     */
    public function getFbUserAction(Request $request)
    {
        FacebookSession::setDefaultApplication('607180716056430', '85a6f44d312ebc553b1ba8c23693d3e2');
        /*if($request->query->get('code') !== null ){
            $session = new FacebookSession($request->query->get('code'));

        }*/
        $helper = new FacebookRedirectLoginHelper('http://cawakharkov.mooo.com:81'.$this->generateUrl('gat_fb_user'));
        try {
            $session = $helper->getSessionFromRedirect();

        } catch(FacebookRequestException $ex) {
            // When Facebook returns an error
        } catch(\Exception $ex) {
            // When validation fails or other local issues
        }
        //die();
        if ($session) {
            $request = new FacebookRequest($session, 'GET', '/me');
            $response = $request->execute();
            $graphObject = $response->getGraphObject();
            var_dump($graphObject);
        }else{
            return $this->redirect($helper->getLoginUrl());
        }
        $me = (new FacebookRequest(
            $session, 'GET', '/me'
        ))->execute()->getGraphObject(GraphUser::className());
        die();
        return [];

    }


    /**
     * @Route("/shareButtons/{alias}/{text}",name="get_share_buttons")
     * @Template()
     */
    public function shareButtonsAction($alias, $text)
    {
        $url = 'http://cawakharkov.mooo.com:81/page/'.$alias;
        return ['url' => $url, 'text' => $text];
    }

    /**
     * @Route("/smallShareButtons/{alias}/{text}",name="get_share_buttons")
     * @Template()
     */
    public function smallShareButtonsAction($alias, $text)
    {
        $url = 'http://cawakharkov.mooo.com:81/page/'.$alias;
        return ['url' => $url, 'text' => $text];
    }
    /**
     * @Route("/postFb",name="post_fb")
     * @Template()
     */
    public function postFacebookAction(Request $request)
    {
        FacebookSession::setDefaultApplication('607180716056430', '85a6f44d312ebc553b1ba8c23693d3e2');

        /*if($request->query->get('code') !== null ){
            $session = new FacebookSession($request->query->get('code'));

        }*/
        $helper = new FacebookRedirectLoginHelper('http://cawakharkov.mooo.com:81'.$this->generateUrl('post_fb'));

        try {
            $session = $helper->getSessionFromRedirect();

        } catch(FacebookRequestException $ex) {
            // When Facebook returns an error
        } catch(\Exception $ex) {
            // When validation fails or other local issues
        }
        //die();
        if ($session) {
            $request = new FacebookRequest(
                $session,
                'POST',
                '/691492907591516/feed',
                array (
                    'message' => 'This is a test message',
                )
            );
            $response = $request->execute();
            $graphObject = $response->getGraphObject();
            var_dump($graphObject);
        }else{
            return $this->redirect($helper->getLoginUrl(['scope'=>'manage_pages, publish_actions']));
        }
        die();
        return [];
    }

}
