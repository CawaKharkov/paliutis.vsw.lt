<?php

namespace VswSystem\CmsBundle\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use VswSystem\AdminBundle\Controller\AbstractController as AdminAbstractController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use VswSystem\CmsBundle\Entity\ContentBlock;
use VswSystem\CmsBundle\Entity\Subscriber;
use VswSystem\CoreBundle\Captcha\ReCaptcha;

/**
 * @Route("/email")
 */
class EmailController extends AdminAbstractController
{
    /**
     * @Route("/addSubscribe/",name="add_subscribe")
     * @Template()
     */
    public function subscribeAction(Request $request)
    {

        $secret = '6Le73QATAAAAAFeX01-S6KUd5dj6E2SQn-J_w4Hn';
        var_dump($request->request->get('g-recaptcha-response', 'default value if bar does not exist'));
        $reCaptcha = new ReCaptcha($secret);
        $resp = $reCaptcha->verifyResponse(
            $_SERVER["REMOTE_ADDR"],
            $request->request->get('g-recaptcha-response', 'default value if bar does not exist')

        );
        var_dump($resp);
        die();
        $sb = new Subscriber();

        $name = $request->request->get('name', 'default value if bar does not exist');
        $email = $request->request->get('email', 'default value if bar does not exist');
        $message = $request->request->get('message', 'default value if bar does not exist');

        $sb->setName($name);
        $sb->setEmail($email);
        $sb->setMessage($message);

        $this->getUserManager()->save($sb, true);
        $this->sendEmail($email, $name);

        return new JsonResponse();


    }

    protected function sendEmail($email, $user)
    {
        $message = \Swift_Message::newInstance()
            ->setSubject('Subscriber Email')
            ->setFrom('uzsakymai@paliutis.lt')
            ->setContentType('text/html')
            ->setTo($email)
            ->setBody(
                $this->renderView(
                    'VswSystemCmsBundle:Email:email.html.twig',
                    array('user' => $user)
                )
            );
        $this->get('mailer')->send($message);
    }

    /**
     * @Route("/form/",name="subscribe_form")
     * Template()
     */
    public function formAction(Request $request)
    {
        $blockForm = $this->createForm('add_subscribe', new Subscriber(), ['show_legend' => false]);
        $blockForm->handleRequest($request);

        $valid = false;

        if ($request->getMethod() == 'POST') {
            if ($blockForm->isValid()) {
                $block = $this->getUserManager()->save($blockForm->getData(), true);
                $settings = $this->get('vswsystem.orm.system.manager')->getSettingsGroup('email_nottifications')->getSettings()->getValues();
                foreach($settings as $setting)
                {
                    $emails[] = $setting->getSettingValue();
                }
                $message = \Swift_Message::newInstance()
                    ->setSubject('Uzsklausimas is www')
                    ->setFrom($block->getEmail())//
                    ->setContentType('text/html')
                    //->setTo($this->get('vswsystem.orm.system.manager')->getSettingValue('email')['settingValue'])
                    ->setTo($emails)
                    ->setBody('<div>
                    User name:'.$block->getName().'<br/>
                    Message:'.$block->getMessage().'<br/>
                    Email:'.$block->getEmail().'<br/>
                </div>');

                $this->get('mailer')->send($message);

                //$this->sendEmail($this->get('vswsystem.orm.system.manager')->getSettingValue('email')['settingValue'],$block);

            } else {
                return JsonResponse::create(['success' => false, 'error' => 'Code'], 500);
            }
        }

        return $this->render('VswSystemCmsBundle:Email:form.html.twig',
            ['form' => $blockForm->createView()]);

    }

    /**
     * @Route("/test/{id}",name="test_email")
     * @ParamConverter("user", class="VswSystemCmsBundle:Subscriber")
     * Template()
     */
    public function testAction($user)
    {

        return $this->render('VswSystemCmsBundle:Email:email.html.twig', ['user' => $user]);
    }

}
