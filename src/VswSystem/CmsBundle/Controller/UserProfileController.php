<?php

namespace VswSystem\CmsBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\User\UserInterface;
use VswSystem\AdminBundle\Controller\AbstractController as AdminAbstractController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use VswSystem\SecurityBundle\Entity\User;
use VswSystem\SecurityBundle\Entity\UserProfile;

/**
 * @Route("/user/profile")
 */
class UserProfileController extends AdminAbstractController
{
    /**
     * @Route("/",name="view_my_profile")
     * @Security("is_authenticated()")
     * @Template()
     */
    public function viewMyProfileAction(Request $request)
    {
        $profile = $this->getUser()->getProfile();
        $avatarForm = $this->createForm('user_profile_avatar_edit',$profile);
        $profileForm = $this->createForm('user_profile_edit',$profile);

        $avatarForm->handleRequest($request);
        $profileForm->handleRequest($request);

        if ($avatarForm->isValid()) {
            $this->getUserManager()->save($avatarForm->getData(), true);
            return $this->redirect($this->generateUrl($request->get('_route'), $request->query->all()));
        }

        if ($profileForm->isValid()) {
            $this->getUserManager()->save($profileForm->getData(), true);
            return $this->redirect($this->generateUrl($request->get('_route'), $request->query->all()));
        }

        return ['profile' => $profile,'profileForm'=>$profileForm->createView(),'avatarForm' => $avatarForm->createView()];
    }


    /**
     * @Route("/{id}",name="view_user_profile")
     * @Security("is_fully_authenticated()")
     * @ParamConverter("user", class="VswSystemSecurityBundle:User")
     * @Template()
     */
    public function viewUserProfileAction(Request $request,User $user)
    {
        return ['profile' => $user->getProfile(),'username' => $user->getUsername()];
    }

}
