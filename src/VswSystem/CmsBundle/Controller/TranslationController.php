<?php

namespace VswSystem\CmsBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
/**
 * TranslationController
 * @Route("/lang")
 */
class TranslationController extends Controller
{
    /**
     * @Route("/en",name="lang_en")
     */
    public function enAction(Request $request)
    {
        $request->getSession()->set('Locale','en');
        return $this->redirect($request->headers->get('referer'));
    }


    /**
     * @Route("/lt",name="lang_lt")
     */
    public function ltAction(Request $request)
    {
        $request->getSession()->set('Locale','lt');
        return $this->redirect($request->headers->get('referer'));
    }
}
