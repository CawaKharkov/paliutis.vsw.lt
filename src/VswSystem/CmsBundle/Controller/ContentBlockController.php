<?php

namespace VswSystem\CmsBundle\Controller;

use VswSystem\AdminBundle\Controller\AbstractController as AdminAbstractController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;
use VswSystem\CmsBundle\Entity\ContentBlock;

/**
 * @Route("/content")
 */
class ContentBlockController extends AdminAbstractController
{
    /**
     * @Route("/{id}",name="view_content_block")
     * @ParamConverter("block", class="VswSystemCmsBundle:ContentBlock")
     * @Template()
     */
    public function viewContentBlockAction($block)
    {
        return ['block' => $block];
    }

    /**
     * @Route("/getBlocks",name="get_content_block_list")
     * @Template()
     */
    public function getContentBlocksAction()
    {
        $blocks = $this->get('vswsystem.orm.content.manager')->getContentBlocks();
        return ['blocks' => $blocks];
    }

    /**
     * @Route("/getContent/{id}",name="get_content_block")
     * ParamConverter("block", class="VswSystemCmsBundle:ContentBlock")
     * @Template()
     */
    public function getContentBlockAction($id)
    {
        $block = $this->get('vswsystem.orm.content.manager')->getContentBlock($id);
        return ['block' => $block];
    }


    /**
     * @Route("/getWideBlocks/{count}{type}",name="get_wide_blocks")
     * @Template()
     */
    public function getDoubleBlocksAction($count,$type)
    {
        $blocks = $this->getContentManager()->getWideBlocks($count,$type);
        return ['blocks' => $blocks];
    }



    /**
     * @Route("/getMainBlocks/{count}",name="get_main_blocks")
     * @Template()
     */
    public function geMainBlocksAction($count = 3)
    {
        $blocks = $this->getContentManager()->geMainBlocks($count);
        return ['blocks' => $blocks];
    }


}
