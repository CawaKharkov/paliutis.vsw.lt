<?php

namespace VswSystem\CmsBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;


abstract class AbstractController extends Controller
{

    /**
     * @return \VswSystem\CoreBundle\Page\Service\AliasService
     */
    public function getAliasService()
    {
        return $this->get('vswsystem.orm.service.alias');
    }

    /**
     * @return \VswSystem\CoreBundle\Page\Service\TextPageService
     */
    public function getTextPageService()
    {
        return $this->get('vswsystem.orm.service.text_page');
    }

    /**
     * @return \VswSystem\CoreBundle\Page\Service\GalleryPageService
     */
    public function getGalleryPageService()
    {
        return $this->get('vswsystem.orm.service.gallery_page');
    }

    /**
     * @return \VswSystem\CoreBundle\Page\Service\FaqPageService
     */
    public function getFaqPageService()
    {
        return $this->get('vswsystem.orm.service.faq_page');
    }

    /**
     * @return \VswSystem\CoreBundle\Page\Service\NewsPageService
     */
    public function getNewsPageService()
    {
        return $this->get('vswsystem.orm.service.news_page');
    }
}
