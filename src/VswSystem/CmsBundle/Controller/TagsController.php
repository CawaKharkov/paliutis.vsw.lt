<?php

namespace VswSystem\CmsBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use VswSystem\AdminBundle\Controller\AbstractController as AdminAbstractController;

/**
 * @Route("/tags")
 */
class TagsController extends AdminAbstractController
{
    /**
     * @Route("/{name}",name="tag_search")
     * @Template()
     */
    public function viewAction($name)
    {
        $events = $this->getContentManager()->getEventBlockRepository()->findByTagName($name);
        $blocks = $this->getContentManager()->getContentBlockRepository()->findByTagName($name);
        $textPages = $this->getPagesManager()->getTextPageRepository()->findByTagName($name);


        return ['tag' => $name, 'events' => $events, 'blocks' => $blocks, 'textPages' => $textPages];
    }

    /**
     * @Route("/getBlocks",name="get_content_block_list")
     * @Template()
     */
    public function getContentBlocksAction()
    {
        $blocks = $this->get('vswsystem.orm.content.manager')->getContentBlocks();
        return ['blocks' => $blocks];
    }

    /**
     * @Route("/getContent/{id}",name="get_content_block")
     * ParamConverter("block", class="VswSystemCmsBundle:ContentBlock")
     * @Template()
     */
    public function getContentBlockAction($id)
    {
        $block = $this->get('vswsystem.orm.content.manager')->getContentBlock($id);
        return ['block' => $block];
    }


    /**
     * @Route("/getWideBlocks/{count}{type}",name="get_wide_blocks")
     * @Template()
     */
    public function getDoubleBlocksAction($count, $type)
    {
        $blocks = $this->getContentManager()->getWideBlocks($count, $type);
        return ['blocks' => $blocks];
    }


    /**
     * @Route("/getMainBlocks/{count}",name="get_main_blocks")
     * @Template()
     */
    public function geMainBlocksAction($count = 3)
    {
        $blocks = $this->getContentManager()->geMainBlocks($count);
        return ['blocks' => $blocks];
    }


}
