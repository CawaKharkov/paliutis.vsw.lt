<?php

namespace VswSystem\CmsBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use VswSystem\AdminBundle\Controller\AbstractController as AdminAbstractController;
use VswSystem\CmsBundle\Entity\Donor;

/**
 * @Route("/messages")
 */
class MessageController extends AdminAbstractController
{
    /**
     * @Route("/",name="user_messages")
     * @Template()
     */
    public function indexAction(Request $request)
    {
        return [];
    }


}
