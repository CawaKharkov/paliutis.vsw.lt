<?php

namespace VswSystem\CmsBundle\Controller;

use VswSystem\AdminBundle\Controller\AbstractController as AdminAbstractController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;
use VswSystem\CmsBundle\Entity\ContentBlock;

/**
 * @Route("/event")
 */
class EventBlockController extends AdminAbstractController
{

    /**
     * @Route("/{id}",name="view_event_block")
     * @ParamConverter("block", class="VswSystemCmsBundle:EventBlock")
     * @Template()
     */
    public function viewEventBlockAction($block)
    {
        return ['block' => $block];
    }


    /**
     * @Route("/getLastEvents/{count}",name="get_last_events")
     * @Template()
     */
    public function getLastEventsAction($count)
    {
        $events = $this->getContentManager()->getLastEvents($count);
        return ['events' => $events];
    }


    /**
     * @Route("/mainPage/{count}",name="get_main_events")
     * @Template()
     */
    public function getMainEventsAction($count =3)
    {
        $events = $this->getContentManager()->getMainEvents($count);
        return ['events' => $events];
    }

}
