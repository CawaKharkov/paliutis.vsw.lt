<?php

namespace VswSystem\CmsBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use VswSystem\AdminBundle\Controller\AbstractController as AdminAbstractController;

/**
 * @Route("/gallery")
 */
class GalleryController extends AdminAbstractController
{

    /**
     * @Route("/{alias}",name="view_gallery")
     * @Template()
     */
    public function viewAction(Request $request, $alias)
    {
        $alisEntity = $this->getPagesManager()->getAliasRepsitory()->findOneBy(['alias' => $alias]);
        $gallery = $this->getMediaManager()->getGalleriesGroupByAlias($alisEntity);
        $qb = $this->getPagesManager()->getGalleryPageRepository()
            ->createQueryBuilder('page')
            ->andWhere(':gallery MEMBER OF page.galleries')
            ->setParameter('gallery', $gallery);
        $page= $qb->getQuery()->getSingleResult();

        return ['gallery' => $gallery,'page' =>$page];
    }


    /**
     * @Route("/image/{id}",name="view_gallery_image")
     * @ParamConverter("image", class="VswSystemCmsBundle:GalleryPageImage")
     * @Template()
     */
    public function viewImageAction(Request $request, $image)
    {
        return ['image' =>$image];
    }


}
