<?php

namespace VswSystem\CmsBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use VswSystem\AdminBundle\Controller\AbstractController as AdminAbstractController;
use VswSystem\CmsBundle\Entity\Donor;

/**
 * @Route("/donor")
 */
class DonorController extends AdminAbstractController
{
    /**
     * @Route("/register/",name="donor_register")
     */
    public function donorRegisterAction(Request $request)
    {
        $donor = new Donor();
        $donor->setName($_POST['name']);
        $donor->setCity($_POST['city']);
        $donor->setBloodGroup($_POST['bloodGroup']);
        $donor->setBloodRh($_POST['bloodRh']);
        $donor->setDonationTime(new \DateTime(date('Y-m-d', strtotime($_POST['date']))));
        $donor->setPhone($_POST['phone']);

        $this->getUserManager()->save($donor, true);

        return new JsonResponse();
    }

    /**
     * @Route("/registerFast/",name="donor_register_fast")
     */
    public function donorRegisterFastAction(Request $request)
    {

        $donor = new Donor();
        $donor->setCity($_POST['city']);
        $donor->setBloodGroup($_POST['bloodGroup']);
        $donor->setBloodRh($_POST['bloodRh']);
        $donor->setPhone($_POST['phoneNumber']);

        $this->getUserManager()->save($donor, true);

        return new JsonResponse();
    }

    /**
     * @Route("/registerSidebar/",name="donor_register_side")
     */
    public function asideRegistrationAction()
    {
        $donor = new Donor();
        $donor->setName($_POST['name']);
        $donor->setCity($_POST['city']);
        $donor->setPhone($_POST['phone']);

        $this->getUserManager()->save($donor, true);
        //
        $response = new Response();
        $response->setStatusCode(200);
        $now = new \DateTime();
        $response->headers->setCookie(new Cookie("asideregistration", $now->format('d-m-Y')));
        $response->sendHeaders();
        return $response;
    }

    /**
     * @Route("/success/",name="donor_register_success")
     * @Template()
     */
    public function registeredAction(Request $request)
    {
        $response = new Response();
        $now = new \DateTime();
        $response->headers->setCookie(new Cookie("asideregistration", $now->format('d-m-Y')));
        $response->sendHeaders();
        return [];
    }

}
