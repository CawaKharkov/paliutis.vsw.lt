<?php

namespace VswSystem\CmsBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;
use VswSystem\AdminBundle\Controller\AbstractController as AdminAbstractController;
use VswSystem\CmsBundle\Entity\TextContentPage;

/**
 * @Route("/settings")
 */
class SettingsController extends AdminAbstractController
{

    /**
     * @Route("/getSetting/{name}",name="get_setting")
     * @Template()
     */
    public function getSettingAction($name = null)
    {
        if(!$name){
            return [];
        }

        $settingValue = $this->get('vswsystem.orm.system.manager')->getSettingByName($name);
        return $settingValue;

    }


}
