<?php

namespace VswSystem\CmsBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use VswSystem\CmsBundle\Controller\AbstractController as CmsAbstractController;

/**
 * @Route("/page")
 */
class PageController extends CmsAbstractController
{

    /**
     * @Route("/{alias}",name="view_page")
     * @Template()
     */
    public function viewAction(Request $request, $alias)
    {
        $type = $this->getAliasService()->getType($alias);
        switch ($type) {
            case 'TextContentPage':
                $action = $this->forward('VswSystemCmsBundle:Page:textPage', ['alias' => $alias]);
                break;

            case 'GalleryContentPage':
                $action = $this->forward('VswSystemCmsBundle:Page:galleryPage', ['alias' => $alias]);
                break;

            case 'FaqContentPage':
                $action = $this->forward('VswSystemCmsBundle:Page:faqPage', ['alias' => $alias]);
                break;

            case 'NewsContentPage':
                $action = $this->forward('VswSystemCmsBundle:Page:newsPage', ['alias' => $alias]);
                break;

        }
       // return [];
        return $action;

    }

    /**
     * @Route("/content/{alias}" , name="get_page_content")
     * @Security("has_role('ROLE_ADMIN')")
     * @Template()
     */
    public function getContentAction(Request $request, $alias)
    {
        $page = $this->getPagesManager()->getPageByAlias($alias);

        $response = new \Symfony\Component\HttpFoundation\JsonResponse();

        if ($request->getMethod() == 'POST') {
            $newContent = $request->request->get('value');
            $page->setContent($newContent);

            $this->get('vswsystem.orm.content.manager')->save($page, true);
            $response->setData($this->container->get('markdown.parser')->transformMarkdown($newContent));
        } else {
            $response->setData($page->getContent());
        }

        return $response;
    }

    /**
     * @Route("/{alias}",name="view_text_page")
     * @Template()
     */
    public function textPageAction(Request $request, $alias)
    {
        $page = $this->getTextPageService()->getPageByAlias($alias);

        $this->isExists($page);

        $pollForm = null;
        $pollsStats = [];
        $total = 0;
        $percents = [];
        if ($page->getPoll()) {
            if (!$request->cookies->get('poll' . $page->getPoll()->getId())) {
                $pollForm = $this->createForm('widget_poll_vote', $page->getPoll(), ['show_legend' => false,
                    'action' => $this->generateUrl('widget_poll_vote')])->createView();
            } else {
                foreach ($page->getPoll()->getAnswers() as $answer) {
                    $total += $answer->getHits();
                    $pollsStats[$page->getPoll()->getId()][] = ($answer->getHits() !== null) ? $answer->getHits() : 0;
                }
            }


            foreach ($pollsStats as $index => $stat) {
                foreach ($stat as $st) {
                    $percents[$index][] = $this->percent($st, $total);
                }

            }
        }
        $redirect = $this->isCreated($page);

        return !$redirect ?
            ['page' => $page, 'pollForm' => $pollForm,'percents' => $percents] :
            $redirect;

    }

    public function percent($num_amount, $num_total) {
        $count1 = $num_amount / $num_total;
        $count2 = $count1 * 100;
        $count = number_format($count2, 0);
        return $count;
    }

    /**
     * @Route("/{alias}",name="view_gallery_page")
     * @Template()
     */
    public function galleryPageAction(Request $request, $alias)
    {
        $page = $this->getGalleryPageService()->getPageByAlias($alias);
        $pageNumber = $request->query->get('page', 1);
        $pagination = null;

        $this->isExists($page);
        $redirect = $this->isCreated($page);

        return !$redirect ?
            ['page' => $page, 'pagination' => $pagination] :
            $redirect;

    }

    /**
     * @Route("/{alias}",name="view_faq_page")
     * @Template()
     */
    public function faqPageAction(Request $request, $alias)
    {
        $page = $this->getFaqPageService()->getPageByAlias($alias);
        $pageNumber = $request->query->get('page', 1);

        $this->isExists($page);

        $pagination = $this->get('vswsystem.pagination.faq')->getPagination($page->getQuestions(), $pageNumber);

        $pollForm = null;
        $pollsStats = [];
        $total = 0;
        $percents = [];
        if ($page->getPoll()) {
            if (!$request->cookies->get('poll' . $page->getPoll()->getId())) {
                $pollForm = $this->createForm('widget_poll_vote', $page->getPoll(), ['show_legend' => false,
                    'action' => $this->generateUrl('widget_poll_vote')])->createView();
            } else {
                foreach ($page->getPoll()->getAnswers() as $answer) {
                    $total += $answer->getHits();
                    $pollsStats[$page->getPoll()->getId()][] = ($answer->getHits() !== null) ? $answer->getHits() : 0;
                }
            }


            foreach ($pollsStats as $index => $stat) {
                foreach ($stat as $st) {
                    $percents[$index][] = $this->percent($st, $total);
                }

            }
        }
        $redirect = $this->isCreated($page);

        return !$redirect ?
            ['page' => $page, 'pollForm' => $pollForm,'percents' => $percents, 'pagination' => $pagination] :
            $redirect;

    }

    /**
     * @Route("/{alias}",name="view_news_page")
     * @Template()
     */
    public function newsPageAction(Request $request, $alias)
    {
        $page = $this->getNewsPageService()->getPageByAlias($alias);
        $pageNumber = $request->query->get('page', 1);
        $pagination = null;
        $this->isExists($page);

        if ($page->getPaginatable() !== null && count($page->getEvents()) >= 5) {
            $pagination = $this->get('vswsystem.pagination.faq')->getPagination($page->getEvents(), $pageNumber, 4);
        }

        $redirect = $this->isCreated($page);
        return !$redirect ?
            ['page' => $page, 'pagination' => $pagination] :
            $redirect;

    }

    public function isCreated($page)
    {
        $canCreate = $this->get('security.context')->isGranted('ROLE_SUPER_ADMIN');

        if (!$page && $canCreate) {
            return $this->redirect($this->generateUrl('admin_pages'));
        }
    }

    public function isExists($page)
    {
        if (!$page) {
            throw $this->createNotFoundException("Page not found");
        }
    }

}
