<?php

namespace VswSystem\CmsBundle\Controller;

use VswSystem\AdminBundle\Controller\AbstractController as AdminAbstractController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;
use VswSystem\CmsBundle\Entity\ContentBlock;

/**
 * @Route("/question")
 */
class QuestionController extends AdminAbstractController
{
    /**
     * @Route("/getQuestion/{id}",name="get_question")
     * @ParamConverter("block", class="VswSystemCmsBundle:QuestionContent")
     * @Template()
     */
    public function getQuestionAction($question)
    {
        return ['question' => $question];
    }

    /**
     * @Route("/getWideBlocks/{count}",name="get_last_questions")
     * @Template()
     */
    public function getLastQuestionsAction($count)
    {
        $questions = $this->getContentManager()->getLastQuestions($count);
        return ['questions' => $questions];
    }

}
