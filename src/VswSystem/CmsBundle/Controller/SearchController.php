<?php

namespace VswSystem\CmsBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use VswSystem\AdminBundle\Controller\AbstractController as AdminAbstractController;

class SearchController extends AdminAbstractController
{

    /**
     * @Route("/search",name="search")
     * @Template()
     */
    public function indexAction(Request $request)
    {
        $query = $request->get('search');
        $items = $this->get('vswsystem.orm.service.ctalog.item')
            ->getRepository()->createQueryBuilder('i')
            ->where('i.content LIKE :content or i.title LIKE :content')
            ->setParameter('content', '%'.$query.'%')
            ->getQuery()
            ->getResult();

        return ['items' => $items];
    }



}
