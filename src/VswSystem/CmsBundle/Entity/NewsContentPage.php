<?php

namespace VswSystem\CmsBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use VswSystem\CmsBundle\Entity\AbstractEntity\AbstractContentPage;
use VswSystem\CmsBundle\Entity\Traits\Paginatable;
use VswSystem\CmsBundle\Entity\Traits\PaginatableEntity;
use Gedmo\Mapping\Annotation as Gedmo;


/**
 * NewsContentPage
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="VswSystem\CmsBundle\Entity\Repository\NewsContentPageRepository")
 * @Gedmo\Loggable
 */
class NewsContentPage extends AbstractContentPage
{


    use PaginatableEntity;

    /**
     * @var EventBlock[]
     * @ORM\OrderBy({"eventTime" = "DESC"})
     * @ORM\ManyToMany(targetEntity="EventBlock", cascade={"persist"}, orphanRemoval=false)
     */
    protected $events;
    protected $eventsFromExisting;

    /**
     * @var integer
     *
     * @ORM\Column(name="position", type="smallint")
     */
    protected $questionLayout = 1;
    protected static $questionLayouts = [0 => 'LEFT_DATE', 1 => 'TOP',2=>'BOTTOM',4=>'LEFT_SMALL_IMAGE','5'=>'BLOCKS'];



    function __construct()
    {
        $this->events = new ArrayCollection();
        $this->eventsFromExisting = new ArrayCollection();
    }

    /**
     * @var integer
     *
     * @ORM\Column(name="showImages", type="boolean")
     */
    protected $showImages = 0;

    /**
     * @param EventBlock $event
     * @return $this
     */
    public function addEvent(EventBlock $event)
    {
        if (!$this->events->contains($event)) {
            $this->events->add($event);
        }
        return $this;
    }

    /**
     * @param EventBlock $event
     * @return $this
     */
    public function addEventsFromExisting(EventBlock $event)
    {
        if (!$this->events->contains($event)) {
            $this->events->add($event);
        }
        return $this;
    }

    public function removeEventsFromExisting(EventBlock $event)
    {
        $this->events->removeElement($event);
        return $this;
    }

    /**
     * Just empty function to avoid error in form
     */
    public function getEventsFromExisting()
    {
        // DUMMY
    }


    /**
     * @param EventBlock $question
     * @return $this
     */
    public function removeEvent(EventBlock $event)
    {
        $this->events->removeElement($event);
        return $this;
    }

    /**
     * Get all news\events
     * @return ArrayCollection
     */
    public function getEvents()
    {
        return $this->events;
    }

    /**
     * @param int $showImages
     */
    public function setShowImages($showImages)
    {
        $this->showImages = $showImages;
    }

    /**
     * @return int
     */
    public function getShowImages()
    {
        return (bool)$this->showImages;
    }

    /**
     * @param int $questionLayout
     */
    public function setQuestionLayout($questionLayout)
    {
        $this->questionLayout = $questionLayout;
    }

    /**
     * @return int
     */
    public function getQuestionLayout()
    {
        return $this->questionLayout;
    }

    /**
     * @param array $questionLayouts
     */
    public static function setQuestionLayouts($questionLayouts)
    {
        self::$questionLayouts = $questionLayouts;
    }

    /**
     * @return array
     */
    public static function getQuestionLayouts()
    {
        return self::$questionLayouts;
    }



}
