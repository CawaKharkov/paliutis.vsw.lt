<?php

namespace VswSystem\CmsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use VswSystem\CmsBundle\Entity\AbstractEntity\AbstractContent;
use VswSystem\CmsBundle\Entity\Traits\FbContentEntity;
use VswSystem\CmsBundle\Entity\Traits\MainPageBlockEntity;
use VswSystem\CmsBundle\Entity\Traits\MainPageEntity;
use VswSystem\CmsBundle\Entity\Traits\TaggedEntity;

/**
 * ContentBlock
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="VswSystem\CmsBundle\Entity\Repository\ContentBlockRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class ContentBlock extends AbstractContent
{

    use FbContentEntity;
    use MainPageEntity;
    Use MainPageBlockEntity;

    /**
     * @var string
     * @Gedmo\Translatable
     * @ORM\Column(name="shortText", type="string", length=255)
     */
    protected $shortText;


    /**
     * @var integer
     *
     * @ORM\Column(name="blockSize", type="smallint",nullable=true)
     */
    protected $blockSize;




    /**
     * Set shortText
     *
     * @param string $shortText
     * @return ContentBlock
     */
    public function setShortText($shortText)
    {
        $this->shortText = $shortText;

        return $this;
    }

    /**
     * Get shortText
     *
     * @return string
     */
    public function getShortText()
    {
        return $this->shortText;
    }

    /**
     * @param int $blockSize
     */
    public function setBlockSize($blockSize)
    {
        $this->blockSize = $blockSize;
    }

    /**
     * @return int
     */
    public function getBlockSize()
    {
        return $this->blockSize;
    }


}
