<?php

namespace VswSystem\CmsBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use VswSystem\CmsBundle\Entity\Traits\IdentificationalEntity;
use VswSystem\CmsBundle\Entity\Traits\NamedEntity;


// gedmo annotations

/**
 * Menu
 *
 * @ORM\Table(indexes={
 * @ORM\Index(name="tree_idx", columns={"rgt", "lft","root"}),
 * @ORM\Index(name="lft_idx", columns={"lft"})
 * })
 * @ORM\Entity(repositoryClass="VswSystem\CmsBundle\Entity\Repository\MenuRepository")
 * @Gedmo\Tree(type="nested")
 * @Gedmo\Loggable
 */
class Menu
{
    use IdentificationalEntity;
    //use NamedEntity;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     * @Assert\NotBlank
     * @Assert\Length(min="3")
     * @Gedmo\Versioned
     */
    protected $name;

    /**
     * @var integer
     *
     * @ORM\Column(name="type", type="smallint")
     */
    protected $type = 0;
    protected static $types = [0 => 'MAIN_MENU', 1 => 'SUB_MENU', 2 => 'SUB_MENU_CHILD'];

    /**
     * @var integer
     *
     * @ORM\Column(name="position", type="smallint")
     */
    protected $position = 4;
    protected static $positions = [0 => 'TOP', 1 => 'LEFT', 2 => 'RIGHT', 3 => 'FOOT',
        4 => 'DEFAULT'];


    /**
     * @ORM\ManyToMany(targetEntity="Navigation",cascade={"persist"})
     * @ORM\OrderBy({"position" = "ASC"})
     */
    protected $navigation;
    protected $navigationFromExisting;

    /**
     * @Gedmo\TreeLeft
     * @ORM\Column(name="lft", type="integer")
     */
    protected $lft;

    /**
     * @Gedmo\TreeLevel
     * @ORM\Column(name="lvl", type="integer")
     */
    protected $lvl;

    /**
     * @Gedmo\TreeRight
     * @ORM\Column(name="rgt", type="integer")
     */
    protected $rgt;

    /**
     * @Gedmo\TreeRoot
     * @ORM\Column(name="root", type="integer", nullable=true)
     */
    protected $root;

    /**
     * @Gedmo\TreeParent
     * @ORM\ManyToOne(targetEntity="Menu", inversedBy="children")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $parent;

    /**
     * @ORM\OneToMany(targetEntity="Menu", mappedBy="parent")
     * @ORM\OrderBy({"lft" = "ASC"})
     */
    protected $children;


    /**
     * @ORM\OneToOne(targetEntity="VswSystem\CmsBundle\Entity\Navigation")
     */
    protected $connectorElement;

       /**
     * Set type
     *
     * @param integer $type
     * @return Menu
     */
    public function setType($type)
    {

        if (!array_key_exists($type, self::$types)) {
            throw new \InvalidArgumentException('Theres no such type of menu - ' . $type);
        }
        $this->type = $type;
        return $this;
    }

    /**
     * Get type
     *
     * @return integer
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Get type
     *
     * @return integer
     */
    public function getTypeName()
    {
        return self::$types[$this->type];
    }

    /**
     * Get type
     *
     * @return integer
     */
    public static function getTypeList()
    {
        return self::$types;
    }

    /**
     * Set position
     *
     * @param integer $position
     * @return Menu
     */
    public function setPosition($position)
    {
        if (!in_array($position, self::$positions)) {
            throw new InvalidArgumentException('Theres no such positon of menu - ' . $type);
        }
        $this->position = $position;
        return $this;
    }

    /**
     * Get position
     *
     * @return integer
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * Get position name
     *
     * @return string
     */
    public function getPositionName()
    {
        return self::$positions[$this->position];
    }

    /**
     * Get position list
     *
     * @return array
     */
    public static function getPositionList()
    {
        return self::$positions;
    }


    public function __construct()
    {
        $this->navigation = new ArrayCollection();
        $this->navigationFromExisting = new ArrayCollection();
    }

    public function addNavigation(Navigation $navigation)
    {
        if (!$this->navigation->contains($navigation)) {
            $this->navigation->add($navigation);
        }
        return $this;
    }

    public function getNavigationFromExisting()
    {

    }

    public function addNavigationFromExisting(Navigation $navigation)
    {
        $this->navigation[] = $navigation;
        return $this;
    }

    public function removeNavigationFromExisting(Navigation $navigation)
    {
        var_dump($navigation);
    }

    public function removeNavigation(Navigation $navigation)
    {
        $this->navigation->removeElement($navigation);
        return $this;
    }

    public function getNavigation()
    {
        return $this->navigation;
    }

    public function setParent(Menu $parent = null)
    {
        $this->parent = $parent;
    }

    public function getParent()
    {
        return $this->parent;
    }

    public function __toString()
    {
        $prefix = "";
        for ($i = 2; $i <= $this->lvl; $i++) {
            $prefix .= "& nbsp;& nbsp;& nbsp;& nbsp;";
        }
        return $prefix . $this->name;
    }

    public function getLaveledName()
    {
        return (string)$this;
    }

    /**
     * @param mixed $connectorElement
     */
    public function setConnectorElement($connectorElement)
    {
        $this->connectorElement = $connectorElement;
    }

    /**
     * @return mixed
     */
    public function getConnectorElement()
    {
        return $this->connectorElement;
    }

    /**
     * @return mixed
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }


}
