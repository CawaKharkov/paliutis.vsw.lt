<?php

namespace VswSystem\CmsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use VswSystem\CmsBundle\Entity\Traits\IdentificationalEntity;
use VswSystem\CmsBundle\Entity\Traits\NamedEntity;
use VswSystem\CmsBundle\Entity\Traits\TitledEntity;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * GalleryImageGroup
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="VswSystem\CmsBundle\Entity\Repository\GalleryImageGroupRepository")
 */
class GalleryImageGroup
{

    use IdentificationalEntity;
    use NamedEntity;
    use TitledEntity;


    /**
     * @var ContentImage
     *
     * @ORM\OneToOne(targetEntity="ContentImage", cascade={"persist"})
     */
    protected  $cover;


    /**
     * @var string
     *
     * @ORM\OneToOne(targetEntity="Alias",cascade={"persist"})
     */
    protected $alias;

    /**
     * @var integer
     *
     * @ORM\Column(name="position", type="smallint")
     */
    protected $imagePosition = 1;
    protected static $imagePositions = [0 => 'TOP', 3 => 'BOTTOM'];


    /**
     * @ORM\ManyToMany(targetEntity="GalleryPageImage",cascade={"persist"}, orphanRemoval=true)
     */
    protected $images;



    public function __construct()
    {
        $this->images = new ArrayCollection();
    }

    public function addImage(PageImage $image)
    {
        $this->images[] = $image;
        return $this;
    }

    public function removeImage(PageImage $image)
    {
        $this->images->removeElement($image);
        return $this;
    }

    public function getImages()
    {
        return $this->images;
    }


    /**
     * @param int $imagePosition
     */
    public function setImagePosition($imagePosition)
    {
        $this->imagePosition = $imagePosition;
    }

    /**
     * @return int
     */
    public function getImagePosition()
    {
        return $this->imagePosition;
    }

    /**
     * @param array $imagePositions
     */
    public static function setImagePositions($imagePositions)
    {
        self::$imagePositions = $imagePositions;
    }

    /**
     * @return array
     */
    public static function getImagePositions()
    {
        return self::$imagePositions;
    }

    /**
     * @param \VswSystem\CmsBundle\Entity\ContentImage $cover
     */
    public function setCover($cover)
    {
        $this->cover = $cover;
    }

    /**
     * @return \VswSystem\CmsBundle\Entity\ContentImage
     */
    public function getCover()
    {
        return $this->cover;
    }

    /**
     * @param string $alias
     */
    public function setAlias($alias)
    {
        $this->alias = $alias;
    }

    /**
     * @return string
     */
    public function getAlias()
    {
        return $this->alias;
    }


}
