<?php

namespace VswSystem\CmsBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;
use VswSystem\CmsBundle\Entity\Traits\FindByTagName;

/**
 * Class AbstractRepository
 * Base class for all repos
 * @package VswSystem\CmsBundle\Entity\Repository
 */
abstract class AbstractRepository extends EntityRepository
{

    use FindByTagName;

    public function findAll()
    {
        $qb = $this->createQueryBuilder('a')
            ->select('a');

        return $qb->getQuery()->getResult();
    }

}
