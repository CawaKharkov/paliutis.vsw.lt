<?php

namespace VswSystem\CmsBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use VswSystem\CmsBundle\Entity\AbstractEntity\AbstractContentPage;
use Gedmo\Mapping\Annotation as Gedmo;


/**
 * Page
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="VswSystem\CmsBundle\Entity\Repository\GalleryPageRepository")
 * @ORM\HasLifecycleCallbacks()
 * @Gedmo\Loggable
 */
class GalleryContentPage extends AbstractContentPage
{

    /**
     * @ORM\ManyToMany(targetEntity="GalleryImageGroup",cascade={"persist"})
     */
    protected $galleries;


    public function __construct()
    {
        parent::__construct();
        $this->galleries = new ArrayCollection();

    }

    public function addGallery(GalleryImageGroup $gallery)
    {
        $this->galleries[] = $gallery;
        return $this;
    }

    public function removeGallery(GalleryImageGroup $gallery)
    {
        $this->galleries->removeElement($gallery);
        return $this;
    }

    public function getGalleries()
    {
        return $this->galleries;
    }

}
