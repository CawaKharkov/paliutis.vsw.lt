<?php

namespace VswSystem\CmsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use VswSystem\CmsBundle\Entity\AbstractEntity\File;
use VswSystem\CmsBundle\Entity\Traits\IdentificationalEntity;

/**
 * PageImage
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="VswSystem\CmsBundle\Entity\Repository\PageImageRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class PageImage extends File
{
    protected $uploadPath = 'uploads/images/pageImages';

}
