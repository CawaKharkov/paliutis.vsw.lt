<?php

namespace VswSystem\CmsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use VswSystem\CmsBundle\Entity\Traits\IdentificationalEntity;
use VswSystem\CmsBundle\Entity\Traits\NamedEntity;

/**
 * Block
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="VswSystem\CmsBundle\Entity\Repository\BlockRepository")
 */
class Block
{
    use IdentificationalEntity;
    use NamedEntity;

    /**
     * @var string
     *
     * @ORM\Column(name="comment", type="string", length=255)
     */
    private $comment;

    /**
     * @var integer
     *
     * @ORM\Column(name="type", type="smallint")
     */
    private $type;


    /**
     * Set comment
     *
     * @param string $comment
     * @return Block
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return string 
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set type
     *
     * @param integer $type
     * @return Block
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return integer 
     */
    public function getType()
    {
        return $this->type;
    }
}
