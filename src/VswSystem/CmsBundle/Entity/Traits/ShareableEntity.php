<?php
namespace VswSystem\CmsBundle\Entity\Traits;


/**
 * Class ShareableEntity
 * @package VswSystem\CmsBundle\Entity\Traits
 */
trait ShareableEntity
{
    /**
     * @var integer
     * @ORM\Column(name="shareable", type="boolean")
     */
    protected $shareable = null;

    /**
     * @param int $shareable
     */
    public function setShareable($shareable)
    {
        $this->shareable = $shareable;
    }

    /**
     * @return int
     */
    public function getShareable()
    {
        return $this->shareable;
    }



}