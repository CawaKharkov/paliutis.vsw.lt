<?php
namespace VswSystem\CmsBundle\Entity\Traits;


/**
 * Class FbContentEntity
 * @package VswSystem\CmsBundle\Entity\Traits
 */
trait FbContentEntity
{
    /**
     * @var integer
     * @ORM\Column(name="isPosted", type="boolean")
     */
    protected $isPosted = 1;

    /**
     * @param int $isPosted
     */
    public function setIsPosted($isPosted)
    {
        $this->isPosted = $isPosted;
    }

    /**
     * @return int
     */
    public function getIsPosted()
    {
        return $this->isPosted;
    }



}