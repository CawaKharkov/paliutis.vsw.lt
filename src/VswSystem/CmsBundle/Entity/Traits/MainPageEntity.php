<?php
/**
 * Created by PhpStorm.
 * User: cawa
 * Date: 23.09.14
 * Time: 10:39
 */

namespace VswSystem\CmsBundle\Entity\Traits;


trait MainPageEntity
{

    /**
     * @var integer
     * @ORM\Column(name="isMainPage", type="boolean")
     */
    protected $isMainPage;



    /**
     * @return int
     */
    public function getIsMainPage()
    {
        return $this->isMainPage;
    }

    /**
     * @param int $isMainPage
     */
    public function setIsMainPage($isMainPage)
    {
        $this->isMainPage = $isMainPage;
    }





} 