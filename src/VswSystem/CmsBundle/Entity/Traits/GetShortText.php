<?php
namespace VswSystem\CmsBundle\Entity\Traits;


/**
 * Class GetShortText
 * @package VswSystem\CmsBundle\Entity\Traits
 */
trait GetShortText
{
    public function getShortText($field,$length = 250)
    {
        return $this->truncate($this->{$field},$length);
    }


    public function truncate($str, $len)
    {
        $tail = max(0, $len - 10);
        $trunk = substr($str, 0, $tail);
        $trunk .= strrev(preg_replace('~^..+?[\s,:]\b|^...~', '...', strrev(substr($str, $tail, $len - $tail))));
        return $trunk;
    }
}