<?php
namespace VswSystem\CmsBundle\Entity\Traits;


/**
 * Class ShortTextEntity
 * @package VswSystem\CmsBundle\Entity\Traits
 */
trait ShortTextEntity
{

    /**
     * @var string
     *
     * @ORM\Column(name="shortText", type="string", length=255, nullable=true)
     */
    protected $shortText;
}