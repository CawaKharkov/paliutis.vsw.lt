<?php
namespace VswSystem\CmsBundle\Entity\Traits;

use Symfony\Component\Validator\Constraints as Assert;


/**
 * Class NamedEntity
 * @package VswSystem\CmsBundle\Entity\Traits
 */
trait NamedEntity
{
    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     * @Assert\NotBlank
     *
     */
    protected $name;


    /**
     * Set name
     *
     * @param string $name
     * @return File
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
}