<?php

namespace VswSystem\CmsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use VswSystem\CmsBundle\Entity\AbstractEntity\File;
use VswSystem\CmsBundle\Entity\Traits\IdentificationalEntity;
use VswSystem\CmsBundle\Entity\Traits\NamedEntity;

/**
 * SliderImage
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="VswSystem\CmsBundle\Entity\Repository\SliderImageRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class SliderImage extends File
{

    /**
     * @var string
     *
     * @ORM\Column(name="caption", type="string", length=255, nullable=true)
     */
    protected  $caption;

    /**
     * @var string
     *
     * @ORM\Column(name="text", type="string", length=255, nullable=true)
     */
    protected  $text;


    /**
     * Available text colors
     * @var array
     */
    public static $colors = ['D21F27' => 'RED', '373737'=>'GRAY', 'ffffff' => 'WHITE'];


    /**
     * @var string
     *
     * @ORM\Column(name="textColor", type="string", length=255)
     */
    private $textColor = 'fff';


    /**
     * @ORM\ManyToOne(targetEntity="VswSystem\CmsBundle\Entity\TextContentPage")
     */
    private $textPageLink;


    protected $uploadPath = 'uploads/images/slides';


    /**
     * @var string
     *
     * @ORM\Column(name="route", type="string", length=255,nullable=true)
     */
    private $route;


    /**
     * Set caption
     *
     * @param string $caption
     * @return SliderImage
     */
    public function setCaption($caption)
    {
        $this->caption = $caption;
        return $this;
    }

    /**
     * Get caption
     *
     * @return string
     */
    public function getCaption()
    {
        return $this->caption;
    }

    /**
     * Set text
     *
     * @param string $text
     * @return SliderImage
     */
    public function setText($text)
    {
        $this->text = $text;

        return $this;
    }

    /**
     * Get text
     *
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * @param string $textColor
     */
    public function setTextColor($textColor)
    {
        $this->textColor = $textColor;
    }

    /**
     * @return string
     */
    public function getTextColor()
    {
        return $this->textColor;
    }

    /**
     * @return array
     */
    public static function getColors()
    {
        return self::$colors;
    }

    /**
     * @param mixed $pageLink
     */
    public function setTextPageLink($pageLink)
    {
        $this->textPageLink = $pageLink;
    }

    /**
     * @return mixed
     */
    public function getTextPageLink()
    {
        return $this->textPageLink;
    }

    /**
     * @return string
     */
    public function getRoute()
    {
        return $this->route;
    }

    /**
     * @param string $route
     */
    public function setRoute($route)
    {
        $this->route = $route;
    }




}
