<?php

namespace VswSystem\CmsBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Translatable\Translatable;
use VswSystem\CmsBundle\Entity\AbstractEntity\AbstractContentPage;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Page
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="VswSystem\CmsBundle\Entity\Repository\TextPageRepository")
 * @ORM\HasLifecycleCallbacks()
 * @Gedmo\Loggable()
 *
 *
 */
class TextContentPage extends AbstractContentPage implements Translatable
{
    /**
     * @var string
     * @Gedmo\Translatable
     * @ORM\Column(name="content", type="text",nullable=true)
     */
    protected $content;

    /**
     * @var string
     * @ORM\Column(name="htmlContent", type="text",nullable=true)
     */
    protected $htmlContent;


    /**
     * @Gedmo\Locale
     * Used locale to override Translation listener`s locale
     * this is not a mapped field of entity metadata, just a simple property
     */
    protected $locale;

    /**
     * @ORM\ManyToMany(targetEntity="PageImage",cascade={"persist"})
     */
    protected $images;

    /**
     * @ORM\ManyToMany(targetEntity="ContentPdf",cascade={"persist"})
     */
    protected $pdf;
    protected $pdfFromExisting;


    /**
     * @var integer
     *
     * @ORM\Column(name="showImages", type="boolean")
     */
    protected $showImages;


    /**
     * @var integer
     *
     * @ORM\Column(name="position", type="smallint")
     */
    protected $imagePosition = 1;
    protected static $imagePositions = [0 => 'TOP', 3 => 'BOTTOM'];


    /**
     * Set content
     *
     * @param string $content
     * @return Page
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    public function __construct()
    {
        parent::__construct();
        $this->images = new ArrayCollection();
        $this->pdf = new ArrayCollection();

    }


    public function addImage(PageImage $image)
    {
        $this->images[] = $image;
        return $this;
    }

    public function removeImage(PageImage $image)
    {
        $this->images->removeElement($image);
        return $this;
    }

    public function getImages()
    {
        return $this->images;
    }

    public function addPdf(ContentPdf $pdfFile)
    {
        $this->pdf[] = $pdfFile;
        return $this;
    }

    public function removePdf(ContentPdf $pdfFile)
    {
        $this->pdf->removeElement($pdfFile);
        return $this;
    }

    public function getPdf()
    {
        return $this->pdf;
    }

    public function getPdfFromExisting()
    {

    }

    public function addPdfFromExisting(ContentPdf $pdfFile)
    {
        $this->pdf[] = $pdfFile;
        return $this;
    }

    public function removePdfFromExisting(ContentPdf $pdfFile)
    {
    }

    /**
     * @param int $imagePosition
     */
    public function setImagePosition($imagePosition)
    {
        $this->imagePosition = $imagePosition;
    }

    /**
     * @return int
     */
    public function getImagePosition()
    {
        return $this->imagePosition;
    }

    /**
     * @param array $imagePositions
     */
    public static function setImagePositions($imagePositions)
    {
        self::$imagePositions = $imagePositions;
    }

    /**
     * @return array
     */
    public static function getImagePositions()
    {
        return self::$imagePositions;
    }

    public function setTranslatableLocale($locale)
    {
        $this->locale = $locale;
    }


    /**
     * @param int $showImages
     */
    public function setShowImages($showImages)
    {
        $this->showImages = $showImages;
    }

    /**
     * @return int
     */
    public function getShowImages()
    {
        return $this->showImages;
    }

    /**
     * @param string $htmlContent
     */
    public function setHtmlContent($htmlContent)
    {
        $this->htmlContent = $htmlContent;
    }

    /**
     * @return string
     */
    public function getHtmlContent()
    {
        return $this->htmlContent;
    }



}
