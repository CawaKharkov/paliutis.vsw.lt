<?php

namespace VswSystem\CmsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use VswSystem\CmsBundle\Entity\Traits\IdentificationalEntity;
use VswSystem\CmsBundle\Entity\Traits\NamedEntity;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Donor
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="VswSystem\CmsBundle\Entity\Repository\DonorRepository")
 */
class Donor
{
    use IdentificationalEntity;
    use NamedEntity;
    use TimestampableEntity;

    /**
     * @var string
     *
     * @ORM\Column(name="phone", type="string", length=255)
     */
    protected $phone;

    /**
     * @var string
     *
     * @ORM\Column(name="city", type="string", length=255)
     */
    protected $city;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="donationTime", type="datetime",nullable=true)
     */
    protected $donationTime;


    /**
     * @var string
     *
     * @ORM\Column(name="bloodGroup", type="string", length=255,nullable=true)
     */
    protected $bloodGroup;
    /**
     * @var array http://en.wikipedia.org/wiki/Blood_type
     */
    public static $bloodGroups = [0 => '0(I)', 1 => 'A(II)', 2 => 'B(III)', 3 => 'AB(IV)', 9 => 'Nežinau'];

    /**
     * @var string
     *
     * @ORM\Column(name="bloodRh", type="string", length=255,nullable=true)
     */
    protected $bloodRh;
    public static $bloodRhs = [0 => 'Rh+', 1 => 'Rh-', 9 => 'Nežinau'];


    /**
     * Set phone
     *
     * @param string $phone
     * @return Donor
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set donationTime
     *
     * @param \DateTime $donationTime
     * @return Donor
     */
    public function setDonationTime($donationTime)
    {
        $this->donationTime = $donationTime;

        return $this;
    }

    /**
     * Get donationTime
     *
     * @return \DateTime
     */
    public function getDonationTime()
    {
        return $this->donationTime;
    }

    /**
     * @param string $bloodGroup
     */
    public function setBloodGroup($bloodGroup)
    {
        $this->bloodGroup = $bloodGroup;
    }

    /**
     * @return string
     */
    public function getBloodGroup()
    {
        return $this->bloodGroup;
    }

    /**
     * @param string $bloodRh
     */
    public function setBloodRh($bloodRh)
    {
        $this->bloodRh = $bloodRh;
    }

    /**
     * @return string
     */
    public function getBloodRh()
    {
        return $this->bloodRh;
    }

    /**
     * @param string $city
     */
    public function setCity($city)
    {
        $this->city = $city;
    }

    /**
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @return array
     */
    public static function getBloodGroups()
    {
        return self::$bloodGroups;
    }

    /**
     * @return array
     */
    public static function getBloodRhs()
    {
        return self::$bloodRhs;
    }


}
