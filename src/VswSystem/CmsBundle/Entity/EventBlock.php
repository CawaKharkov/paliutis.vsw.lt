<?php

namespace VswSystem\CmsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use VswSystem\CmsBundle\Entity\AbstractEntity\AbstractContent;
use VswSystem\CmsBundle\Entity\Traits\FbContentEntity;
use VswSystem\CmsBundle\Entity\Traits\GetShortText;
use VswSystem\CmsBundle\Entity\Traits\MainPageEntity;

/**
 * EventBlock
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="VswSystem\CmsBundle\Entity\Repository\EventBlockRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class EventBlock extends AbstractContent
{

    use GetShortText;
    use FbContentEntity;
    use MainPageEntity;



    /**
     * @var string
     * @ORM\Column(name="title", type="string", length=255,nullable=true)
     */
    protected $title;

    /**
     * @var string
     * @ORM\Column(name="content", type="text")
     */
    protected $content;


    /**
     * @ORM\Column(type="datetime",nullable=true)
     */
    protected $eventTime = null;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $showEventDate = null;

    /**
     * @return mixed
     */
    public function getEventTime()
    {
        return $this->eventTime;
    }

    /**
     * @param mixed $eventTime
     */
    public function setEventTime($eventTime)
    {

        $this->eventTime = $eventTime;
    }

    /**
     * @return mixed
     */
    public function getShowEventDate()
    {
        return $this->showEventDate;
    }

    /**
     * @param mixed $showEventDate
     */
    public function setShowEventDate($showEventDate)
    {
        $this->showEventDate = $showEventDate;
    }


}
