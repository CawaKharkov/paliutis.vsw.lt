<?php

namespace VswSystem\CmsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use VswSystem\CmsBundle\Entity\AbstractEntity\File;
/**
 * ContentImage
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="VswSystem\CmsBundle\Entity\Repository\ContentImageRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class ContentImage extends File
{
    protected $uploadPath = 'uploads/images/contentImages';
}
