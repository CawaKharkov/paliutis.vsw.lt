<?php

namespace VswSystem\CmsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Translatable\Entity\MappedSuperclass\AbstractPersonalTranslation;
use VswSystem\CmsBundle\Entity\TextContentPage;

/**
* Entity\Translation\ProductTranslation.php

* @ORM\Entity
* @ORM\Table(name="text_page_translations",
*   uniqueConstraints={@ORM\UniqueConstraint(name="lookup_unique_idx", columns={
*     "locale", "object_id", "field"
*   })}
* )
*/
class TextContentPageTranslation extends AbstractPersonalTranslation
{
/**
* @ORM\ManyToOne(targetEntity="TextContentPage", inversedBy="translations")
* @ORM\JoinColumn(name="object_id", referencedColumnName="id", onDelete="CASCADE")
*/
protected $object;
}