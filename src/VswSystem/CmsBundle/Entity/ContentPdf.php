<?php

namespace VswSystem\CmsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use VswSystem\CmsBundle\Entity\AbstractEntity\File;
/**
 * ContentPdf
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="VswSystem\CmsBundle\Entity\Repository\ContentPdfRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class ContentPdf extends File
{
    protected $uploadPath = 'uploads/images/contentPdf';
}
