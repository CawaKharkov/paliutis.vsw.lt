<?php

namespace VswSystem\CmsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use VswSystem\CmsBundle\Entity\SliderImage;
use Doctrine\Common\Collections\ArrayCollection;
use VswSystem\CmsBundle\Entity\Traits\IdentificationalEntity;
use VswSystem\CmsBundle\Entity\Traits\NamedEntity;

/**
 * Slider
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="VswSystem\CmsBundle\Entity\Repository\SliderRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Slider
{
    use IdentificationalEntity;
    use NamedEntity;

    /**
     * @var SliderImage
     * @ORM\ManyToMany(targetEntity="SliderImage", cascade={"persist"}, orphanRemoval=true)
     * 
     */
    protected  $slides;

    public function __construct()
    {
        $this->slides = new ArrayCollection();
    }


    /**
     * Add slide
     * @param \VswSystem\CmsBundle\Entity\SliderImage $slide
     * @return \VswSystem\CmsBundle\Entity\Slider
     */
    public function addSlide(SliderImage $slide)
    {
        if (!$this->slides->contains($slide)) {

            $this->slides->add($slide);
        }
        return $this;
    }

    /**
     * Remove the slide
     * @param \VswSystem\CmsBundle\Entity\SliderImage $slide
     * @return \VswSystem\CmsBundle\Entity\Slider
     */
    public function removeSlide(SliderImage $slide)
    {
        $this->slides->removeElement($slide);
        return $this;
    }
    
    /**
     * Get all slides
     * @return ArrayCollection
     */
    public function getSlides()
    {
        return $this->slides;
    }

}
