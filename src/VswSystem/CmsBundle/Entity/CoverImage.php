<?php

namespace VswSystem\CmsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use VswSystem\CmsBundle\Entity\AbstractEntity\File;
/**
 * CoverImage
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="VswSystem\CmsBundle\Entity\Repository\CoverImageRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class CoverImage extends File
{
     protected $uploadPath = 'uploads/images/cover';
}
