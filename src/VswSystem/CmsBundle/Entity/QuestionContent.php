<?php

namespace VswSystem\CmsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use VswSystem\CmsBundle\Entity\Traits\GetShortText;
use VswSystem\CmsBundle\Entity\Traits\IdentificationalEntity;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * QuestionContent
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="VswSystem\CmsBundle\Entity\Repository\QuestionContentRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class QuestionContent
{
    use IdentificationalEntity;
    use TimestampableEntity;
    use GetShortText;

    /**
     * @var string
     *
     * @ORM\Column(name="question", type="string", length=255)
     */
    protected  $question;

    /**
     * @var string
     *
     * @ORM\Column(name="answer", type="text")
     */
    protected $answer;

    /**
     * @var string
     *
     * @ORM\OneToOne(targetEntity="ContentImage", cascade={"all"})
     * @ORM\JoinColumn(nullable=true)
     */
    protected $image;

    /**
     * Set question
     *
     * @param string $question
     * @return QuestionContent
     */
    public function setQuestion($question)
    {
        $this->question = $question;

        return $this;
    }

    /**
     * Get question
     *
     * @return string 
     */
    public function getQuestion()
    {
        return $this->question;
    }

    /**
     * Set answer
     *
     * @param string $answer
     * @return QuestionContent
     */
    public function setAnswer($answer)
    {
        $this->answer = $answer;

        return $this;
    }

    /**
     * Get answer
     *
     * @return string 
     */
    public function getAnswer()
    {
        return $this->answer;
    }

    /**
     * Set image
     *
     * @param string $image
     * @return QuestionContent
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }
}
