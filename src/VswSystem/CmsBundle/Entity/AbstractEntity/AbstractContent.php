<?php

namespace VswSystem\CmsBundle\Entity\AbstractEntity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use VswSystem\CmsBundle\Entity\Traits\GetShortText;
use VswSystem\CmsBundle\Entity\Traits\IdentificationalEntity;
use VswSystem\CmsBundle\Entity\ContentImage;
use VswSystem\CmsBundle\Entity\Traits\ShortTextEntity;
use VswSystem\CmsBundle\Entity\Traits\TaggedEntity;

/**
 * Abstract Content entity
 */
abstract class AbstractContent
{

    /**
     * Hook timestampable behavior
     * updates createdAt, updatedAt fields
     */
    use TimestampableEntity;
    use IdentificationalEntity;
    use TaggedEntity;
    use ShortTextEntity;


    /**
     * @var ContentImage
     *
     * @ORM\OneToOne(targetEntity="ContentImage", cascade={"all"})
     */
    protected  $cover;

    /**
     * @var string
     * @Gedmo\Translatable
     * @ORM\Column(name="title", type="string", length=255,nullable=true)
     */
    protected $title;

    /**
     * @var string
     * @Gedmo\Translatable
     * @ORM\Column(name="content", type="text")
     */
    protected $content;


    /**
     * @Gedmo\Locale
     * Used locale to override Translation listener`s locale
     * this is not a mapped field of entity metadata, just a simple property
     */
    protected $locale;

    /**
     * Set cover
     *
     * @param ContentImage $cover
     * @return ContentBlock
     */
    public function setCover(ContentImage $cover)
    {
        $this->cover = $cover;
        return $this;
    }

    /**
     * Remove cover image
     */
    public function removeCover()
    {
        $this->cover = null;
    }

    /**
     * Get cover
     *
     * @return CoverImage
     */
    public function getCover()
    {
        return $this->cover;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return ContentBlock
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set content
     *
     * @param string $content
     * @return ContentBlock
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }


    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     * @ORM\PreFlush()
     */
    public function checkCover()
    {
       // var_dump($this->getCover()); die();
       // if (!$this->getCover()->getFile()) {
      //      $this->removeCover();
     //   }
        //var_dump($this); die();

    }

    /**
     * @param int $blockSize
     */
    public function setBlockSize($blockSize)
    {
        $this->blockSize = $blockSize;
    }

    /**
     * @return int
     */
    public function getBlockSize()
    {
        return $this->blockSize;
    }

    /**
     * @param mixed $locale
     */
    public function setLocale($locale)
    {
        $this->locale = $locale;
    }

    /**
     * @return mixed
     */
    public function getLocale()
    {
        return $this->locale;
    }



}
