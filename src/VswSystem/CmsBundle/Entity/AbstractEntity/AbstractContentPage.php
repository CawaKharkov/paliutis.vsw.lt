<?php

namespace VswSystem\CmsBundle\Entity\AbstractEntity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use VswSystem\CmsBundle\Entity\Block;
use VswSystem\CmsBundle\Entity\Traits\IdentificationalEntity;
use VswSystem\CmsBundle\Entity\Traits\NamedEntity;
use VswSystem\CmsBundle\Entity\Traits\ShareableEntity;
use VswSystem\CmsBundle\Entity\Traits\TaggedEntity;
use VswSystem\WidgetBundle\Entity\Poll;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * Page
 */
class AbstractContentPage
{
    use IdentificationalEntity;
    use NamedEntity;
    use TimestampableEntity;
    use ShareableEntity;
    use TaggedEntity;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     * @Gedmo\Versioned
     */
    protected $title;

    /**
     * @var string
     *
     * @ORM\OneToOne(targetEntity="Alias",cascade={"persist"})
     * @Gedmo\Versioned
     * @Assert\Valid()
     */
    protected $alias;


    /**
     * @ORM\ManyToMany(targetEntity="Block",cascade={"persist"})
     */
    protected $block;


    /**
     * @ORM\OneToOne(targetEntity="VswSystem\WidgetBundle\Entity\Poll")
     */
    protected $poll;




    /**
     * Set title
     *
     * @param string $title
     * @return Page
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set alias
     *
     * @param string $alias
     * @return Page
     */
    public function setAlias($alias)
    {
        $this->alias = $alias;

        return $this;
    }

    /**
     * Get alias
     *
     * @return string
     */
    public function getAlias()
    {
        return $this->alias;
    }


    /**
     *
     */
    public function __construct()
    {
        $this->block = new ArrayCollection();
        $this->poll = new ArrayCollection();
    }

    /**
     * @param Block $block
     * @return $this
     */
    public function addBlock(Block $block)
    {
        $this->block[] = $block;
        return $this;
    }

    /**
     * @param Block $block
     * @return $this
     */
    public function removeBlock(Block $block)
    {
        $this->block->removeElement($block);
        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getBlock()
    {
        return $this->block;
    }

    /**
     * @return mixed
     */
    public function getPoll()
    {
        return $this->poll;
    }

    /**
     * @param mixed $poll
     */
    public function setPoll($poll)
    {
        $this->poll = $poll;
    }




}
