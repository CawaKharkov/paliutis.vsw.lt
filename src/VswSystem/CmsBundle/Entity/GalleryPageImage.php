<?php

namespace VswSystem\CmsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use VswSystem\CmsBundle\Entity\AbstractEntity\File;
use VswSystem\CmsBundle\Entity\Traits\IdentificationalEntity;

/**
 * PageImage
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="VswSystem\CmsBundle\Entity\Repository\GalleryPageImageRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class GalleryPageImage extends File
{
    protected $uploadPath = 'uploads/images/galleryPageImages';

    /**
     * @var string
     *
     * @ORM\Column(name="caption", type="string", length=255, nullable=true)
     */
    protected $caption;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="showImages", type="boolean")
     */
    protected $showImages = 0;
    
    /**
     * @var string
     *
     * @ORM\Column(name="link", type="string", length=255, nullable=true)
     */
    
    protected $link;
    
    
    /**
     * @param string $caption
     */
    

    public function setCaption($caption)
    {
        $this->caption = $caption;
    }

    /**
     * @return string
     */
    public function getCaption()
    {
        return $this->caption;
    }

     /**
     * @param int $showImages
     */
    public function setVisible($showImages)
    {
        $this->showImages = $showImages;
    }

    /**
     * @return int
     */
    public function getVisible()
    {
        return (bool)$this->showImages;
    }
    
    
    /**
     * @param text $link
     */
    public function setLink($link)
    {
        $this->link = $link;
    }

    /**
     * @return string
     */
    public function getLink()
    {
        return $this->link;
    }

}
