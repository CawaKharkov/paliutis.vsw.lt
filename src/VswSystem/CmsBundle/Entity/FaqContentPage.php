<?php

namespace VswSystem\CmsBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use VswSystem\CmsBundle\Entity\AbstractEntity\AbstractContentPage;
use VswSystem\CmsBundle\Entity\Traits\PaginatableEntity;


/**
 * FaqContentPage
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="VswSystem\CmsBundle\Entity\Repository\FaqContentPageRepository")
 * @Gedmo\Loggable()
 */
class FaqContentPage extends AbstractContentPage
{
    use PaginatableEntity;

    /**
     * @var QuestionContent
     * @ORM\OrderBy({"createdAt" = "DESC"})
     * @ORM\ManyToMany(targetEntity="QuestionContent", cascade={"persist"})
     */
    protected $questions;

    function __construct()
    {
        $this->questions = new ArrayCollection();
    }

    /**
     * @var integer
     *
     * @ORM\Column(name="showImages", type="boolean")
     */
    protected $showImages = 0;

    /**
     * @param QuestionContent $question
     * @return $this
     */
    public function addQuestion(QuestionContent $question)
    {
        if (!$this->questions->contains($question)) {
            $this->questions->add($question);
        }
        return $this;
    }


    /**
     * @param QuestionContent $question
     * @return $this
     */
    public function remove(QuestionContent $question)
    {
        $this->questions->removeElement($question);
        return $this;
    }

    /**
     * Get all question
     * @return ArrayCollection
     */
    public function getQuestions()
    {
        return $this->questions;
    }

    /**
     * @param int $showImages
     */
    public function setShowImages($showImages)
    {
        $this->showImages = $showImages;
    }

    /**
     * @return int
     */
    public function getShowImages()
    {
        return (bool)$this->showImages;
    }


}
