<?php

namespace VswSystem\CmsBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class AddSubscribeType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', 'text', ['required' => true, 'label' => false, 'attr' => ['class' => '',
                'placeholder' => 'Your name', 'style' => 'background-color: #fff;border-color: #e9e9e9']])
            ->add('email', 'email',
                ['required' => false, 'label' => false, 'attr' => ['class' => '',
                    'placeholder' => 'Email', 'style' => 'background-color: #fff;border-color: #e9e9e9']])
            ->add('captcha','captcha',['label' => false, 'attr' => ['placeholder' => 'Code form image',] ])
            ->add('message', 'textarea', ['label' => false,
                'required' => false, 'attr' => ['class' => 'form-control msg-custom-field',
                    'rows' => 30,'style' => ' height: 150px;']]);
           // ->add('subscribe', 'submit', ['attr' => ['class' => 'btn btn-rounded']]);
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'VswSystem\CmsBundle\Entity\Subscriber',
        ));
    }

    public function getName()
    {
        return 'add_subscribe';
    }

}
