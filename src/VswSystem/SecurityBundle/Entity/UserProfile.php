<?php

namespace VswSystem\SecurityBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\Security\Core\User\UserInterface;
use VswSystem\CmsBundle\Entity\ContentImage;
use VswSystem\CmsBundle\Entity\Traits\IdentificationalEntity;

/**
 * UserProfile
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="VswSystem\SecurityBundle\Entity\UserProfileRepository")
 */
class UserProfile
{

    use IdentificationalEntity;
    use TimestampableEntity;

    /**
     * @var string
     *
     * @ORM\Column(name="userPhone", type="string", length=100,nullable=true)
     */
    protected $userPhone;


    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=100,nullable=true)
     */
    protected $name;

    /**
     * @var string
     *
     * @ORM\Column(name="surname", type="string", length=255,nullable=true)
     */
    protected $surname;

    /**
     * @var ContentImage
     *
     * @ORM\OneToOne(targetEntity="\VswSystem\CmsBundle\Entity\ContentImage", cascade={"all"})
     */
    protected $avatar;

    /**
     * @var string
     *
     * @ORM\Column(name="bloodGroup", type="string", length=255,nullable=true)
     */
    protected $bloodGroup;
    /**
     * @var array http://en.wikipedia.org/wiki/Blood_type
     */
    public static $bloodGroups = [0 => '0(I)', 1 => 'A(II)', 2 => 'B(III)', 3 => 'AB(IV)', 9 => 'Doesn\'t know'];

    /**
     * @var string
     *
     * @ORM\Column(name="bloodRh", type="string", length=255,nullable=true)
     */
    protected $bloodRh;
    public static $bloodRhs = [0 => 'Rh+', 1 => 'Rh-', 9 => 'Doesn\'t know'];

    /**
     * @var integer
     *
     * @ORM\Column(name="bloodAmount", type="smallint",nullable=true)
     */
    protected $bloodAmount;


    /**
     * @var User
     * @ORM\OneToOne(targetEntity="User", mappedBy="profile", cascade={"persist", "remove"})
     */
    protected $user;


    /**
     * @var string
     *
     * @ORM\Column(name="about", type="string", length=255,nullable=true)
     */
    protected $about;

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }


    /**
     * Set userPhone
     *
     * @param string $userPhone
     * @return UserProfile
     */
    public function setUserPhone($userPhone)
    {
        $this->userPhone = $userPhone;

        return $this;
    }

    /**
     * Get userPhone
     *
     * @return string
     */
    public function getUserPhone()
    {
        return $this->userPhone;
    }


    /**
     * Set surname
     *
     * @param string $surname
     * @return UserProfile
     */
    public function setSurname($surname)
    {
        $this->surname = $surname;

        return $this;
    }

    /**
     * Get surname
     *
     * @return string
     */
    public function getSurname()
    {
        return $this->surname;
    }

    /**
     * Set avatar
     *
     * @param string $avatar
     * @return UserProfile
     */
    public function setAvatar(ContentImage $avatar)
    {
        $this->avatar = $avatar;

        return $this;
    }

    /**
     * Get avatar
     *
     * @return string
     */
    public function getAvatar()
    {
        return $this->avatar;
    }

    /**
     * Set bloodGroup
     *
     * @param string $bloodGroup
     * @return UserProfile
     */
    public function setBloodGroup($bloodGroup)
    {
        $this->bloodGroup = $bloodGroup;

        return $this;
    }

    /**
     * Get bloodGroup
     *
     * @return string
     */
    public function getBloodGroup()
    {

        return isset($this->bloodGroup) ? $this->bloodGroup : 9;
    }

    /**
     * @param string $bloodRh
     */
    public function setBloodRh($bloodRh)
    {
        $this->bloodRh = $bloodRh;
    }

    /**
     * @return string
     */
    public function getBloodRh()
    {
        return isset($this->bloodRh) ? $this->bloodRh : 9;
    }

    /**
     * @return array
     */
    public static function getBloodGroups()
    {
        return self::$bloodGroups;
    }

    /**
     * @return array
     */
    public static function getBloodRhs()
    {
        return self::$bloodRhs;
    }


    /**
     * Set bloodAmount
     *
     * @param integer $bloodAmount
     * @return UserProfile
     */
    public function setBloodAmount($bloodAmount)
    {
        $this->bloodAmount = $bloodAmount;

        return $this;
    }

    /**
     * Get bloodAmount
     *
     * @return integer
     */
    public function getBloodAmount()
    {
        return $this->bloodAmount;
    }

    /**
     * @param \VswSystem\SecurityBundle\Entity\User $user
     */
    public function setUser(UserInterface $user)
    {
        $this->user = $user;
    }

    /**
     * @return \VswSystem\SecurityBundle\Entity\User
     */
    public function getUser()
    {
     //   return $this->user;
    }

    /**
     * @param string $about
     */
    public function setAbout($about)
    {
        $this->about = $about;
    }

    /**
     * @return string
     */
    public function getAbout()
    {
        return $this->about;
    }


}
