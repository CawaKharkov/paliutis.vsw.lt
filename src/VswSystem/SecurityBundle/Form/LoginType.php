<?php

namespace VswSystem\SecurityBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class LoginType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
                ->add('_username', 'text', ['required' => true, 'attr' => ['class' => 'form-control input-lg',
                        'placeholder' => 'Username']])
                // ->add('name', 'text', ['required'=>true, 'attr'=>['class'=>'form-control', 'placeholder'=>'Ваш ник']])
                ->add('_password', 'password', ['required' => true, 'attr' => ['class' => 'form-control input-lg col-lg-6',
                        'widget_class' => 'col-lg-6',
                        'placeholder' => 'Password']])
                ->add('login', 'submit', ['attr' => ['class' => 'btn btn-default btn-rounded']]);
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'VswSystem\SecurityBundle\Entity\User',
            'validation_groups' => ['login'],
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return null;
    }

}
