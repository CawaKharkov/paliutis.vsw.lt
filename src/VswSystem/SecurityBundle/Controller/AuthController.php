<?php

namespace VswSystem\SecurityBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

class AuthController extends Controller
{

    /**
     * @Route("/user/check", name="_security_check")
     */
    public function securityCheckAction()
    {
        // The security layer will intercept this request
    }

    /**
     * @Route("/logout/", name="_user_logout")
     */
    public function logoutAction()
    {
        // The security layer will intercept this request
    }

    /**
     * @Route("/user/login/", name="_user_login_page")
     * @Template()
     */
    public function loginPageAction(Request $request)
    {
        $loginForm = $this->get('form.factory')
                ->createNamedBuilder(null, 'form')
                ->setAction($this->generateUrl('_security_check'))
                ->add('_username', 'text', ['required' => true, 'label' => 'Username or email',
                    'attr' => ['class' => 'form-control input-lg','style'=>'width:48%;height:40px;'
                       ]])
                ->add('_password', 'password', ['required' => true, 'label' => 'Password',
                    'attr' => ['class' => 'form-control input-lg col','style'=>'width:48%;height:40px;',
                       ]])
                ->add('_remember_me', 'checkbox', ['required' => false])
                ->add('login', 'submit', ['label' => 'Login',
                    'attr' => ['class' => '"btn btn-default btn-rounded','style'=>'position: absolute;left:27%;']])
                ->getForm();
        /* $this->get('session')->getFlashBag()->add(
          'info',
          'Your changes were saved!'
          ); */
        if ($request->attributes->has(SecurityContext::AUTHENTICATION_ERROR)) {
            $error = $request->attributes->get(SecurityContext::AUTHENTICATION_ERROR);
        } else {
            $error = $request->getSession()->get(SecurityContext::AUTHENTICATION_ERROR);
        }
        return ['loginForm' => $loginForm->createView(), 'error' => $error];
    }

}
