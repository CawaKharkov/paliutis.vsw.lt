<?php

namespace VswSystem\SecurityBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use VswSystem\SecurityBundle\Entity\User;

class UserController extends Controller
{

    /**
     * @Route("/user/signup/", name="_user_signup")
     * @Template()
     */
    public function signupAction(Request $request)
    {
        $signupForm = $this->createForm('signup', new User(),['show_legend'=>false],[ 'label_render' => 'nl2br',]);
        $signupForm->handleRequest($request);

        $isCreated = false;
        if ($signupForm->isValid()) {
            $this->get('vswsystem.security.registration')->registerUser($signupForm->getData(),true);
            $isCreated = true;

            $this->get('session')->getFlashBag()->add(
                'success', 'Congratulations on a successful registration with the National Blood Centre site');

        }
        return $isCreated
                ? $this->redirect($this->generateUrl('homepage'))
                : ['signupForm' => $signupForm->createView()];
    }

}
