<?php

namespace VswSystem\WidgetBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use VswSystem\CmsBundle\Entity\Donor;
use VswSystem\WidgetBundle\Controller\AbstractController as WidgetAbstractController;

/**
 * @Route("/admin/widget/fast_registration")
 */
class FastRegistrationWidgetController extends WidgetAbstractController
{

    /**
     * @Route("/" , name="admin_widget_fast_registration")
     * @Security("has_role('ROLE_ADMIN')")
     * @Template()
     */
    public function indexAction(Request $request)
    {
       return [];
    }

    /**
     * @Route("/remove/{id}" , name="admin_donor_remove")
     * @Security("has_role('ROLE_SUPER_ADMIN')")
     * @ParamConverter("page", class="VswSystemCmsBundle:Donor")
     * @Template()
     */
    public function removeAction(Request $request, Donor $donor)
    {
        $this->getPagesManager()->remove($donor);
        $this->get('session')->getFlashBag()->add(
            'info', 'Donor ' . $donor->getName() . ' have been removed!');
        return $this->redirect($request->headers->get('referer'));
    }


}
