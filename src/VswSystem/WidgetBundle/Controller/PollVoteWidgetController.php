<?php

namespace VswSystem\WidgetBundle\Controller;

use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\Response;
use VswSystem\WidgetBundle\Controller\AbstractController as WidgetAbstractController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;
use VswSystem\WidgetBundle\Entity\Poll;


/**
 * @Route("/widget/poll/vote")
 */
class PollVoteWidgetController extends WidgetAbstractController
{

    /**
     * @Route("/" , name="widget_poll_vote")
     * @Template()
     */
    public function voteAction(Request $request)
    {
        $poll = json_decode($request->getContent(), true);

        $response = new Response(json_encode(['pollId'=>$poll['pollId'],'error'=> false]));
        $response->setStatusCode(200);
        $response->headers->set('Content-Type', 'application/json');

        $answer = $this->getPollAnswerService()->get($poll['answerId']);
        $answer->setHits($answer->getHits() + 1);
        $this->getPollAnswerService()->save($answer,true);

        $now = new \DateTime();
        $response->headers->setCookie(new Cookie("poll".$poll['pollId'], $poll['answerId'])); //.':'.$now->format('d-m-Y')
        return $response;
    }


}
