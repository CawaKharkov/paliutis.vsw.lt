<?php

namespace VswSystem\WidgetBundle\Controller;

use VswSystem\WidgetBundle\Controller\AbstractController as WidgetAbstractController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;
use VswSystem\WidgetBundle\Entity\HallOfFameWidget;
use VswSystem\WidgetBundle\Entity\Partner;


/**
 * @Route("/admin/widget/hall")
 */
class HallWidgetController extends WidgetAbstractController
{

    /**
     * @Route("/" , name="admin_widget_hall")
     * @Security("has_role('ROLE_ADMIN')")
     * @Template()
     */
    public function indexAction(Request $request)
    {
        $sortField = $request->get('sort', null);
        $sortDirection = $request->get('direction', null);

        $partners = $this->getWidgetManager()->getHallWidgets();

        $paginator = $this->get('knp_paginator');

        $pagination = $paginator->paginate(
            $partners,
            $request->query->get('page', 1) /*page number*/,
            10/*limit per page*/
        );
        return ['pagination' => $pagination];
    }

    /**
     * @Route("/partners/{id}" , name="admin_widget_hall_partners")
     * @ParamConverter("widget", class="VswSystemWidgetBundle:HallOfFameWidget")
     * @Security("has_role('ROLE_ADMIN')")
     * @Template()
     */
    public function partnersAction(Request $request, $widget)
    {
        $sortField = $request->get('sort', null);
        $sortDirection = $request->get('direction', null);

        $partners = $widget->getPartners();

        $paginator = $this->get('knp_paginator');

        $pagination = $paginator->paginate(
            $partners,
            $request->query->get('page', 1) /*page number*/,
            10/*limit per page*/
        );
        return ['pagination' => $pagination,'widget' => $widget];
    }

    /**
     * @Route("/create" , name="admin_widget_hall_create")
     * @Security("has_role('ROLE_ADMIN')")
     * @Template()
     */
    public function createAction(Request $request)
    {
        $widgetForm = $this->createForm('widget_edit_hall', new HallOfFameWidget(), ['show_legend' => false]);
        $widgetForm->handleRequest($request);

        $isCreated = false;
        if ($widgetForm->isValid()) {
            $page = $this->getWidgetManager()->save($widgetForm->getData(), true);
            $this->get('session')->getFlashBag()->add(
                'success', 'Widget: ' . $page->getName() . ' have been created!');
            $isCreated = true;
        }

        return $isCreated
            ? $this->redirect($this->generateUrl('admin_widget_hall'))
            : ['widgetForm' => $widgetForm->createView()];
    }

    /**
     * @Route("/edit/{id}" , name="admin_widget_hall_edit")
     * @ParamConverter("widget", class="VswSystemWidgetBundle:HallOfFameWidget")
     * @Security("has_role('ROLE_ADMIN')")
     * @Template()
     */
    public function editAction(Request $request, $widget)
    {
        $widgetForm = $this->createForm('widget_edit_hall',$widget, ['show_legend' => false]);
        $widgetForm->handleRequest($request);

        $isCreated = false;
        if ($widgetForm->isValid()) {
            $page = $this->getWidgetManager()->save($widgetForm->getData(), true);
            $this->addFlash('success', 'Widget: ' . $page->getName() . ' have been created!');
            $isCreated = true;
        }

        return $isCreated
            ? $this->redirectToRoute('admin_widget_hall')
            : ['widgetForm' => $widgetForm->createView()];
    }

    /**
     * @Route("/remove/{id}" , name="admin_widget_hall_remove")
     * @Security("has_role('ROLE_SUPER_ADMIN')")
     * @ParamConverter("page", class="VswSystemWidgetBundle:HallOfFameWidget")
     * @Template()
     */
    public function removeAction(Request $request, HallOfFameWidget $fameWidget)
    {
        $this->getWidgetManager()->remove($fameWidget,true);
        $this->addFlash('info', 'HallOfFameWidget ' . $fameWidget->getName() . ' have been removed!');
        return $this->redirect($request->headers->get('referer'));
    }

    /**
     * @Route("/editPartner/{id}" , name="admin_partner_edit")
     * @ParamConverter("partner", class="VswSystemWidgetBundle:Partner")
     * @Security("has_role('ROLE_ADMIN')")
     * @Template()
     */
    public function editPartnerAction(Request $request, $partner)
    {
        $pageForm = $this->createForm('widget_edit_partner', $partner, ['show_legend' => false]);
        $pageForm->add('Save partner', 'submit', ['attr' => ['class' => 'btn btn-primary btn-lg']]);
        $pageForm->handleRequest($request);

        $isCreated = false;
        if ($pageForm->isValid()) {
            $page = $this->getWidgetManager()->save($pageForm->getData(), true);
            $this->addFlash('success', 'Page: ' . $page->getName() . ' have been updated!');
            $isCreated = true;
        }

        return $isCreated
            ? $this->redirectToRoute('admin_widget_hall')
            : ['pageForm' => $pageForm->createView()];
    }


    /**
     * @Route("/removePartner/{id}" , name="admin_partner_remove")
     * @Security("has_role('ROLE_SUPER_ADMIN')")
     * @ParamConverter("partner", class="VswSystemWidgetBundle:Partner")
     * @Template()
     */
    public function removePartnerAction(Request $request,Partner  $partner)
    {
        $this->getWidgetManager()->remove($partner);
        $this->get('session')->getFlashBag()->add(
            'info', 'Partner ' . $partner->getName() . ' have been removed!');
        return $this->redirect($request->headers->get('referer'));
    }

}
