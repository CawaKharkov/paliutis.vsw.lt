<?php

namespace VswSystem\WidgetBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;


abstract class AbstractController extends Controller
{

    /**
     * @return \VswSystem\WidgetBundle\Poll\Service\PollService
     */
    public function getPollService()
    {
        return $this->get('vswsystem.orm.service.widget.poll');
    }

    /**
     * @return \VswSystem\WidgetBundle\Poll\Service\AnswerService
     */
    public function getPollAnswerService()
    {
        return $this->get('vswsystem.orm.service.widget.poll.answer');
    }


    /**
     * @return \VswSystem\WidgetBundle\Line\Service\LineService
     */
    public function getPartnerLineService()
    {
        return $this->get('vswsystem.orm.service.widget.line');
    }

    /**
     * @return \VswSystem\WidgetBundle\Manager\WidgetManager
     */
    public function getWidgetManager()
    {
        return $this->get('vswsystem.orm.widget.manager');
    }
}
