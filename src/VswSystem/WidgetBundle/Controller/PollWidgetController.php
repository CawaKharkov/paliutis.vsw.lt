<?php

namespace VswSystem\WidgetBundle\Controller;

use VswSystem\WidgetBundle\Controller\AbstractController as WidgetAbstractController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;
use VswSystem\WidgetBundle\Entity\Poll;


/**
 * @Route("/admin/widget/poll")
 */
class PollWidgetController extends WidgetAbstractController
{

    /**
     * @Route("/" , name="admin_widget_poll")
     * @Security("has_role('ROLE_ADMIN')")
     * @Template()
     */
    public function indexAction(Request $request)
    {
        $sortField = $request->get('sort', null);
        $sortDirection = $request->get('direction', null);

        $polls = $this->getPollService()->getRepository()->findAll();

        $paginator = $this->get('knp_paginator');

        $pagination = $paginator->paginate(
            $polls,
            $request->query->get('page', 1) /*page number*/,
            10/*limit per page*/
        );
        return ['pagination' => $pagination];
    }

    /**
     * @Route("/create" , name="admin_widget_poll_create")
     * @Security("has_role('ROLE_ADMIN')")
     * @Template()
     */
    public function createAction(Request $request)
    {
        $widgetForm = $this->createForm('widget_poll_edit', new Poll(), ['show_legend' => false]);
        $widgetForm->handleRequest($request);

        $isCreated = false;
        if ($widgetForm->isValid()) {
            $poll = $this->getPollService()->save($widgetForm->getData(), true);
            foreach($poll->getAnswers() as $answer){
                $answer->setPoll($poll);
                $this->getPollAnswerService()->save($answer,true);
            }
            $this->get('session')->getFlashBag()->add(
                'success', 'Widget: ' . $poll->getName() . ' have been created!');
        }

        return $isCreated
            ? $this->redirect($this->generateUrl('admin_widget_poll'))
            : ['widgetForm' => $widgetForm->createView()];
    }

    /**
     * @Route("/edit/{id}" , name="admin_widget_poll_edit")
     * @ParamConverter("widget", class="VswSystemWidgetBundle:Poll")
     * @Security("has_role('ROLE_ADMIN')")
     * @Template()
     */
    public function editAction(Request $request, $widget)
    {
        $widgetForm = $this->createForm('widget_poll_edit',$widget, ['show_legend' => false]);
        $widgetForm->handleRequest($request);


        $isCreated = false;
        if ($widgetForm->isValid()) {
            //var_dump($widgetForm->getData()); die();
            $poll = $this->getPollService()->save($widgetForm->getData(), true);
            foreach($poll->getAnswers() as $answer){
                $answer->setPoll($poll);
                $this->getPollAnswerService()->save($answer,true);
            }
            $this->get('session')->getFlashBag()->add(
                'success', 'Widget: ' . $poll->getName() . ' have been created!');
            $isCreated = true;
        }

        return ['widgetForm' => $widgetForm->createView()];
    }

    /**
     * @Route("/remove/{id}" , name="admin_widget_poll_remove")
     * @Security("has_role('ROLE_SUPER_ADMIN')")
     * @ParamConverter("page", class="VswSystemWidgetBundle:Poll")
     * @Template()
     */
    public function removeAction(Request $request, Poll $poll)
    {
        $this->getPollService()->remove($poll,true);
        $this->get('session')->getFlashBag()->add(
            'info', 'Poll ' . $poll->getName() . ' have been removed!');
        return $this->redirect($request->headers->get('referer'));
    }

    /**
     * @Route("/get/{id}" , name="widget_poll_get")
     * @Security("has_role('ROLE_SUPER_ADMIN')")
     * @ParamConverter("page", class="VswSystemWidgetBundle:Poll")
     * @Template()
     */
    public function getPollAction(Request $request, Poll $poll)
    {
        return ['poll'=>$poll];
    }
}
