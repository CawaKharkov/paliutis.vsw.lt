<?php

namespace VswSystem\WidgetBundle\Controller;

use VswSystem\WidgetBundle\Controller\AbstractController as WidgetAbstractController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;
use VswSystem\WidgetBundle\Entity\HallOfFameWidget;
use VswSystem\WidgetBundle\Entity\Partner;
use VswSystem\WidgetBundle\Entity\PartnerLine;
use VswSystem\WidgetBundle\Entity\PartnerLineWidget;


/**
 * @Route("/admin/widget/line")
 */
class LineWidgetController extends WidgetAbstractController
{

    /**
     * @Route("/" , name="admin_widget_line")
     * @Security("has_role('ROLE_ADMIN')")
     * @Template()
     */
    public function indexAction(Request $request)
    {
        $sortField = $request->get('sort', null);
        $sortDirection = $request->get('direction', null);

        $partners = $this->getPartnerLineService()->find('name','main');

        $paginator = $this->get('knp_paginator');
        if(!$partners){
            $partners = [];
        }else{
            $partners = $partners->getPartners();
        }
        $pagination = $paginator->paginate(
            $partners,
            $request->query->get('page', 1) /*page number*/,
            10/*limit per page*/
        );
        return ['pagination' => $pagination];
    }

    /**
     * @Route("/create" , name="admin_widget_line_create")
     * @Security("has_role('ROLE_ADMIN')")
     * @Template()
     */
    public function createAction(Request $request)
    {
        $widgetForm = $this->createForm('widget_edit_line', $this->getPartnerLineService()->create(), ['show_legend' => false]);
        $widgetForm->handleRequest($request);

        $isCreated = false;
        if ($widgetForm->isValid()) {
            $page = $this->getWidgetManager()->save($widgetForm->getData(), true);
            $this->get('session')->getFlashBag()->add(
                'success', 'Widget: ' . $page->getName() . ' have been created!');
            $isCreated = true;
        }

        return $isCreated
            ? $this->redirect($this->generateUrl('admin_widget_line'))
            : ['widgetForm' => $widgetForm->createView()];
    }

    /**
     * @Route("/edit/{id}" , name="admin_widget_line_edit")
     * @ParamConverter("widget", class="VswSystemWidgetBundle:PartnerLineWidget")
     * @Security("has_role('ROLE_ADMIN')")
     * @Template()
     */
    public function editAction(Request $request, $widget)
    {
        $widgetForm = $this->createForm('widget_edit_line',$widget, ['show_legend' => false]);
        $widgetForm->handleRequest($request);

        $isCreated = false;
        if ($widgetForm->isValid()) {
            $page = $this->getWidgetManager()->save($widgetForm->getData(), true);
            $this->addFlash('success', 'Widget: ' . $page->getName() . ' have been created!');
            $isCreated = true;
        }

        return $isCreated
            ? $this->redirectToRoute('admin_widget_line')
            : ['widgetForm' => $widgetForm->createView()];
    }

    /**
     * @Route("/remove/{id}" , name="admin_widget_line_remove")
     * @Security("has_role('ROLE_SUPER_ADMIN')")
     * @ParamConverter("widget", class="VswSystemWidgetBundle:PartnerLineWidget")
     * @Template()
     */
    public function removeAction(Request $request, PartnerLineWidget $widget)
    {
        $this->getPartnerLineService()->remove($widget);
        $this->addFlash('info', 'Partner have been removed!');
        return $this->redirect($request->headers->get('referer'));
    }

    /**
     * @Route("/editPartner/{id}" , name="admin_line_partner_edit")
     * @ParamConverter("partner", class="VswSystemWidgetBundle:PartnerLine")
     * @Security("has_role('ROLE_ADMIN')")
     * @Template()
     */
    public function editPartnerAction(Request $request, $partner)
    {
        $pageForm = $this->createForm('widget_edit_line_partner', $partner, ['show_legend' => false]);
        $pageForm->add('Save partner', 'submit', ['attr' => ['class' => 'btn btn-primary btn-lg']]);
        $pageForm->handleRequest($request);

        $isCreated = false;
        if ($pageForm->isValid()) {
            $page = $this->getPartnerLineService()->save($pageForm->getData(), true);
            $this->addFlash('success', 'Partner: ' . $page->getLink() . ' have been updated!');
            $isCreated = true;
        }

        return $isCreated
            ? $this->redirectToRoute('admin_widget_line')
            : ['pageForm' => $pageForm->createView()];
    }


    /**
     * @Route("/removePartner/{id}" , name="admin_line_partner_remove")
     * @Security("has_role('ROLE_SUPER_ADMIN')")
     * @ParamConverter("partner", class="VswSystemWidgetBundle:PartnerLine")
     * @Template()
     */
    public function removePartnerAction(Request $request,PartnerLine  $partner)
    {
        $this->getPartnerLineService()->remove($partner,true);
        $this->get('session')->getFlashBag()->add(
            'info', 'Partner ' . $partner->getLink() . ' have been removed!');
        return $this->redirect($request->headers->get('referer'));
    }

}
