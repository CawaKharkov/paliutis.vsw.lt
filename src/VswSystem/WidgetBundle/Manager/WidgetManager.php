<?php

namespace VswSystem\WidgetBundle\Manager;

use VswSystem\CoreBundle\Manager\AbstractManager;

/**
 * Description of WidgetManager
 *
 * @author cawa
 */
class WidgetManager extends AbstractManager
{

    protected $hallOfFameEntity;
    protected $partnerEntity;

    /**
     * @param mixed $hallOfFameEntity
     */
    public function setHallOfFameEntity($hallOfFameEntity)
    {
        $this->hallOfFameEntity = $hallOfFameEntity;
    }

    /**
     * @return mixed
     */
    public function getHallOfFameEntity()
    {
        return $this->hallOfFameEntity;
    }

    /**
     * @param mixed $partnerEntity
     */
    public function setPartnerEntity($partnerEntity)
    {
        $this->partnerEntity = $partnerEntity;
    }

    /**
     * @return mixed
     */
    public function getPartnerEntity()
    {
        return $this->partnerEntity;
    }

    /**
     * @return \Doctrine\Common\Persistence\ObjectRepository
     */
    public function getHallOfFameRepository()
    {
        return $this->getRepository($this->getHallOfFameEntity());
    }

    /**
     * @return \Doctrine\Common\Persistence\ObjectRepository
     */
    public function getPagtnerRepository()
    {
        return $this->getRepository($this->getPartnerEntity());
    }


    public function getPartners()
    {
        return $this->getPagtnerRepository()->findAll();
    }

    public function getHallWidget($id = null, $name = null)
    {
        if ($id) {
            $widget = $this->getHallOfFameRepository()->find($id);
        } else {
            $widget = $this->getHallOfFameRepository()->findOneBy(['name' => $name]);
        }
        return $widget;

    }

    public function getHallWidgets()
    {
        return $this->getHallOfFameRepository()->findAll();

    }



}
