<?php

namespace VswSystem\WidgetBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use VswSystem\CmsBundle\Entity\Traits\IdentificationalEntity;
use VswSystem\CmsBundle\Entity\Traits\NamedEntity;

/**
 * Poll
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="VswSystem\WidgetBundle\Entity\Repository\PollRepository")
 */
class Poll
{

    use IdentificationalEntity;
    use NamedEntity;

    /**
     * @var string
     *
     * @ORM\Column(name="question", type="string", length=255)
     */
    protected  $question;

    /**
     * @ORM\OneToMany(targetEntity="VswSystem\WidgetBundle\Entity\PollAnswer",cascade={"persist"},mappedBy="poll")
     */
    protected $answers;

    public function __construct()
    {
        $this->answers = new ArrayCollection();
    }
    /**
     * Set question
     *
     * @param string $question
     * @return Poll
     */
    public function setQuestion($question)
    {
        $this->question = $question;

        return $this;
    }

    /**
     * Get question
     *
     * @return string
     */
    public function getQuestion()
    {
        return $this->question;
    }


    public function addAnswer(PollAnswer $answer)
    {
        if (!$this->answers->contains($answer)) {
            $this->answers->add($answer);
        }
        return $this;
    }




    public function removeAnswer(PollAnswer $answer)
    {
        $this->answers->removeElement($answer);
        return $this;
    }

    /**
     * Get answers
     *
     * @return string
     */
    public function getAnswers()
    {
        return $this->answers;
    }
}
