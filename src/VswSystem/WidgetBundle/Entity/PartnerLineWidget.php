<?php

namespace VswSystem\WidgetBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use VswSystem\CmsBundle\Entity\Traits\IdentificationalEntity;
use VswSystem\CmsBundle\Entity\Traits\NamedEntity;

/**
 * HallOfFameWidget
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="VswSystem\WidgetBundle\Entity\Repository\PartnerLineWidgetRepository")
 */
class PartnerLineWidget
{

    use IdentificationalEntity;
    use NamedEntity;


    /**
     * @var Partner[]
     * @ORM\OrderBy({"updatedAt" = "DESC"})
     * @ORM\ManyToMany(targetEntity="PartnerLine", cascade={"persist"}, orphanRemoval=true)
     */
    protected $partners;
    protected $partnersFromExisting;

    public function __construct()
    {
        $this->partners = new ArrayCollection();
        $this->partnersFromExisting = new ArrayCollection();
    }

    /**
     * @param \VswSystem\WidgetBundle\Entity\PartnerLine[] $partners
     */
    public function addPartner(PartnerLine $partner)
    {
        if (!$this->partners->contains($partner)) {
            $this->partners->add($partner);
        }
        return $this;
    }

    /**
     * @return \VswSystem\WidgetBundle\Entity\Partner[]
     */
    public function getPartners()
    {
        return $this->partners;
    }

    public function removePartner(PartnerLine $partner)
    {
        $this->partners->remove($partner);
        return $this;
    }


}
