<?php

namespace VswSystem\WidgetBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use VswSystem\CmsBundle\Entity\Traits\IdentificationalEntity;
use VswSystem\CmsBundle\Entity\Traits\NamedEntity;
use VswSystem\CmsBundle\Entity\ContentImage;
use VswSystem\CmsBundle\Entity\Traits\TitledEntity;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * Partner
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="VswSystem\WidgetBundle\Entity\Repository\PartnerLineRepository")
 */
class PartnerLine
{

    use IdentificationalEntity;
    use TimestampableEntity;
 //   use TitledEntity;


    /**
     * @var ContentImage
     *
     * @ORM\OneToOne(targetEntity="\VswSystem\CmsBundle\Entity\ContentImage", cascade={"all"})
     */
    protected  $cover;

    /**
     * @var boolean
     *
     * @ORM\Column(name="visible", type="boolean")
     */
    protected $visible =1;


    /**
     * @var string
     *
     * @ORM\Column(name="link", type="string", length=255, nullable=true)
     * @Assert\Url()
     */
    protected $link;


    /**
     * Set about
     *
     * @param string $about
     * @return Partner
     */
    public function setAbout($about)
    {
        $this->about = $about;

        return $this;
    }

    /**
     * Get about
     *
     * @return string 
     */
    public function getAbout()
    {
        return $this->about;
    }

    /**
     * @param \VswSystem\CmsBundle\Entity\ContentImage $cover
     */
    public function setCover($cover)
    {
        $this->cover = $cover;
    }

    /**
     * @return \VswSystem\CmsBundle\Entity\ContentImage
     */
    public function getCover()
    {
        return $this->cover;
    }


    /**
     * Set visible
     *
     * @param boolean $visible
     * @return Partner
     */
    public function setVisible($visible)
    {
        $this->visible = $visible;

        return $this;
    }

    /**
     * Get visible
     *
     * @return boolean 
     */
    public function getVisible()
    {
        return $this->visible;
    }

    /**
     * @return string
     */
    public function getLink()
    {
        return $this->link;
    }

    /**
     * @param string $link
     */
    public function setLink($link)
    {
        $this->link = $link;
    }


}
