<?php

namespace VswSystem\WidgetBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use VswSystem\CmsBundle\Entity\Traits\IdentificationalEntity;

/**
 * PollAnswer
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="VswSystem\WidgetBundle\Entity\Repository\PollAnswerRepository")
 */
class PollAnswer
{

    use IdentificationalEntity;

   /**
     * @var string
     *
     * @ORM\Column(name="answer", type="string", length=255)
     */
    protected $answer;

    /**
     * @var integer
     *
     * @ORM\Column(name="hits", type="smallint",nullable=true)
     */
    protected $hits;

    /**
     * @ORM\ManyToOne(targetEntity="VswSystem\WidgetBundle\Entity\Poll",cascade={"persist"},inversedBy="answers")
     */
    protected $poll;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set poll
     *
     * @param string $poll
     * @return PollAnswer
     */
    public function setPoll($poll)
    {
        $this->poll = $poll;

        return $this;
    }

    /**
     * Get poll
     *
     * @return string 
     */
    public function getPoll()
    {
        return $this->poll;
    }

    /**
     * Set answer
     *
     * @param string $answer
     * @return PollAnswer
     */
    public function setAnswer($answer)
    {
        $this->answer = $answer;

        return $this;
    }

    /**
     * Get answer
     *
     * @return string 
     */
    public function getAnswer()
    {
        return $this->answer;
    }

    /**
     * Set hits
     *
     * @param integer $hits
     * @return PollAnswer
     */
    public function setHits($hits)
    {
        $this->hits = $hits;

        return $this;
    }

    /**
     * Get hits
     *
     * @return integer 
     */
    public function getHits()
    {
        return $this->hits;
    }


}
