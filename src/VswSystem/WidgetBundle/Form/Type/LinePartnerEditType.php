<?php

namespace VswSystem\WidgetBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class LinePartnerEditType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('cover', 'admin_files_content_image_edit', ['label' => false])
                ->add('link', 'url', ['required' => true, 'attr' => ['class' => '',
                    'placeholder' => 'Partner link',]]);
        if (isset($options['attr']['submit'])) {
            $builder->add('Save widget', 'submit', ['attr' => ['class' => 'btn btn-primary btn-lg']]);
        }
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'VswSystem\WidgetBundle\Entity\PartnerLine',
        ));
    }

    public function getName()
    {
        return 'widget_edit_line_partner';
    }

}
