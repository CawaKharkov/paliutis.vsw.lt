<?php

namespace VswSystem\WidgetBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use VswSystem\CmsBundle\Entity\NewsContentPage;


class LineWidgetEditType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name', 'text', [
            'required' => true, 'attr' => ['class' => 'form-input input-lg']])

            ->add('partners', 'collection', ['type' => 'widget_edit_line_partner',
                'allow_add' => true, 'allow_delete' => true,
                'widget_add_btn' => ['label' => "Add partner"],
                'show_legend' => false,  // dont show another legend of subform
                'delete_empty' => true,
                'options' => array( // options for collection fields
                    'label_render' => false,
                    'widget_remove_btn' => array('label' => "Remove this partner",
                        "icon" => "pencil",
                        'attr' => array('class' => 'btn btn-danger')),
                )])
            ->add('Save widget', 'submit', ['attr' => ['class' => 'btn btn-primary btn-lg']]);
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'VswSystem\WidgetBundle\Entity\PartnerLineWidget',
            'validation_groups' => ['edit_page'],
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'widget_edit_line';
    }

}
