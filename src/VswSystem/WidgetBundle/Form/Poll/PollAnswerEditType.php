<?php

namespace VswSystem\WidgetBundle\Form\Poll;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use VswSystem\CmsBundle\Entity\Navigation;

class PollAnswerEditType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder // ['data' => $options['pollId']
                ->add('answer', 'text', ['required' => true, 'attr' => ['class' => 'form-control input-lg',
                        'placeholder' => 'Answer']]);

    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'VswSystem\WidgetBundle\Entity\PollAnswer',
        ));
    }

    public function getName()
    {
        return 'widget_poll_answer_edit';
    }

}
