<?php

namespace VswSystem\WidgetBundle\Form\Poll;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;


class PollEditType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name', 'text', [
            'required' => true, 'attr' => ['class' => 'form-control']])
            ->add('question', 'text', [
                'required' => true, 'attr' => ['class' => 'form-control']])
//            ->add('showImages', 'checkbox', ['required' => false,])
            //           ->add('shareable', 'checkbox', ['required' => false,])
            //          ->add('pdfFromExisting', 'collection', ['type' => 'entity',
            /*              'allow_add' => true, 'allow_delete' => true, 'prototype' => true,
                          'widget_add_btn' => ['label' => "Add existing PDF"],
                          'show_legend' => false, // dont show another legend of subform
                          'delete_empty' => true,
                          'options' => array( // options for collection fields
                              'class' => 'VswSystemCmsBundle:ContentPdf',
                              'property' => 'name',
                              'label_render' => false,
                              'widget_remove_btn' => array('label' => "Remove this file",
                                  "icon" => "pencil",
                                  'attr' => array('class' => 'btn btn-danger')),
                              'horizontal_input_wrapper_class' => "col-lg-8")])*/
            ->add('answers', 'collection', ['type' => 'widget_poll_answer_edit',
                'allow_add' => true, 'allow_delete' => true, 'prototype' => true,
                'widget_add_btn' => ['label' => "Add answer"],
                'show_legend' => false, // dont show another legend of subform
                'delete_empty' => true,
                'options' => array( // options for collection fields
                    'label_render' => false,
                    'widget_remove_btn' => array('label' => "Remove answer",
                        "icon" => "pencil",
                        'attr' => array('class' => 'btn btn-danger')),
                )])
            ->add('Save page', 'submit', ['attr' => ['class' => 'btn btn-success']]);
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'VswSystem\WidgetBundle\Entity\Poll',
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'widget_poll_edit';
    }


}
