<?php

namespace VswSystem\WidgetBundle\Form\Poll;

use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class PollType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $data = $builder->getData();
        $builder->setAction($options['action']);
        $builder
            ->add('id', 'hidden')
            ->add('answers', 'entity', ['class' => 'VswSystemWidgetBundle:PollAnswer',
                'label' => ' ',
                'attr' => ['class' => 'radio'],
                'property' => 'answer',
                'query_builder' => function (EntityRepository $er) use($data) {
                    return $er->createQueryBuilder('a')
                        ->select('a')
                        ->where('a.poll = :p')
                        ->setParameter('p',$data->getId());
                },
                'expanded' => true, 'multiple' => false])
            ->add('Vote', 'submit', ['attr' => ['class' => 'btn btn-default btn-xs']]);
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'VswSystem\WidgetBundle\Entity\Poll',
            'attr' => array(
                'class' => 'voting'
            ),
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'widget_poll_vote';
    }
}
