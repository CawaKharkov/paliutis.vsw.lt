<?php

namespace VswSystem\WidgetBundle\Form\Hall;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class PartnerEditType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('cover', 'admin_files_content_image_edit', ['label' => false])
                ->add('name', 'text', ['required' => true, 'label' => false, 'attr' => ['class' => '',
                    'placeholder' => 'Partner name']])
                ->add('about', 'textarea', ['required' => true, 'label' => false, 'attr' => ['class' => '',
                    'placeholder' => 'About partner',]])
                ->add('link', 'url', ['required' => true, 'label' => false, 'attr' => ['class' => '',
                    'placeholder' => 'Partner link',]]);
        if (isset($options['attr']['submit'])) {
            $builder->add('Save widget', 'submit', ['attr' => ['class' => 'btn btn-primary btn-lg']]);
        }
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'VswSystem\WidgetBundle\Entity\Partner',
        ));
    }

    public function getName()
    {
        return 'widget_edit_partner';
    }

}
