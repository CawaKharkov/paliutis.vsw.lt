<?php

namespace VswSystem\WidgetBundle\Menu;

use Doctrine\ORM\EntityManagerInterface;
use Knp\Menu\FactoryInterface;
use Symfony\Component\DependencyInjection\ContainerAware;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Security\Core\SecurityContextInterface;

class MenuBuilder extends ContainerAware
{

    protected $factory;
    protected $securityContext = null;

    /**
     *
     * @var \VswSystem\CoreBundle\Manager\NavigationManager
     */
    protected $manager = null;

    /**
     * @param FactoryInterface $factory
     */
    public function __construct(FactoryInterface $factory)
    {
        $this->factory = $factory;
    }

    public function createWidgetMenu(RequestStack $requestStack)
    {
        $request = $requestStack->getCurrentRequest();
        $menu = $this->factory->createItem('root');


        $widgets = $menu->addChild('Widgets', ['icon' => 'book',
            'dropdown' => true,
            'caret' => true]);
        $widgets->addChild('Hall of fame', ['route' => 'admin_widget_hall', 'icon' => 'list']);
        $widgets->addChild('Line', ['route' => 'admin_widget_line', 'icon' => 'picture']);
        $widgets->addChild('Poll', ['route' => 'admin_widget_poll', 'icon' => 'list']);


        $widgets = $menu->addChild('ADMIN', ['route' => 'admin','icon' => 'home',]);
        return $menu;
    }


    public function setSecurityContext(SecurityContextInterface $sc = null)
    {
        $this->securityContext = $sc;
    }

    public function setManager(EntityManagerInterface $manager = null)
    {
        $this->manager = $manager;
    }

    public function addUserMenu($menu)
    {
        /* if ($this->securityContext->isGranted('ROLE_ADMIN') ||
          $this->securityContext->isGranted('ROLE_SUPER_ADMIN') ||
          $this->securityContext->isGranted('ROLE_MODERATOR')) {
          $menu->addChild('Admin panel', ['route' => 'admin', 'icon' => 'registration-mark']);
          } */

        if (!$this->securityContext->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            $menu->addChild('Login', ['route' => '_user_login_page', 'icon' => 'log-in']);
            $menu->addChild('SignUp', ['route' => '_user_signup', 'icon' => 'registration-mark']);
        }

        if ($this->securityContext->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            $menu->addChild('Logout', ['route' => '_user_logout', 'icon' => 'log-out']);
        }
    }

}
