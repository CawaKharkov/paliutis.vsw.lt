<?php

namespace VswSystem\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Gedmo\Mapping\Annotation as Gedmo;
use VswSystem\CmsBundle\Entity\Traits\IdentificationalEntity;

/**
 * EntityLogMessage
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="VswSystem\AdminBundle\Entity\Repository\EntityLogMessageRepository")
 */
class EntityLogMessage
{

    use IdentificationalEntity;
    use TimestampableEntity;

    /**
     * @var string
     *
     * @ORM\Column(name="text", type="string", length=255)
     */
    protected  $text;

    /**
     * @var string
     *
     * @ORM\Column(name="entity", type="string", length=255)
     */
    protected $entity;

    /**
     * @var integer
     * @ORM\OneToOne(targetEntity="VswSystem\SecurityBundle\Entity\User")
     */
    protected $user;

    /**
     * @var string
     *
     * @ORM\Column(name="params", type="text")
     */
    protected $params;

    /**
     * @var integer
     *
     * @ORM\Column(name="type", type="string",length=20)
     */
    protected $type;

    CONST TYPE_UPDATE = 'update';
    CONST TYPE_REMOVE = 'remove';


    /**
     * Set text
     *
     * @param string $text
     * @return EntityLogMessage
     */
    public function setText($text)
    {
        $this->text = $text;

        return $this;
    }

    /**
     * Get text
     *
     * @return string 
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * Set entity
     *
     * @param string $entity
     * @return EntityLogMessage
     */
    public function setEntity($entity)
    {
        $this->entity = $entity;

        return $this;
    }

    /**
     * Get entity
     *
     * @return string 
     */
    public function getEntity()
    {
        return $this->entity;
    }

    /**
     * Set user
     *
     * @param integer $user
     * @return EntityLogMessage
     */
    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return integer 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set params
     *
     * @param string $params
     * @return EntityLogMessage
     */
    public function setParams($params)
    {
        $this->params = $params;

        return $this;
    }

    /**
     * Get params
     *
     * @return string 
     */
    public function getParams()
    {
        return $this->params;
    }

    /**
     * @return int
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param int $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }


}
