<?php

namespace VswSystem\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use VswSystem\CmsBundle\Entity\Traits\IdentificationalEntity;

/**
 * SittingsValue
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="VswSystem\AdminBundle\Entity\Repository\SettingsValueRepository")
 */
class SettingsValue
{
    use IdentificationalEntity;

    /**
     * @var string
     *
     * @ORM\Column(name="settingName", type="string", length=255)
     */
    protected  $settingName;

    /**
     * @var string
     *
     * @ORM\Column(name="settingValue", type="string", length=255)
     */
    protected $settingValue;

    /**
     * @var integer
     * @ORM\ManyToMany(targetEntity="VswSystem\AdminBundle\Entity\SettingsGroup",mappedBy="settings")
     */
    protected $settingsGroups;

    /**
     * @var string
     *
     * @ORM\Column(name="defaultValue", type="string", length=255,nullable=true)
     */
    protected $defaultValue;


    /**
     * Set settingName
     *
     * @param string $settingName
     * @return SittingsValue
     */
    public function setSettingName($settingName)
    {
        $this->settingName = $settingName;

        return $this;
    }

    /**
     * Get settingName
     *
     * @return string 
     */
    public function getSettingName()
    {
        return $this->settingName;
    }

    /**
     * Set settingValue
     *
     * @param string $settingValue
     * @return SittingsValue
     */
    public function setSettingValue($settingValue)
    {
        $this->settingValue = $settingValue;

        return $this;
    }

    /**
     * Get settingValue
     *
     * @return string 
     */
    public function getSettingValue()
    {
        return $this->settingValue;
    }

    /**
     * Set settingsGroup
     *
     * @param integer $settingsGroup
     * @return SittingsValue
     */
    public function setSettingsGroups($settingsGroup)
    {
        die('s');
        $this->settingsGroup = $settingsGroup;
        return $this;
    }

    /**
     * Get settingsGroup
     *
     * @return integer 
     */
    public function getSettingsGroups()
    {
        return $this->settingsGroups;
    }

    /**
     * Set defaultValue
     *
     * @param string $defaultValue
     * @return SittingsValue
     */
    public function setDefaultValue($defaultValue)
    {
        $this->defaultValue = $defaultValue;

        return $this;
    }

    /**
     * Get defaultValue
     *
     * @return string 
     */
    public function getDefaultValue()
    {
        return $this->defaultValue;
    }
}
