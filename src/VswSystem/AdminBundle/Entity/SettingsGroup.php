<?php

namespace VswSystem\AdminBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use VswSystem\CmsBundle\Entity\Traits\IdentificationalEntity;
use VswSystem\CmsBundle\Entity\Traits\NamedEntity;

/**
 * SettingsGroup
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="VswSystem\AdminBundle\Entity\Repository\SettingsGroupRepository")
 */
class SettingsGroup
{
    use IdentificationalEntity;
    use NamedEntity;

    /**
     * @ORM\ManyToMany(targetEntity="VswSystem\AdminBundle\Entity\SettingsValue", inversedBy="settingsGroups",cascade={"persist"},orphanRemoval=true)
     * @ORM\JoinTable(name="settings_groups")
     **/
    private $settings;

    public function __construct()
    {
        $this->settings = new ArrayCollection();
    }

    /**
     * @return SettignsValue[]
     */
    public function getSettings()
    {
        return $this->settings;
    }


    public function addSetting($setting)
    {
        var_dump($setting); die();
        if(!$this->settings->contains($setting)){

            $setting->setSettingsGroups($this);
            var_dump($setting); die();
            $this->settings->add($setting);
        }

        return $this;
    }

    public function removeSetting($setting)
    {
        var_dump($setting); die();
        $this->settings->remove($setting);

        return $this;
    }


    public function addSettings($setting)
    {
        var_dump($setting); die();
      if(!$this->settings->contains($setting)){

          $setting->setSettingsGroup($this);
          var_dump($setting); die();
          $this->settings->add($setting);
      }

        return $this;
    }

    public function removeSettings($setting)
    {
        var_dump($setting); die();
        $this->settings->remove($setting);

        return $this;
    }

    public function setSettings($settings)
    {
        die('set');
    }


}
