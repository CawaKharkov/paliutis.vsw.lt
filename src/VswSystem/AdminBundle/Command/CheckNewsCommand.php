<?php
/**
 * Created by PhpStorm.
 * User: cawa
 * Date: 9/8/14
 * Time: 3:57 PM
 */

namespace VswSystem\AdminBundle\Command;


use Facebook\FacebookSession;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;


class CheckNewsCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('facebook:update-news')
            ->setDescription('Parse events in database to check news and post them to FB')
            /*   ->addOption(
                   'redirect-uri',
                   null,
                   InputOption::VALUE_REQUIRED | InputOption::VALUE_IS_ARRAY,
                   'Sets redirect uri for client. Use this option multiple times to set multiple redirect URIs.',
                   null
               )*/

            ->setHelp(
                <<<EOT
                    The <info>%command.name%</info> check's news and post them to FB.
                    <info>php %command.full_name%</info>
EOT
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $contentManager = $this->getContainer()->get('vswsystem.orm.content.manager');
        $blocks = $contentManager->getNotPostedEvents();
        FacebookSession::setDefaultApplication('607180716056430', '85a6f44d312ebc553b1ba8c23693d3e2');
        /* EDIT EMAIL AND PASSWORD */

        /*  $login_email = 'cawa123@mail.ru';
          $login_pass = 'cawa80631039171';
          $ch = curl_init();
          curl_setopt($ch, CURLOPT_URL, 'https://www.facebook.com/login.php');
          curl_setopt($ch, CURLOPT_POSTFIELDS,'email='.urlencode($login_email).'&pass='.urlencode($login_pass).'&login=Login');
          curl_setopt($ch, CURLOPT_POST, 1);
          curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
          curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
          curl_setopt($ch, CURLOPT_HEADER, 0);
          curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
          curl_setopt($ch, CURLOPT_COOKIEJAR, "cookies.txt");
          curl_setopt($ch, CURLOPT_COOKIEFILE, "cookies.txt");
          curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
          curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.3) Gecko/20070309 Firefox/2.0.0.3");
          curl_setopt($ch, CURLOPT_REFERER, "http://www.facebook.com/groups/691492907591516/");
          $page = curl_exec($ch) or die(curl_error($ch));
          file_put_contents('index.html',$page);
          curl_close($ch);
  */

       /* $appid= "607180716056430";
        $code_url = "https://graph.facebook.com/oauth/authorize?username=cawakharkov&password=cawa80631039171&client_id=".$appid."&redirect_uri=".urlencode('http://cawakharkov.mooo.com:81/')."&type=client_cred&display=page&scope=user_photos,publish_stream,read_stream,user_likes";
        var_dump($code_url);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$code_url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION  ,1);
        curl_setopt($ch, CURLOPT_HEADER      ,0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER  ,1);
        $fb_code = curl_exec($ch); // get code
        curl_close($ch);
        var_dump($fb_code);
        die();*/
        $token = $this->getAccessToken('607180716056430', '85a6f44d312ebc553b1ba8c23693d3e2');
        $access_token =$token; //'CAAIoOkblH24BAFSvP5jb43yYVJKNQK1CJZC2VNvU79xBjc4UbjTLcRyPKTFTZCIDUOrkOgzQt8KPEkW3dUv2tq5vqEH38y6HcIcsZBziFpKoyyVXmGYlOAhkrZAGZATFtDiAZCoAVW5ZC5xPTvivyotBZBeQ5COPomyf95ZA2mueZCPjFZCUGrqSsMfb0ooPWSRKvxskyQtMTmx703sTkpxKAOP';
        var_dump($token);

        $graph_url = "https://graph.facebook.com/me/feed";
        $postData = "&message=" . urlencode('test message....')
            . "&access_token=" . $access_token;

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $graph_url);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

        $output = curl_exec($ch);
        var_dump($output);
        curl_close($ch);


    }


    public function postUrl($url, $params)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($params, null, '&'));
        $ret = curl_exec($ch);
        curl_close($ch);
        return $ret;
    }

    public function getAccessToken($app_id, $secret)
    {
        $url = 'https://graph.facebook.com/oauth/access_token';
        $token_params = array(
            "type" => "client_cred",
            "client_id" => $app_id,
            "client_secret" => $secret
        );
        return str_replace('access_token=', '', $this->postUrl($url, $token_params));
    }

}
