<?php

namespace VswSystem\AdminBundle\Controller;

use VswSystem\AdminBundle\Controller\AbstractController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;
use VswSystem\CmsBundle\Entity\NewsContentPage as Page;

/**
 * @Route("/admin/news_page")
 */
class NewsPageController extends AbstractController
{

    /**
     * @Route("/" , name="admin_news_page")
     * @Security("has_role('ROLE_ADMIN')")
     * @Template()
     */
    public function indexAction()
    {
        $pages = $this->getPagesManager()->getNewsPages();
        return ['pages' => $pages];
    }

    /**
     * @Route("/create" , name="admin_news_page_create")
     * @Security("has_role('ROLE_ADMIN')")
     * @Template()
     */
    public function createAction(Request $request)
    {
        $pageForm = $this->createForm('admin_news_page_edit', new Page(), ['show_legend' => false]);
        $pageForm->handleRequest($request);

        $isCreated = false;
        if ($pageForm->isValid()) {
            $page = $this->getPagesManager()->save($pageForm->getData(), true);
            $this->get('session')->getFlashBag()->add(
                    'success', 'News\Events page: ' . $page->getName() . ' have been created!');
            $isCreated = true;
        }

        return $isCreated
                ? $this->redirect($this->generateUrl('admin_news_page'))
                : ['pageForm' => $pageForm->createView()];
    }

    /**
     * @Route("/edit/{id}" , name="admin_news_page_edit")
     * @ParamConverter("page", class="VswSystemCmsBundle:NewsContentPage")
     * @Security("has_role('ROLE_ADMIN')")
     * @Template()
     */
    public function editAction(Request $request, $page)
    {
        $pageForm = $this->createForm('admin_news_page_edit', $page, ['show_legend' => false]);
        $pageForm->handleRequest($request);

        $isCreated = false;
        if ($pageForm->isValid()) {
            $page = $this->getPagesManager()->save($pageForm->getData(), true);
            $this->get('session')->getFlashBag()->add(
                    'success', 'New\Events page: ' . $page->getName() . ' have been updated!');
            $isCreated = true;
        }

        return $isCreated
                ? $this->redirect($this->generateUrl('admin_news_page'))
                : ['pageForm' => $pageForm->createView()];
    }


    /**
     * @Route("/remove/{id}" , name="admin_news_page_remove")
     * @Security("has_role('ROLE_SUPER_ADMIN')")
     * @ParamConverter("page", class="VswSystemCmsBundle:NewsContentPage")
     * @Template()
     */
    public function removeAction(Request $request,Page $page)
    {
        $this->getPagesManager()->remove($page);
        $this->get('session')->getFlashBag()->add(
                'info', 'News\Events page ' . $page->getName() . ' have been removed!');
        return $this->redirect($request->headers->get('referer'));
    }

}
