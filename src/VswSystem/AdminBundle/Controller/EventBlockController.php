<?php

namespace VswSystem\AdminBundle\Controller;

use VswSystem\AdminBundle\Controller\AbstractController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;
use VswSystem\CmsBundle\Entity\EventBlock;


/**
 * @Route("/admin/content/event")
 */
class EventBlockController extends AbstractController
{

    /**
     * @Route("/" , name="admin_event_block")
     * @Security("has_role('ROLE_ADMIN')")
     * @Template()
     */
    public function indexAction(Request $request)
    {
        $sortField = $request->get('sort', null);
        $sortDirection = $request->get('direction', null);

        $blocks = $this->getContentManager()->getEventBlocksSorted(['field'=>$sortField,'direction'=>$sortDirection]);


        $paginator = $this->get('knp_paginator');

        $pagination = $paginator->paginate(
            $blocks,
            $request->query->get('page', 1) /*page number*/,
            10/*limit per page*/
        );

        return ['pagination' => $pagination];
    }

    /**
     * @Route("/create" , name="admin_event_block_create")
     * @Security("has_role('ROLE_SUPER_ADMIN')")
     * @Template()
     */
    public function createAction(Request $request)
    {
        $blockForm = $this->createForm('admin_event_block_edit', new EventBlock, ['show_legend' => false,
            'attr'=>['submit'=>true]]);
        $blockForm->handleRequest($request);

        $isCreated = false;
        if ($blockForm->isValid()) {
            $block = $this->getContentManager()->save($blockForm->getData(), true);
            $this->get('session')->getFlashBag()->add(
                    'success', 'Content block was created!');
            $isCreated = true;
        }

        return $isCreated
                ? $this->redirect($this->generateUrl('admin_event_block'))
                : ['blockForm' => $blockForm->createView()];
    }

    /**
     * @Route("/edit/{id}" , name="admin_event_block_edit")
     * @ParamConverter("block", class="VswSystemCmsBundle:EventBlock")
     * @Security("has_role('ROLE_ADMIN')")
     * @Template()
     */
    public function editAction(Request $request, $block)
    {
        $blockForm = $this->createForm('admin_event_block_edit', $block, ['show_legend' => false,
            'attr'=>['submit'=>true]]);
        $blockForm->handleRequest($request);

        $isCreated = false;
        if ($blockForm->isValid()) {
            $block = $this->getContentManager()->save($blockForm->getData(), true);
            $this->get('session')->getFlashBag()->add(
                    'success', 'Content block was updated!');
            $isCreated = true;
        }

        return //$isCreated
                //? $this->redirect($this->generateUrl('admin_event_block'))
               // :
        ['blockForm' => $blockForm->createView()];
    }


    /**
     * @Route("/remove/{id}" , name="admin_event_block_remove")
     * @Security("has_role('ROLE_SUPER_ADMIN')")
     * @ParamConverter("block", class="VswSystemCmsBundle:EventBlock")
     * @Template()
     */
    public function removeAction( Request $request,EventBlock $block)
    {
        $this->getContentManager()->remove($block);
        $this->get('session')->getFlashBag()->add(
            'info', 'Block ' . $block->getId() . ' have been removed!');
        return $this->redirect($request->headers->get('referer'));
    }

}
