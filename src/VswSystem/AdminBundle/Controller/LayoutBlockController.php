<?php

namespace VswSystem\AdminBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use VswSystem\CoreBundle\Entity\LayoutBlock;

/**
 * @Route("/admin/layout/block")
 */
class LayoutBlockController extends AbstractController
{

    /**
     * @Route("/" , name="admin_layout_block")
     * @Security("has_role('ROLE_ADMIN')")
     * @Template()
     */
    public function indexAction(Request $request)
    {
        $sortField = $request->get('sort', null);
        $sortDirection = $request->get('direction', null);
        $filterField = $request->get('filterField', null);
        $filterValue = $request->get('filterValue', null);

        $blocks = $this->getService()->findAll();

        $paginator = $this->get('knp_paginator');

        $pagination = $paginator->paginate(
            $blocks,
            $request->query->get('page', 1) /*page number*/,
            10/*limit per page*/
        );

        return ['pagination' => $pagination];
    }

    /**
     * @Route("/create" , name="admin_layout_block_create")
     * @Security("has_role('ROLE_ADMIN')")
     * @Template()
     */
    public function createAction(Request $request)
    {
        $blockForm = $this->createForm('admin_layout_block_edit',$this->getService()->create(), ['show_legend' => false]);
        $blockForm->handleRequest($request);

        $isCreated = false;
        if ($blockForm->isValid()) {
            $block = $this->getService()->save($blockForm->getData(), true);
            $this->addFlash(
                'success', 'Block ' . $block->getName() . ' have been updated!');
            $isCreated = true;
        }

        return $isCreated
            ? $this->redirectToRoute('admin_layout_block')
            : ['blockForm' => $blockForm->createView()];
    }

    /**
     * @Route("/edit/{id}" , name="admin_layout_block_edit")
     * @ParamConverter("block", class="VswSystemCoreBundle:LayoutBlock")
     * @Security("has_role('ROLE_ADMIN')")
     * @Template()
     */
    public function editAction(Request $request, LayoutBlock $block)
    {
        $blockForm = $this->createForm('admin_layout_block_edit', $block, ['show_legend' => false]);
        $blockForm->handleRequest($request);

        $isCreated = false;
        if ($blockForm->isValid()) {
            $block = $this->getService()->save($blockForm->getData(), true);
            $this->addFlash(
                'info', 'Block ' . $block->getName() . ' have been updated!');
            $isCreated = true;
        }

        /*return $isCreated
            ? $this->redirect($this->generateUrl('admin_text_page'))
            : ['pageForm' => $pageForm->createView()];*/
        return ['blockForm' => $blockForm->createView()];
    }


    /**
     * @Route("/remove/{id}" , name="admin_layout_block_remove")
     * @Security("has_role('ROLE_SUPER_ADMIN')")
     * @ParamConverter("block", class="VswSystemCoreBundle:LayoutBlock")
     * @Template()
     */
    public function removeAction(Request $request, LayoutBlock $block)
    {
        $this->getPagesManager()->remove($block);
        $this->addFlash(
            'warn', 'Block ' . $block->getName() . ' have been removed!');
        return $this->redirect($request->headers->get('referer'));
    }

    /**
     * @return \VswSystem\CoreBundle\Layout\LayoutBlockService
     */
    protected function getService()
    {
        return $this->get('vswsystem.orm.service.layout_block');
    }
}
