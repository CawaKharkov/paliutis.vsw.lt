<?php

namespace VswSystem\AdminBundle\Controller;

use VswSystem\AdminBundle\Controller\AbstractController as AdminAbstractController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;
use VswSystem\AdminBundle\Entity\SettingsGroup;

/**
 * SettingsGroup controller.
 *
 * @Route("/admin/settings/groups")
 */
class SettingsGroupController extends AdminAbstractController
{

    /**
     * Lists all SettingsGroup entities.
     *
     * @Route("/", name="admin_settings_groups")
     * @Security("has_role('ROLE_ADMIN')")
     * @Template()
     */
    public function indexAction()
    {
        $settings = $this->getSystemManager()->getSettingsGroups();
        return array(
            'groups' => $settings,
        );
    }


    /**
     * @Route("/create" , name="admin_settings_group_create")
     * @Security("has_role('ROLE_ADMIN')")
     * @Template()
     */
    public function createAction(Request $request)
    {
        $settingsForm = $this->createForm('admin_settings_group_edit', new SettingsGroup(), ['show_legend' => false]);
        $settingsForm->handleRequest($request);

        $isCreated = false;
        if ($settingsForm->isValid()) {
            $setting = $this->getSystemManager()->save($settingsForm->getData(), true);
            $this->get('session')->getFlashBag()->add(
                'success', 'Setting group: ' . $setting->getName() . ' have been created!');
            $isCreated = true;
        }

        return $isCreated
            ? $this->redirect($this->generateUrl('admin_settings_groups'))
            : ['settingsForm' => $settingsForm->createView()];
    }

    /**
     * @Route("/edit/{id}" , name="admin_settings_group_edit")
     * @ParamConverter("setting", class="VswSystemAdminBundle:SettingsGroup")
     * @Security("has_role('ROLE_ADMIN')")
     * @Template()
     */
    public function editAction(Request $request, $setting)
    {
        $settingsForm = $this->createForm('admin_settings_group_edit', $setting, ['show_legend' => false]);
        $settingsForm->handleRequest($request);

        $isCreated = false;
        if ($settingsForm->isValid()) {
            $settings = $this->getSystemManager()->save($settingsForm->getData(), true);

           /* foreach($settingsForm->getData()->getSettings() as $setting){
                $setting->setSettingsGroup($settings);
                $this->getSystemManager()->save($setting, true);
            }*/


            $this->get('session')->getFlashBag()->add(
                'success', 'Setting group: ' . $settings->getName() . ' have been updated!');
            $isCreated = true;
        }

        return $isCreated
            ? $this->redirect($this->generateUrl('admin_settings_groups'))
            : ['settingsForm' => $settingsForm->createView()];
    }


    /**
     * @Route("/remove/{id}" , name="admin_settings_group_remove")
     * @Security("has_role('ROLE_SUPER_ADMIN')")
     * @ParamConverter("setting", class="VswSystemAdminBundle:SettingsGroup")
     * @Template()
     */
    public function removeAction(Request $request, SettingsGroup $setting)
    {
        $this->getSystemManager()->remove($setting);
        $this->get('session')->getFlashBag()->add(
            'info', 'Settings group ' . $setting->getName() . ' have been removed!');
        return $this->redirect($request->headers->get('referer'));
    }

}
