<?php

namespace VswSystem\AdminBundle\Controller;

use VswSystem\AdminBundle\Controller\AbstractController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;
use VswSystem\CmsBundle\Entity\QuestionContent;

/**
 * @Route("/admin/questions")
 */
class QuestionsController extends AbstractController
{

    /**
     * @Route("/" , name="admin_questions")
     * @Security("has_role('ROLE_ADMIN')")
     * @Template()
     */
    public function indexAction()
    {
        $questions = $this->getContentManager()->getQuestions();
        return ['questions' => $questions];
    }

    /**
     * @Route("/create" , name="admin_question_create")
     * @Security("has_role('ROLE_ADMIN')")
     * @Template()
     */
    public function createAction(Request $request)
    {
        $questionForm = $this->createForm('admin_question_content_edit', new QuestionContent(), ['show_legend' => false,
            'attr'=>['submit'=>true]]);

        $questionForm->handleRequest($request);

        $isCreated = false;
        if ($questionForm->isValid()) {
            $question = $this->getPagesManager()->save($questionForm->getData(), true);
            $this->get('session')->getFlashBag()->add(
                    'success', 'Question: ' . $question->getQuestion() . ' have been created!');
            $isCreated = true;
        }

        return $isCreated
                ? $this->redirect($this->generateUrl('admin_questions'))
                : ['questionForm' => $questionForm->createView()];
    }

    /**
     * @Route("/edit/{id}" , name="admin_question_edit")
     * @ParamConverter("question", class="VswSystemCmsBundle:QuestionContent")
     * @Security("has_role('ROLE_ADMIN')")
     * @Template()
     */
    public function editAction(Request $request,QuestionContent $question)
    {
        $questionForm = $this->createForm('admin_question_content_edit', $question, ['show_legend' => false,
            'attr'=>['submit'=>true]]);
        $questionForm->handleRequest($request);

        $isCreated = false;
        if ($questionForm->isValid()) {
            $question = $this->getPagesManager()->save($questionForm->getData(), true);
            $this->get('session')->getFlashBag()->add(
                    'success', 'Question: ' . $question->getQuestion() . ' have been updated!');
            $isCreated = true;
        }

        return $isCreated
                ? $this->redirect($this->generateUrl('admin_questions'))
                : ['questionForm' => $questionForm->createView()];
    }


    /**
     * @Route("/remove/{id}" , name="admin_question_remove")
     * @Security("has_role('ROLE_SUPER_ADMIN')")
     * @ParamConverter("question", class="VswSystemCmsBundle:QuestionContent")
     * @Template()
     */
    public function removeAction(Request $request,QuestionContent $question)
    {
        $this->getPagesManager()->remove($question);
        $this->get('session')->getFlashBag()->add(
                'info', 'Question ' . $question->getName() . ' have been removed!');
        return $this->redirect($request->headers->get('referer'));
    }

}
