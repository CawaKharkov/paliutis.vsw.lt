<?php

namespace VswSystem\AdminBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use VswSystem\CmsBundle\Entity\Donor;

/**
 * @Route("/admin/log")
 */
class LogController extends AbstractController
{

    /**
     * @Route("/text_page" , name="admin_log_pages")
     * @Security("has_role('ROLE_ADMIN')")
     * @Template()
     */
    public function textPageLogAction(Request $request)
    {
        $sortField = $request->get('sort', null);
        $sortDirection = $request->get('direction', null);
        $filterField = $request->get('filterField', null);
        $filterValue = $request->get('filterValue', null);

        $em = $this->getDoctrine()->getManager();
        $repo = $em->getRepository('Gedmo\Loggable\Entity\LogEntry'); // we use default log entry class
        $pages = $this->getPagesManager()->getTextPages();

        $logs = [];
        foreach ($pages as $page) {
            if ($repo->getLogEntries($page)) {
                foreach($repo->getLogEntries($page) as $log){
                    $logs[] = $log;
                }
            }
        }

        $paginator = $this->get('knp_paginator');

        $pagination = $paginator->paginate(
            $logs,
            $request->query->get('page', 1) /*page number*/,
            10/*limit per page*/
        );
        // var_dump($pagination[0]->getData()); die();
        return ['pagination' => $pagination];
    }

    /**
     * @Route("/remove/{id}" , name="admin_log_remove")
     * @Security("has_role('ROLE_SUPER_ADMIN')")
     * @Template()
     */
    public function removeAction(Request $request,$id)
    {

        return $this->redirect($request->headers->get('referer'));
    }


}
