<?php

namespace VswSystem\AdminBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use VswSystem\CmsBundle\Entity\Donor;
use VswSystem\CmsBundle\Entity\Subscriber;

/**
 * @Route("/admin/subscribers")
 */
class SubscribersController extends AbstractController
{

    /**
     * @Route("/" , name="admin_user_subscribers")
     * @Security("has_role('ROLE_ADMIN')")
     * @Template()
     */
    public function indexAction(Request $request)
    {
        $sortField = $request->get('sort', null);
        $sortDirection = $request->get('direction', null);
        $filterField = $request->get('filterField', null);
        $filterValue = $request->get('filterValue', null);

        $subscribers = $this->getSystemUserManager()->getSubscriberRepository()->findAll();

        $paginator = $this->get('knp_paginator');

        $pagination = $paginator->paginate(
            $subscribers,
            $request->query->get('page', 1) /*page number*/,
            10/*limit per page*/
        );

        return ['pagination' => $pagination];
    }

    /**
     * @Route("/remove/{id}" , name="admin_subscriber_remove")
     * @Security("has_role('ROLE_SUPER_ADMIN')")
     * @ParamConverter("subscriber", class="VswSystemCmsBundle:Subscriber")
     * @Template()
     */
    public function removeAction(Request $request, Subscriber $subscriber)
    {
        $this->getSystemUserManager()->remove($subscriber);
        $this->get('session')->getFlashBag()->add(
            'info', 'Subscriber ' . $subscriber->getName() . ' have been removed!');
        return $this->redirect($request->headers->get('referer'));
    }


}
