<?php

namespace VswSystem\AdminBundle\Controller;

use VswSystem\AdminBundle\Controller\AbstractController as AdminAbstractController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;
use VswSystem\CmsBundle\Entity\ContentBlock;


/**
 * @Route("/admin/content/block")
 */
class ContentBlockController extends AdminAbstractController
{

    /**
     * @Route("/" , name="admin_content_block")
     * @Security("has_role('ROLE_ADMIN')")
     * @Template()
     */
    public function indexAction(Request $request)
    {
        $sortField = $request->get('sort', null);
        $sortDirection = $request->get('direction', null);

        $blocks = $this->getContentManager()->getContentBlocksSorted(['field'=>$sortField,'direction'=>$sortDirection]);



        $paginator = $this->get('knp_paginator');

        $pagination = $paginator->paginate(
            $blocks,
            $request->query->get('page', 1) /*page number*/,
            10/*limit per page*/
        );


        return ['pagination' => $pagination];
    }

    /**
     * @Route("/create" , name="admin_content_block_create")
     * @Security("has_role('ROLE_SUPER_ADMIN')")
     * @Template()
     */
    public function createAction(Request $request)
    {
        $blockForm = $this->createForm('admin_content_block_edit', new ContentBlock, ['show_legend' => false]);
        $blockForm->handleRequest($request);

        $isCreated = false;
        if ($blockForm->isValid()) {
            $block = $this->getContentManager()->save($blockForm->getData(), true);
            $this->get('session')->getFlashBag()->add(
                    'success', 'Content block was created!');
            $isCreated = true;
        }

        return $isCreated
                ? $this->redirect($this->generateUrl('admin_content_block'))
                : ['blockForm' => $blockForm->createView()];
    }

    /**
     * @Route("/edit/{id}" , name="admin_content_block_edit")
     * @ParamConverter("block", class="VswSystemCmsBundle:ContentBlock")
     * @Security("has_role('ROLE_ADMIN')")
     * @Template()
     */
    public function editAction(Request $request, $block)
    {
        $blockForm = $this->createForm('admin_content_block_edit', $block, ['show_legend' => false]);
        $blockForm->handleRequest($request);

        $isCreated = false;
        if ($blockForm->isValid()) {
            $block = $this->getContentManager()->save($blockForm->getData(), true);
            $this->get('session')->getFlashBag()->add(
                    'success', 'Content block was updated!');
            $isCreated = true;
        }

        return $isCreated
                ? $this->redirect($this->generateUrl('admin_content_block'))
                : ['blockForm' => $blockForm->createView()];
    }


    /**
     * @Route("/remove/{id}" , name="admin_content_block_remove")
     * @Security("has_role('ROLE_SUPER_ADMIN')")
     * @ParamConverter("block", class="VswSystemCmsBundle:ContentBlock")
     * @Template()
     */
    public function removeAction( Request $request,ContentBlock $block)
    {
        $this->getContentManager()->remove($block);
        $this->get('session')->getFlashBag()->add(
            'info', 'Block ' . $block->getId() . ' have been removed!');
        return $this->redirect($request->headers->get('referer'));
    }

}
