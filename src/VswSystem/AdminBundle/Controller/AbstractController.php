<?php

namespace VswSystem\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;


abstract class AbstractController extends Controller
{

    /**
     * @return \VswSystem\CoreBundle\Manager\ContentManager
     */
    public function getContentManager()
    {
        return $this->get('vswsystem.orm.content.manager');
    }

    /**
     * @return \VswSystem\CoreBundle\Manager\PagesManager
     */
    public function getPagesManager()
    {
        return $this->get('vswsystem.orm.pages.manager');
    }

    /**
     * @return \VswSystem\CoreBundle\Page\Service\AliasService
     */
    public function getAliasService()
    {
        return $this->get('vswsystem.orm.service.alias');
    }

    /**
     * @return \VswSystem\CoreBundle\Manager\SystemManager
     */
    public function getSystemManager()
    {
        return $this->get('vswsystem.orm.system.manager');
    }

    /**
     * @return \VswSystem\CoreBundle\Manager\NavigationManager
     */
    public function getNavigationManager()
    {
        return $this->get('vswsystem.orm.navigation.manager');
    }

    /**
     * @return \VswSystem\CoreBundle\Manager\MediaManager
     */
    public function getMediaManager()
    {
        return $this->get('vswsystem.orm.media.manager');
    }

    /**
     * @return \VswSystem\CoreBundle\Manager\UserManager
     */
    public function getUserManager()
    {
        return $this->get('vswsystem.orm.user.manager');
    }

    /**
     * @return \VswSystem\SecurityBundle\Manager\UserManager
     */
    public function getSystemUserManager()
    {
        return $this->get('vswsystem.orm.user.manager.system');
    }

    /**
     * @return \VswSystem\CoreBundle\Manager\WidgetManager
     */
    public function getWidgetManager()
    {
        return $this->get('vswsystem.orm.widget.manager');
    }

    /**
     * @return \VswSystem\CatalogBundle\Catalog\Service\CategoryService
     */
    protected function getCategoryService()
    {
        return $this->get('vswsystem.orm.service.ctalog.category');
    }

}
