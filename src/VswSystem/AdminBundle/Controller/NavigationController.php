<?php

namespace VswSystem\AdminBundle\Controller;

use VswSystem\AdminBundle\Controller\AbstractController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;
use VswSystem\CmsBundle\Entity\Navigation;

/**
 * @Route("/admin/navigation")
 */
class NavigationController extends AbstractController
{

    /**
     * @Route("/" , name="admin_navigation")
     * @Security("has_role('ROLE_ADMIN')")
     * @Template()
     */
    public function indexAction(Request $request)
    {
        $sortField = $request->get('sort',null);
        $sortDirection = $request->get('direction',null);
        $navigation = $this->getNavigationManager()->getNavigationListSorted(['field'=>$sortField,'direction'=>$sortDirection]);

        $paginator  = $this->get('knp_paginator');

        $pagination = $paginator->paginate(
            $navigation,
            $request->query->get('page', 1)/*page number*/,
            10/*limit per page*/
        );

        return ['links' => $navigation,'pagination' =>$pagination];
    }

    /**
     * @Route("/edit/{id}" , name="admin_navigation_edit")
     * @Security("has_role('ROLE_SUPER_ADMIN')")
     * @ParamConverter("navigation", class="VswSystemCmsBundle:Navigation")
     * @Template()
     */
    public function editAction(Request $request, Navigation $navigation)
    {
        $navigationForm = $this->createForm('admin_navigation_edit', $navigation, ['show_legend' => false]);
        $navigationForm->handleRequest($request);

        $isUpdated = false;
        if ($navigationForm->isValid()) {
            $this->getNavigationManager()->save($navigationForm->getData(), true);
            $isUpdated = true;
        }

        return $isUpdated
            ? $this->redirect($this->generateUrl('admin_navigation'))
            : ['navigationForm' => $navigationForm->createView(), 'navigation' => $navigation];

    }

    /**
     * @Route("/create" , name="admin_navigation_create")
     * @Security("has_role('ROLE_SUPER_ADMIN')")
     * @Template()
     */
    public function createAction(Request $request)
    {
        $navigationForm = $this->createForm('admin_navigation_edit', new Navigation(), ['show_legend' => false]);
        $navigationForm->handleRequest($request);

        $isCreated = false;
        if ($navigationForm->isValid()) {
            $this->getNavigationManager()->save($navigationForm->getData(), true);
            $isCreated = true;
        }

        return $isCreated
            ? $this->redirect($this->generateUrl('admin_navigation'))
            : ['navigationForm' => $navigationForm->createView()];
    }

    /**
     * @Route("/remove/{id}" , name="admin_navigation_remove")
     * @Security("has_role('ROLE_SUPER_ADMIN')")
     * @ParamConverter("navigation", class="VswSystemCmsBundle:Navigation")
     * @Template()
     */
    public function removeAction(Request $request, Navigation $navigation)
    {
        $this->getNavigationManager()->remove($navigation);
        $this->get('session')->getFlashBag()->add(
            'info', 'Navigation ' . $navigation->getId() . ' have been removed!');
        return $this->redirect($request->headers->get('referer'));
    }

}
