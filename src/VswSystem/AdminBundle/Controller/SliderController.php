<?php

namespace VswSystem\AdminBundle\Controller;

use VswSystem\AdminBundle\Controller\AbstractController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;
use VswSystem\CmsBundle\Entity\Slider;

/**
 * @Route("/admin/media/slider")
 */
class SliderController extends AbstractController
{

    /**
     * @Route("/" , name="admin_slider")
     * @Security("has_role('ROLE_ADMIN')")
     * @Template()
     */
    public function indexAction()
    {
        $audit = $this->get('vswsystem.orm.service.text_page')->getPageByAlias('auditai');
        $images = $this->getMediaManager()->getGalleriesGroups();
        $sliders = $this->getMediaManager()->getSliders();
        return ['sliders' => $sliders, 'audit' => $audit];
    }

    /**
     * @Route("/create" , name="admin_slider_create")
     * @Security("has_role('ROLE_SUPER_ADMIN')")
     * @Template()
     */
    public function createAction(Request $request)
    {
        $sliderForm = $this->createForm('admin_slider_edit', new Slider, ['show_legend' => false]);
        $sliderForm->handleRequest($request);

        $isCreated = false;
        if ($sliderForm->isValid()) {
            $slider = $this->getMediaManager()->save($sliderForm->getData(), true);
            $this->get('session')->getFlashBag()->add(
                    'success', 'Slider was created!');
            $isCreated = true;
        }

        return $isCreated
                ? $this->redirect($this->generateUrl('admin_slider'))
                : ['sliderForm' => $sliderForm->createView()];
    }

    /**
     * @Route("/edit/{id}" , name="admin_slider_edit")
     * @ParamConverter("slider", class="VswSystemCmsBundle:Slider")
     * @Security("has_role('ROLE_ADMIN')")
     * @Template()
     */
    public function editAction(Request $request, $slider)
    {

        $sliderForm = $this->createForm('admin_slider_edit', $slider, ['show_legend' => false]);
        $sliderForm->handleRequest($request);

        $isCreated = false;
        if ($sliderForm->isValid()) {
            $slider = $this->getMediaManager()->save($sliderForm->getData(), true);
            $this->get('session')->getFlashBag()->add(
                    'success', 'Slider was updated!');
            $isCreated = true;
        }

        return $isCreated
                ? $this->redirect($this->generateUrl('admin_slider'))
                : ['sliderForm' => $sliderForm->createView()];
    }

    /**
     * @Route("/remove/{id}" , name="admin_slider_remove")
     * @Security("has_role('ROLE_SUPER_ADMIN')")
     * @ParamConverter("menu", class="VswSystemCmsBundle:Slider")
     * @Template()
     */
    public function removeAction(Slider $slider)
    {
        $this->getMediaManager()->remove($slider);
        $this->get('session')->getFlashBag()->add(
                'info', 'Slider ' . $slider->getId() . ' have been removed!');
        return $this->redirect($this->getRequest()->headers->get('referer'));
    }

}
