<?php

namespace VswSystem\AdminBundle\Controller;

use VswSystem\AdminBundle\Controller\AbstractController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Gaufrette\Filesystem;
use Gaufrette\File;

/**
 * @Route("/admin/media")
 */
class MediaController extends AbstractController
{

    /**
     * @Route("/" , name="admin_media")
     * @Security("has_role('ROLE_ADMIN')")
     * @Template()
     */
    public function indexAction()
    {
        $fs = $this->get('knp_gaufrette.filesystem_map')->get('media.filesystem');
        return ['files'=> $fs->keys()];
    }

}
