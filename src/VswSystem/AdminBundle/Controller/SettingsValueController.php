<?php

namespace VswSystem\AdminBundle\Controller;

use VswSystem\AdminBundle\Controller\AbstractController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;
use VswSystem\AdminBundle\Entity\SettingsValue;

/**
 * SettingsValue controller.
 *
 * @Route("/admin/settings")
 */
class SettingsValueController extends AbstractController
{

    /**
     * Lists all SettingsValue entities.
     *
     * @Route("/", name="admin_settings")
     * @Security("has_role('ROLE_ADMIN')")
     * @Template()
     */
    public function indexAction()
    {


        $settings = $this->getSystemManager()->getSettings();
        return array(
            'settings' => $settings,
        );
    }


    /**
     * @Route("/create" , name="admin_settings_create")
     * @Security("has_role('ROLE_ADMIN')")
     * @Template()
     */
    public function createAction(Request $request)
    {
        $settingsForm = $this->createForm('admin_settings_edit', new SettingsValue(), ['show_legend' => false]);
        $settingsForm->handleRequest($request);

        $isCreated = false;
        if ($settingsForm->isValid()) {
            $setting = $this->getSystemManager()->save($settingsForm->getData(), true);
            $this->get('session')->getFlashBag()->add(
                'success', 'Setting value: ' . $setting->getSettingName() . ' have been created!');
            $isCreated = true;
        }

        return $isCreated
            ? $this->redirect($this->generateUrl('admin_settings'))
            : ['settingsForm' => $settingsForm->createView()];
    }

    /**
     * @Route("/edit/{id}" , name="admin_settings_edit")
     * @ParamConverter("setting", class="VswSystemAdminBundle:SettingsValue")
     * @Security("has_role('ROLE_ADMIN')")
     * @Template()
     */
    public function editAction(Request $request, $setting)
    {
        $settingsForm = $this->createForm('admin_settings_edit', $setting, ['show_legend' => false]);
        $settingsForm->handleRequest($request);

        $isCreated = false;
        if ($settingsForm->isValid()) {

            $setting = $this->getSystemManager()->save($settingsForm->getData(), true);
            $this->get('session')->getFlashBag()->add(
                'success', 'Setting value: ' . $setting->getSettingName() . ' have been updated!');
            $isCreated = true;
        }

        return $isCreated
            ? $this->redirect($this->generateUrl('admin_settings'))
            : ['settingsForm' => $settingsForm->createView()];
    }


    /**
     * @Route("/remove/{id}" , name="admin_settings_remove")
     * @Security("has_role('ROLE_SUPER_ADMIN')")
     * @ParamConverter("setting", class="VswSystemAdminBundle:SettingsValue")
     * @Template()
     */
    public function removeAction(Request $request, SettingsValue $setting)
    {
        $this->getSystemManager()->remove($setting);
        $this->get('session')->getFlashBag()->add(
            'info', 'Setting ' . $setting->getSettingName() . ' have been removed!');
        return $this->redirect($request->headers->get('referer'));
    }

}
