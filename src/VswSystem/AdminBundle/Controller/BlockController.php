<?php

namespace VswSystem\AdminBundle\Controller;

use VswSystem\AdminBundle\Controller\AbstractController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;
use VswSystem\CmsBundle\Entity\Block;

/**
 * @Route("/admin/block")
 */
class BlockController extends AbstractController
{

    /**
     * @Route("/" , name="admin_block")
     * @Security("has_role('ROLE_ADMIN')")
     * @Template()
     */
    public function indexAction()
    {
        $blocks = $this->getContentManager()->getBlocks();
        return ['blocks' => $blocks];
    }
    
    
     /**
     * @Route("/create" , name="admin_block_create")
     * @Security("has_role('ROLE_SUPER_ADMIN')")
     * @Template()
     */
    public function createAction(Request $request)
    {
        $blockForm = $this->createForm('admin_block_edit', new Block(), ['show_legend' => false]);
        $blockForm->handleRequest($request);

        $isCreated = false;
        if ($blockForm->isValid()) {
            $menu = $this->getContentManager()->save($blockForm->getData(), true);
            $this->get('session')->getFlashBag()->add(
                    'success', 'Block: ' . $menu->getName() . ' have been created!');
            $isCreated = true;
        }

        return $isCreated
                ? $this->redirect($this->generateUrl('admin_page'))
                : ['blockForm' => $blockForm->createView()];
    }

}
