<?php

namespace VswSystem\AdminBundle\Controller;

use VswSystem\AdminBundle\Controller\AbstractController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use VswSystem\CmsBundle\Entity\Menu;
use VswSystem\CoreBundle\Annotation\FormAnnotation;

/**
 * @Route("/admin/menu")
 */
class MenuController extends AbstractController
{

    /**
     * @Route("/" , name="admin_menu")
     * @Security("has_role('ROLE_ADMIN')")
     * @Template()
     */
    public function indexAction()
    {
        $menus = $this->getNavigationManager()->getMenuList();

        return ['menus' => $menus];
    }

    /**
     * @Route("/edit/{id}" , name="admin_menu_edit")
     * @ParamConverter("menu", class="VswSystemCmsBundle:Menu")
     * @Security("has_role('ROLE_SUPER_ADMIN')")
     * @FormAnnotation()
     * @Template()
     */
    public function editAction(Request $request, $menu)
    {
        $canCreate = $this->get('security.context')->isGranted('ROLE_SUPER_ADMIN');

        $menuForm = $this->createForm('admin_menu_edit', $menu, ['show_legend' => false, 'attr'=>['canCreate' => $canCreate]]);
        $menuForm->handleRequest($request);

        $isUpdated = false;

        if ($menuForm->isValid()) {
            $manager = $this->getNavigationManager();
            $menu = $manager->save($menuForm->getData(), true);
            $this->get('session')->getFlashBag()->add(
                    'info', 'Menu: ' . $menu->getName() . ' have been updated!');
            $isUpdated = true;
        }

        return $isUpdated
                ? $this->redirect($this->generateUrl('admin_menu'))
                : ['menuForm' => $menuForm->createView()];
    }

    /**
     * @Route("/create" , name="admin_menu_create")
     * @Security("has_role('ROLE_ADMIN')")
     * @Template()
     */
    public function createAction(Request $request)
    {
        $canCreate = $this->get('security.context')->isGranted('ROLE_SUPER_ADMIN');

        $menuForm = $this->createForm('admin_menu_edit', new Menu(), ['show_legend' => false, 'attr'=>['canCreate' => $canCreate]]);
        $menuForm->handleRequest($request);

        $isCreated = false;
        if ($menuForm->isValid()) {
            $data = $menuForm->getData();

            $menu = $this->getNavigationManager()->save($data, true);
            $this->get('session')->getFlashBag()->add(
                    'success', 'Menu: ' . $menu->getName() . ' have been created!');
            $isCreated = true;
        }

        return $isCreated
                ? $this->redirect($this->generateUrl('admin_menu'))
                : ['menuForm' => $menuForm->createView()];
    }

    /**
     * @Route("/remove/{id}" , name="admin_menu_remove")
     * @Security("has_role('ROLE_SUPER_ADMIN')")
     * @ParamConverter("menu", class="VswSystemCmsBundle:Menu")
     * @Template()
     */
    public function removeAction(Menu $menu)
    {
        $this->getNavigationManager()->remove($menu);
        $this->get('session')->getFlashBag()->add(
                'info', 'Menu ' . $menu->getId() . ' have been removed!');
        return $this->redirect($this->getRequest()->headers->get('referer'));
    }

    /**
     * @Route("/mainmenu/" , name="admin_navigation_main_menu")
     * @Security("has_role('ROLE_ADMIN')")
     * @Template()
     */
    public function mainMenuAction()
    {
        $mainMenu = $this->getNavigationManager()->getMainMenu();
        return ['mainMenu' => $mainMenu];
    }

}
