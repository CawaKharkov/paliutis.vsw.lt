<?php

namespace VswSystem\AdminBundle\Controller;

use VswSystem\AdminBundle\Controller\AbstractController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;
use VswSystem\CmsBundle\Entity\TextContentPage as Page;

/**
 * @Route("/admin/text_page")
 */
class TextPageController extends AbstractController
{

    /**
     * @Route("/" , name="admin_text_page")
     * @Security("has_role('ROLE_ADMIN')")
     * @Template()
     */
    public function indexAction(Request $request)
    {
        $sortField = $request->get('sort', null);
        $sortDirection = $request->get('direction', null);

        $pages = $this->getPagesManager()->getTextPagesSorted(['field' => $sortField, 'direction' => $sortDirection]);

        $paginator = $this->get('knp_paginator');

        $pagination = $paginator->paginate(
            $pages,
            $request->query->get('page', 1) /*page number*/,
            10/*limit per page*/
        );

        return ['pages' => $pages, 'pagination' => $pagination];
    }

    /**
     * @Route("/create" , name="admin_text_page_create")
     * @Security("has_role('ROLE_ADMIN')")
     * @Template()
     */
    public function createAction(Request $request)
    {
        $pageForm = $this->createForm('admin_text_page_edit', new Page(), ['show_legend' => false]);
        $pageForm->handleRequest($request);

        $isCreated = false;
        if ($pageForm->isValid()) {
            $page = $this->getPagesManager()->save($pageForm->getData(), true);
            $this->get('session')->getFlashBag()->add(
                'success', 'Page: ' . $page->getName() . ' have been created!');
            $isCreated = true;
        }

        return $isCreated
            ? $this->redirect($this->generateUrl('admin_text_page'))
            : ['pageForm' => $pageForm->createView()];
    }

    /**
     * @Route("/edit/{id}" , name="admin_text_page_edit")
     * @ParamConverter("page", class="VswSystemCmsBundle:TextContentPage")
     * @Security("has_role('ROLE_ADMIN')")
     * @Template()
     */
    public function editAction(Request $request, $page)
    {
        $pageForm = $this->createForm('admin_text_page_edit', $page, ['show_legend' => false]);
        $pageForm->handleRequest($request);

        $isCreated = false;
        if ($pageForm->isValid()) {
            $page = $this->getPagesManager()->save($pageForm->getData(), true);
            $this->get('session')->getFlashBag()->add(
                'success', 'Page: ' . $page->getName() . ' have been updated!');
            $isCreated = true;
        }

        /*return $isCreated
            ? $this->redirect($this->generateUrl('admin_text_page'))
            : ['pageForm' => $pageForm->createView()];*/
        return ['pageForm' => $pageForm->createView()];
    }

    /**
     * @Route("/view/{id}" , name="admin_text_page_view_content")
     * @ParamConverter("page", class="VswSystemCmsBundle:TextContentPage")
     * @Security("has_role('ROLE_SUPER_ADMIN')")
     * @Template()
     */
    public function viewContentAction(Request $request, $page)
    {
        return ['page' => $page];
    }

    /**
     * @Route("/remove/{id}" , name="admin_text_page_remove")
     * @Security("has_role('ROLE_SUPER_ADMIN')")
     * @ParamConverter("page", class="VswSystemCmsBundle:TextContentPage")
     * @Template()
     */
    public function removeAction(Request $request, Page $page)
    {
        $this->getPagesManager()->remove($page);
        $this->get('session')->getFlashBag()->add(
            'info', 'Page ' . $page->getName() . ' have been removed!');
        return $this->redirect($request->headers->get('referer'));
    }

}
