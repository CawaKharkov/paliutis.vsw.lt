<?php

namespace VswSystem\AdminBundle\Controller;

use VswSystem\AdminBundle\Controller\AbstractController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;
use VswSystem\CmsBundle\Entity\GalleryContentPage as Page;

/**
 * @Route("/admin/gallery_page")
 */
class GalleryPageController extends AbstractController
{

    /**
     * @Route("/" , name="admin_gallery_page")
     * @Security("has_role('ROLE_ADMIN')")
     * @Template()
     */
    public function indexAction()
    {
        $pages = $this->getPagesManager()->getGalleryPages();
        return ['pages' => $pages];
    }

    /**
     * @Route("/create" , name="admin_gallery_page_create")
     * @Security("has_role('ROLE_ADMIN')")
     * @Template()
     */
    public function createAction(Request $request)
    {
        $pageForm = $this->createForm('admin_gallery_page_edit', new Page(), ['show_legend' => false]);
        $pageForm->handleRequest($request);

        $isCreated = false;
        if ($pageForm->isValid()) {
            $page = $this->getPagesManager()->save($pageForm->getData(), true);
            $this->get('session')->getFlashBag()->add(
                    'success', 'Page: ' . $page->getName() . ' have been created!');
            $isCreated = true;
        }

        return $isCreated
                ? $this->redirect($this->generateUrl('admin_gallery_page'))
                : ['pageForm' => $pageForm->createView()];
    }

    /**
     * @Route("/edit/{id}" , name="admin_gallery_page_edit")
     * @ParamConverter("page", class="VswSystemCmsBundle:GalleryContentPage")
     * @Security("has_role('ROLE_ADMIN')")
     * @Template()
     */
    public function editAction(Request $request, $page)
    {
        $pageForm = $this->createForm('admin_gallery_page_edit', $page, ['show_legend' => false]);
        $pageForm->handleRequest($request);

        $isCreated = false;
        if ($pageForm->isValid()) {
            $page = $this->getPagesManager()->save($pageForm->getData(), true);
            $this->get('session')->getFlashBag()->add(
                    'success', 'Page: ' . $page->getName() . ' have been updated!');
            $isCreated = true;
        }

        return $isCreated
                ? $this->redirect($this->generateUrl('admin_gallery_page'))
                : ['pageForm' => $pageForm->createView()];
    }


    /**
     * @Route("/remove/{id}" , name="admin_gallery_page_remove")
     * @Security("has_role('ROLE_SUPER_ADMIN')")
     * @ParamConverter("page", class="VswSystemCmsBundle:GalleryContentPage")
     * @Template()
     */
    public function removeAction(Request $request,Page $page)
    {
        $this->getPagesManager()->remove($page);
        $this->get('session')->getFlashBag()->add(
                'info', 'Gallery page ' . $page->getName() . ' have been removed!');
        return $this->redirect($request->headers->get('referer'));
    }

}
