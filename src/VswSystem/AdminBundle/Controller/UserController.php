<?php

namespace VswSystem\AdminBundle\Controller;

use VswSystem\AdminBundle\Controller\AbstractController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;
use VswSystem\SecurityBundle\Entity\User;

/**
 * @Route("/admin/user")
 */
class UserController extends AbstractController
{

    /**
     * @Route("/" , name="admin_user")
     * @Security("has_role('ROLE_ADMIN')")
     * @Template()
     */
    public function indexAction()
    {
        $users = $this->getSystemUserManager()->getUsers();
        return ['users' => $users];
    }

    /**
     * @Route("/edit/{id}" , name="admin_user_edit")
     * @Security("has_role('ROLE_SUPER_ADMIN')")
     * @Template()
     */
    public function editAction(Request $request, $id)
    {
        $userRepo = $this->getSystemUserManager()->getUserRepository();
        $user = $userRepo->find($id);

        $userForm = $this->createForm('admin_user_edit', $user, ['show_legend' => false]);

        $userForm->handleRequest($request);
        if ($userForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($userForm->getData());
            $em->flush();

            $this->get('session')->getFlashBag()->add(
                    'warning', 'User ' . $user->getId() . ' have been updated!');
        }


        return ['user' => $user, 'userForm' => $userForm->createView()];
    }

    /**
     * @Route("/create" , name="admin_user_create")
     * @Security("has_role('ROLE_SUPER_ADMIN')")
     * @Template()
     */
    public function createAction(Request $request)
    {
        $userForm = $this->createForm('signup', new User(), ['show_legend' => false]);
        $userForm->handleRequest($request);

        $isCreated = false;
        if ($userForm->isValid()) {
            $this->get('vswsystem.security.registration')->registerUser($userForm->getData(), false);
            $isCreated = true;
        }

        return $isCreated
                ? $this->redirect($this->generateUrl('admin_user'))
                : ['userForm' => $userForm->createView()];
    }

    /**
     * @Route("/remove/{id}" , name="admin_user_remove")
     * @Security("has_role('ROLE_SUPER_ADMIN')")
     * @Template()
     */
    public function removeAction(Request $request,$id)
    {
        $userRepo = $this->getSystemUserManager()->getUserRepository();
        $user = $userRepo->find($id);

        $this->get('session')->getFlashBag()->add(
                'info', 'User ' . $user->getId() . ' have been removed!');
        return $this->redirect($request->headers->get('referer'));
    }

}
