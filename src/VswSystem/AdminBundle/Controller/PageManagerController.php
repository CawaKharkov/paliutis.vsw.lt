<?php

namespace VswSystem\AdminBundle\Controller;

use VswSystem\AdminBundle\Controller\AbstractController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use VswSystem\CmsBundle\Entity\Page;


/**
 * @Route("/admin/pages")
 */
class PageManagerController extends AbstractController
{

    /**
     * @Route("/" , name="admin_pages")
     * @Security("has_role('ROLE_ADMIN')")
     * @Template()
     */
    public function indexAction()
    {
        $textPages = $this->getPagesManager()->getTextPages();
        $galleryPages = $this->getPagesManager()->getGalleryPages();
        $faqPages= $this->getPagesManager()->getFaqPages();
        $newsPages = $this->getPagesManager()->getNewsPages();
        return ['textPages' => $textPages, 'faqPages' => $faqPages, 'galleryPages' => $galleryPages,'newsPages' => $newsPages];
    }
}
