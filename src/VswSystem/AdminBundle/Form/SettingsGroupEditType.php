<?php

namespace VswSystem\AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use VswSystem\CmsBundle\Entity\Navigation;

class SettingsGroupEditType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', 'text', [
                'required' => true, 'attr' => ['class' => 'form-control input-lg']])
            ->add('settings', 'collection', ['type' => 'admin_settings_edit',
                'allow_add' => true, 'allow_delete' => true, 'prototype' => true,
                'widget_add_btn' => ['label' => "Create setting"],
                'show_legend' => false, // dont show another legend of subform
                'by_reference' => true,
                'options' => array(// options for collection fields
                    'by_reference' => false,
                    'attr' => ['collection' => true,'class' => 'test'],
                    'label_render' => false,
                    'widget_remove_btn' => array('label' => "Remove this setting",
                        "icon" => "pencil",
                        'attr' => array('class' => 'btn btn-danger')),
                    'horizontal_input_wrapper_class' => "col-lg-12",
                )])
            ->add('Save', 'submit', ['attr' => ['class' => 'btn btn-primary btn-lg']]);
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'VswSystem\AdminBundle\Entity\SettingsGroup',
        ));
    }

    public function getName()
    {
        return 'admin_settings_group_edit';
    }

}
