<?php

namespace VswSystem\AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use VswSystem\CmsBundle\Entity\SliderImage;
use Symfony\Component\Form\FormEvents;
use VswSystem\AdminBundle\Form\Listner\SettingsDataListener;

class ContentPdfEditType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

       // $listener = new SettingsDataListener($builder->getFormFactory());
   //     $builder->addEventSubscriber($listener);
     //   if ($this->isNew) {
            $builder->add('file', 'file', ['label' => 'File', 'required' => false, 'attr' => ['class' => 'btn']]);
    //    }
        $builder->add('name', 'text', [
            'required' => false, 'label' => 'File name/title', 'attr' => ['class' => 'form-control input-lg',]]);



        //   ->add('Upload', 'submit', ['attr' => ['class' => 'btn btn-primary btn-lg']]);
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'VswSystem\CmsBundle\Entity\ContentPdf',
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'admin_files_content_pdf_edit';
    }


}
