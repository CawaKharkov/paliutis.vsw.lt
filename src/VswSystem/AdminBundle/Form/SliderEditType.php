<?php

namespace VswSystem\AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use VswSystem\AdminBundle\Form\SliderImageEditType;

class SliderEditType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('slides', 'collection', ['type' => new SliderImageEditType(),
                    'allow_add' => true, 'allow_delete' => true, 'prototype' => true,
                    'widget_add_btn' => ['label' => "Create slide"],
                    'show_legend' => false, // dont show another legend of subform
                    'options' => array(// options for collection fields
                        'label_render' => false,
                        'widget_remove_btn' => array('label' => "Remove this slide",
                            "icon" => "pencil",
                            'attr' => array('class' => 'btn btn-danger')),
                        'horizontal_input_wrapper_class' => "col-lg-8",
            )])
                ->add('name', 'text', [
                    'required' => true, 'attr' => ['class' => 'form-control input-lg']])

           ->add('Save', 'submit', ['attr' => ['class' => 'btn btn-primary btn-lg']]);
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'VswSystem\CmsBundle\Entity\Slider',
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'admin_slider_edit';
    }

}
