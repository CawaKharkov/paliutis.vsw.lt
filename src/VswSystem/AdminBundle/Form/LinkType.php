<?php

namespace VswSystem\AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use VswSystem\CmsBundle\Entity\Navigation;

class LinkType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
                ->add('type', 'choice', [
                    'choices' => Navigation::getTypeList(),
                    'required' => true, 'attr' => ['class' => 'form-control input-lg']])
                ->add('title', 'text', ['required' => true, 'attr' => ['class' => 'form-control input-lg',
                        'placeholder' => 'Title']])
                ->add('route', 'text', ['required' => true, 'attr' => ['class' => 'form-control input-lg',
                        'placeholder' => 'Route']])
                ->add('position', 'text', ['required' => false, 'attr' => ['class' => 'form-control input-lg',
                        'placeholder' => 'Postiton']])
            ->add('isHidden', 'checkbox', ['required' => false,]);
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'VswSystem\CmsBundle\Entity\Navigation',
        ));
    }

    public function getName()
    {
        return 'link';
    }

}
