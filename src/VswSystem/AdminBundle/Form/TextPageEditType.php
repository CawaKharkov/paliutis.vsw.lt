<?php

namespace VswSystem\AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use VswSystem\CmsBundle\Entity\TextContentPage as Page;

class TextPageEditType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name', 'text', [
            'required' => true, 'attr' => ['class' => 'form-control input-lg']])
            ->add('title', 'text', [
                'required' => false, 'attr' => ['class' => 'form-control input-lg']])
            ->add('tags', 'text', [
                'required' => false, 'attr' => ['class' => 'form-control input-lg', 'data-role' => 'tagsinput']])
            ->add('alias', 'admin_alias_edit', [
                'cascade_validation' => true,
                'required' => true, 'attr' => ['type' => (new \ReflectionClass($builder->getData()))->getShortName()]])
            ->add('content', 'textarea', [
                'required' => false, 'attr' => ['class' => 'form-control input-lg tinymce',
                    'rows' => 20, 'data-theme' => 'advanced']])
            ->add('block', 'collection', ['type' => 'entity',
                'allow_add' => true, 'allow_delete' => true, 'prototype' => true,
                'widget_add_btn' => ['label' => "Add block"],
                'show_legend' => false, // dont show another legend of subform
                'delete_empty' => true,
                'options' => array( // options for collection fields
                    'class' => 'VswSystemCmsBundle:Block',
                    'property' => 'name',
                    'label_render' => false,
                    'widget_remove_btn' => array('label' => "Remove this block",
                        "icon" => "pencil",
                        'attr' => array('class' => 'btn btn-danger')),
                    'horizontal_input_wrapper_class' => "col-lg-8",
                )])
            ->add('images', 'collection', ['type' => new TextPageImageEditType(),
                'allow_add' => true, 'allow_delete' => true, 'prototype' => true,
                'widget_add_btn' => ['label' => "Add image"],
                'show_legend' => false, // dont show another legend of subform
                'delete_empty' => true,
                'options' => array( // options for collection fields
                    'label_render' => false,
                    'widget_remove_btn' => array('label' => "Remove this image",
                        "icon" => "pencil",
                        'attr' => array('class' => 'btn btn-danger')),
                    'horizontal_input_wrapper_class' => "col-lg-8",
                )])
            ->add('imagePosition', 'choice', ['choices' => Page::getImagePositions(), 'required' => true])
            ->add('showImages', 'checkbox', ['required' => false,])
            ->add('shareable', 'checkbox', ['required' => false,])
            ->add('pdfFromExisting', 'collection', ['type' => 'entity',
                'allow_add' => true, 'allow_delete' => true, 'prototype' => true,
                'widget_add_btn' => ['label' => "Add existing PDF"],
                'show_legend' => false, // dont show another legend of subform
                'delete_empty' => true,
                'options' => array( // options for collection fields
                    'class' => 'VswSystemCmsBundle:ContentPdf',
                    'property' => 'name',
                    'label_render' => false,
                    'widget_remove_btn' => array('label' => "Remove this file",
                        "icon" => "pencil",
                        'attr' => array('class' => 'btn btn-danger')),
                    'horizontal_input_wrapper_class' => "col-lg-8")])
            ->add('pdf', 'collection', ['type' => 'admin_files_content_pdf_edit',
                'allow_add' => true, 'allow_delete' => true, 'prototype' => true,
                'widget_add_btn' => ['label' => "Add pdf file"],
                'show_legend' => false, // dont show another legend of subform
                'delete_empty' => true,
                'options' => array( // options for collection fields
                    'label_render' => false,
                    'widget_remove_btn' => array('label' => "Remove file",
                        "icon" => "pencil",
                        'attr' => array('class' => 'btn btn-danger')),
                    'horizontal_input_wrapper_class' => "col-lg-8",
                )])
            ->add('poll', 'entity', ['class' => 'VswSystemWidgetBundle:Poll', 'property' => 'question','required' => false])
            ->add('Save page', 'submit', ['attr' => ['class' => 'btn btn-primary btn-lg']]);
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'VswSystem\CmsBundle\Entity\TextContentPage',
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'admin_text_page_edit';
    }

}
