<?php

namespace VswSystem\AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use VswSystem\CmsBundle\Entity\Navigation;

class NavigationEditType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
                ->add('type', 'choice', [
                    'choices' => Navigation::getTypeList(),
                    'required' => true, 'attr' => ['class' => 'form-control input-lg']])
                ->add('title', 'text', ['required' => true, 'attr' => ['class' => 'form-control input-lg',
                        'placeholder' => 'Title']])
                ->add('route', 'text', ['required' => true, 'attr' => ['class' => 'form-control input-lg',
                        'placeholder' => 'Route']])
                ->add('position', 'text', ['required' => false, 'attr' => ['class' => 'form-control input-lg',
                        'placeholder' => 'Position']])
                ->add('save', 'submit', ['attr' => ['class' => 'btn btn-success']]);
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'VswSystem\CmsBundle\Entity\Navigation',
            'validation_groups' => ['edit_navigation'],
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'admin_navigation_edit';
    }

}
