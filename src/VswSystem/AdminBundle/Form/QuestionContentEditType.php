<?php

namespace VswSystem\AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class QuestionContentEditType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('image', 'admin_files_content_image_edit', []);

        $builder->add('question', 'text', [
            'required' => true, 'attr' => ['class' => 'input-lg']])
            ->add('answer', 'textarea', [
                'required' => true, 'attr' => ['class' => 'input-lg tinymce',
                    'rows' => 10,'data-theme' => 'advanced','onload' => '']]);

        if (isset($options['attr']['submit'])) {
            $builder->add('Save question', 'submit', ['attr' => ['class' => 'btn btn-primary btn-lg']]);
        }
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'VswSystem\CmsBundle\Entity\QuestionContent',
            'validation_groups' => ['edit_block'],
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'admin_question_content_edit';
    }

}
