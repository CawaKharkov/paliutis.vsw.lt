<?php

namespace VswSystem\AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class AliasEditType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('alias', 'text', [
                'required' => true, 'attr' => ['class' => 'input-lg'],'label'=>false]);

        $builder->add('type', 'hidden', array(
            'data' => $options['attr']['type']
        ));


    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'VswSystem\CmsBundle\Entity\Alias',
            'validation_groups' => ['edit_page'],
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'admin_alias_edit';
    }

}
