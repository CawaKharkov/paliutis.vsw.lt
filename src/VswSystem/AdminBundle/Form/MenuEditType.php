<?php

namespace VswSystem\AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use VswSystem\CmsBundle\Entity\Menu;

class MenuEditType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {


        $builder->add('type', 'choice', [
            'choices' => Menu::getTypeList(),
            'required' => true, 'attr' => ['class' => 'form-control input-lg']])
            ->add('position', 'choice', [
                'choices' => Menu::getPositionList(),
                'required' => false, 'attr' => ['class' => 'form-control input-lg']])
            ->add('name', 'text', [
                'required' => true, 'attr' => ['class' => 'form-control input-lg']])
            ->add('navigationFromExisting', 'collection', ['type' => 'entity',
                'allow_add' => true, 'allow_delete' => true, 'prototype' => true,
                'widget_add_btn' => ['label' => "Add existing link"],
                'show_legend' => false, // dont show another legend of subform
                'delete_empty' => true,
                'options' => array( // options for collection fields
                    'class' => 'VswSystemCmsBundle:Navigation',
                    'property' => 'title',
                    'label_render' => false,
                    'widget_remove_btn' => array('label' => "Remove this link",
                        "icon" => "pencil",
                        'attr' => array('class' => 'btn btn-danger')),
                    'horizontal_input_wrapper_class' => "col-lg-8",
                )]);
        if ($options['attr']['canCreate']) {
            $builder->add('navigation', 'collection', ['type' => new LinkType(),
                'allow_add' => true, 'allow_delete' => true, 'prototype' => true,
                'widget_add_btn' => ['label' => "Create link"],
                'show_legend' => false, // dont show another legend of subform
                'options' => array( // options for collection fields
                    'label_render' => false,
                    'widget_remove_btn' => array('label' => "Remove this link",
                        "icon" => "pencil",
                        'attr' => array('class' => 'btn btn-danger')),
                    'horizontal_input_wrapper_class' => "col-lg-8",
                )]);
        }
        $builder->add('parent', 'entity', array(
            'class' => 'VswSystem\CmsBundle\Entity\Menu',
            'required' => false
        ));

        $builder->add('connectorElement', 'entity', array(
            'class' => 'VswSystem\CmsBundle\Entity\Navigation',
            'property' => 'name',
            'required' => false
        ));

        $builder->add('Save menu', 'submit', ['attr' => ['class' => 'btn btn-primary btn-lg']]);
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'VswSystem\CmsBundle\Entity\Menu',
            // 'validation_groups' => ['edit_menu'],
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'admin_menu_edit';
    }

}
