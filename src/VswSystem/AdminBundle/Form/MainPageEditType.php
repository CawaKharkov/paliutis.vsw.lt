<?php

namespace VswSystem\AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use VswSystem\CmsBundle\Entity\NewsContentPage;


class MainPageEditType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name', 'text', [
            'required' => true, 'attr' => ['class' => 'form-input input-lg']])
            ->add('title', 'text', [
                'required' => false, 'attr' => ['class' => 'form-input input-lg']])
            ->add('alias', 'admin_alias_edit', [
                'label' => false,
                'required' => true, 'attr' => ['type' => (new \ReflectionClass($builder->getData()))->getShortName()]])

            ->add('showImages', 'checkbox', ['required' => false,])
            ->add('shareable', 'checkbox', ['required' => false,])
            ->add('paginatable', 'checkbox', ['required' => false,])
            ->add('questionLayout', 'choice', ['choices' => NewsContentPage::getQuestionLayouts(), 'required' => false])
            ->add('block', 'collection', ['type' => 'entity',
                'allow_add' => true, 'allow_delete' => true, 'prototype' => true,
                'widget_add_btn' => ['label' => "Add block"],
                'show_legend' => false, // dont show another legend of subform
                'delete_empty' => true,
                'options' => array( // options for collection fields
                    'class' => 'VswSystemCmsBundle:Block',
                    'property' => 'name',
                    'label_render' => false,
                    'widget_remove_btn' => array('label' => "Remove this block",
                        "icon" => "pencil",
                        'attr' => array('class' => 'btn btn-danger')),
                    'horizontal_input_wrapper_class' => "col-lg-8",
                )])

            ->add('eventsFromExisting', 'collection', ['type' => 'entity',
                'allow_add' => true, 'allow_delete' => true, 'prototype' => true,
                'widget_add_btn' => ['label' => "Add existing event"],
                'show_legend' => false, // dont show another legend of subform
                'delete_empty' => true,
                'options' => array( // options for collection fields
                    'class' => 'VswSystemCmsBundle:EventBlock',
                    'property' => 'title',
                    'label_render' => false,
                    'widget_remove_btn' => array('label' => "Remove this event",
                        "icon" => "pencil",
                        'attr' => array('class' => 'btn btn-danger')),
                    'horizontal_input_wrapper_class' => "col-lg-8")])

            ->add('events', 'collection', ['type' => 'admin_event_block_edit',
                'allow_add' => true, 'allow_delete' => true,
                'widget_add_btn' => ['label' => "Add event"],
                'show_legend' => false, // dont show another legend of subform
                'delete_empty' => true,
                'options' => array( // options for collection fields
                    'label_render' => false,
                    'widget_remove_btn' => array('label' => "Remove this event",
                        "icon" => "pencil",
                        'attr' => array('class' => 'btn btn-danger')),
                    'horizontal_input_wrapper_class' => "col-lg-8",
                )])
            ->add('Save page', 'submit', ['attr' => ['class' => 'btn btn-primary btn-lg']]);
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'VswSystem\CmsBundle\Entity\MainPage',
            'validation_groups' => ['edit_page'],
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'admin_main_page_edit';
    }

}
