<?php
/**
 * Created by PhpStorm.
 * User: cawa
 * Date: 8/6/14
 * Time: 7:44 PM
 */

namespace VswSystem\AdminBundle\Form\Listner;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormFactoryInterface;

class ImageDataListener implements EventSubscriberInterface
{
    private $factory;

    public function __construct(FormFactoryInterface $factory)
    {

        $this->factory = $factory;
    }

    public static function getSubscribedEvents()
    {
        return array(
            FormEvents::PRE_SET_DATA => 'preSetData',
        );
    }

    public function preSetData(FormEvent $event)
    {

        $data = $event->getData();
        $form = $event->getForm();
        if($data){
            $form->remove('file');
        }


    }
}
