<?php

namespace VswSystem\AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use VswSystem\CmsBundle\Entity\GalleryImageGroup;


class GalleryImageGroupEditType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name', 'text', [
            'required' => true, 'attr' => ['class' => 'form-control input-lg']])
            ->add('title', 'text', [
                'required' => false, 'attr' => ['class' => 'form-control input-lg']])
            ->add('alias', 'admin_alias_edit', [
                'required' => true, 'attr' => ['type' => (new \ReflectionClass($builder->getData()))->getShortName()]])
            ->add('cover', 'admin_files_content_image_edit', [])
            ->add('images', 'collection', ['type' => 'admin_files_galley_page_image_edit',
                'allow_add' => true, 'allow_delete' => true, 'prototype' => true,
                'widget_add_btn' => ['label' => "Add image"],
                'show_legend' => false, // dont show another legend of subform
                'delete_empty' => true,
                'prototype' => true,
                'options' => array( // options for collection fields
                    'label_render' => false,
                    'widget_remove_btn' => array('label' => "Remove this image",
                        "icon" => "pencil",
                        'attr' => array('class' => 'btn btn-danger')),
                    'horizontal_input_wrapper_class' => "col-lg-8",
                )])
            ->add('imagePosition', 'choice', ['choices' => GalleryImageGroup::getImagePositions(), 'required' => true])
            ->add('Save page', 'submit', ['attr' => ['class' => 'btn btn-primary btn-lg']]);
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'VswSystem\CmsBundle\Entity\GalleryImageGroup',
            'validation_groups' => ['edit_page'],
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'admin_gallery_group_edit';
    }

}
