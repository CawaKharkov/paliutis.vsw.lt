<?php

namespace VswSystem\AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use VswSystem\CmsBundle\Entity\ContentBlock;

class ContentBlockEditType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('cover', 'admin_files_content_image_edit', []);


        $builder->add('title', 'text', [
            'required' => false, 'attr' => ['class' => 'form-control input-lg']])
            ->add('shortText', 'textarea', [
                'required' => true, 'attr' => ['class' => 'form-control input-lg']])
            ->add('tags', 'text', [
                'required' => false, 'attr' => ['class' => 'form-control input-lg', 'data-role' => 'tagsinput']])
            ->add('content', 'textarea', [
                'required' => true, 'attr' => ['class' => 'form-control input-lg tinymce',
                    'rows' => 10, 'data-theme' => 'advanced']])
            ->add('shortText', 'textarea', [
                'required' => true, 'attr' => ['class' => 'form-control input-lg',
                    'rows' => 10]])
            ->add('blockSize', 'choice', ['choices' => [0 => 'One', 2 => 'Double'],
                'required' => true, 'attr' => ['class' => 'form-control input-lg']])
            ->add('isMainPage', 'checkbox', ['required' => false,])
            ->add('mainPageBlock', 'choice', ['choices' => ContentBlock::getMainPageBlocks(), 'required' => true])
            ->add('Save block', 'submit', ['attr' => ['class' => 'btn btn-primary btn-lg']]);
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'VswSystem\CmsBundle\Entity\ContentBlock',
            'validation_groups' => ['edit_block'],
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'admin_content_block_edit';
    }

}
