<?php

namespace VswSystem\AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use VswSystem\CmsBundle\Entity\Menu;
use VswSystem\CmsBundle\Entity\Page;

class BlockEditType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name', 'text', [
                    'required' => true, 'attr' => ['class' => 'form-control input-lg']])
                ->add('comment', 'textarea', [
                    'required' => false, 'attr' => ['class' => 'form-control input-lg']])
                ->add('type', 'choice', ['choices' => [1 => 'Block'],
                    'required' => false, 'attr' => ['class' => 'form-control input-lg']])
              
                ->add('Save block', 'submit', ['attr' => ['class' => 'btn btn-primary btn-lg']]);
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'VswSystem\CmsBundle\Entity\Block',
            'validation_groups' => ['edit_block'],
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'admin_block_edit';
    }

}
