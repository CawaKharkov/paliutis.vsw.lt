<?php

namespace VswSystem\AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use VswSystem\CmsBundle\Entity\TextContentPage as Page;
use VswSystem\CoreBundle\Entity\LayoutBlock;

class LayoutBlockEditType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name', 'text', [
            'required' => true, 'attr' => ['class' => 'form-control input-lg']])
            ->add('title', 'text', [
                'required' => false, 'attr' => ['class' => 'form-control input-lg']])
            ->add('url', 'url', [
                'required' => false, 'attr' => ['class' => 'form-control input-lg']])
            ->add('text', 'textarea', [
                'required' => false, 'attr' => ['class' => 'form-control input-lg tinymce',
                    'rows' => 20, 'data-theme' => 'advanced']])
            ->add('cover', 'admin_files_content_image_edit', [])
            ->add('swf', 'admin_files_content_swf_edit', [])
            ->add('contentType', 'choice', ['choices' => LayoutBlock::getContentTypes(), 'required' => true])
            ->add('contentBlock', 'entity',  ['class' => 'VswSystem\CmsBundle\Entity\ContentBlock',
                'property' => 'title','required' => false])
            ->add('eventBlock', 'entity',  ['class' => 'VswSystem\CmsBundle\Entity\EventBlock',
                'property' => 'title','required' => false])
            ->add('visible', 'checkbox', ['required' => false])
            ->add('Save page', 'submit', ['attr' => ['class' => 'btn btn-primary btn-lg']]);
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'VswSystem\CoreBundle\Entity\LayoutBlock',
            'validation_groups' => ['edit_page'],
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'admin_layout_block_edit';
    }

}
