<?php

namespace VswSystem\AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class GalleryPageImageEditType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        //   if ($this->isNew) {
        $builder->add('file', 'file', ['label' => 'Page image', 'required' => false, 'attr' => ['class' => 'btn btn-primari']]);
        //    }
        $builder->add('name', 'text', [
            'required' => true, 'label' => 'Image name', 'attr' => ['class' => 'form-control input-lg',]])
            ->add('caption', 'text', [
                'required' => true, 'label' => 'Image caption', 'attr' => ['class' => 'form-control input-lg',]])
            ->add('link', 'text', [ 'required' => false ])
            
            ->add('visible', 'checkbox', ['required' => false]);
        //   ->add('Upload', 'submit', ['attr' => ['class' => 'btn btn-primary btn-lg']]);
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'VswSystem\CmsBundle\Entity\GalleryPageImage',
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'admin_files_galley_page_image_edit';
    }


}
