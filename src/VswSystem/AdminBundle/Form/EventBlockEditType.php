<?php

namespace VswSystem\AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\DataTransformer\DateTimeToStringTransformer;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;


class EventBlockEditType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('cover', 'admin_files_content_image_edit', [
        //    'horizontal_input_wrapper_class' => 'col-lg-4',
        ]);

        $builder->add('title', 'text', [
            'required' => false, 'attr' => ['class' => 'form-input input-lg']])
            ->add('tags', 'text', [
                'required' => false, 'attr' => ['class' => 'form-control input-lg', 'data-role' => 'tagsinput']])
            ->add('content', 'textarea', [
                'required' => true, 'attr' => ['class' => 'form-control input-lg tinymce',
                    'rows' => 10, 'data-theme' => 'advanced']])
            ->add('isMainPage', 'checkbox', ['required' => false,])
            ->add('showEventDate', 'checkbox', ['required' => false,])
            ->add($builder->create('eventTime', 'text', ['required' => false, 'attr' => ['class' => 'datepicker']])
                ->addViewTransformer(new DateTimeToStringTransformer(null,null,'m/d/Y')));
        if (isset($options['attr']['submit'])) {
            $builder->add('Save block', 'submit', ['attr' => ['class' => 'btn btn-primary btn-lg']]);
        }
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'VswSystem\CmsBundle\Entity\EventBlock',
            'validation_groups' => ['edit_block'],
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'admin_event_block_edit';
    }

}
