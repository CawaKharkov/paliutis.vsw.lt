<?php

namespace VswSystem\AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class SettingsValueEditType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add('settingName', 'text', [
                'required' => true, 'attr' => ['class' => 'form-control input-lg']]);

        $builder->add('settingValue', 'text', [
                'required' => true, 'attr' => ['class' => 'form-control input-lg']]);

        if (!isset($options['attr']['collection'])) {
            $builder->add('Save', 'submit', ['attr' => ['class' => 'btn btn-primary btn-lg']]);
        }

    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'VswSystem\AdminBundle\Entity\SettingsValue',
        ));
    }

    public function getName()
    {
        return 'admin_settings_edit';
    }

}
