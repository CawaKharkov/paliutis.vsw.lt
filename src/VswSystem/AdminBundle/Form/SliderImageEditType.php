<?php

namespace VswSystem\AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use VswSystem\CmsBundle\Entity\SliderImage;
use Symfony\Component\Form\FormEvents;
use VswSystem\AdminBundle\Form\Listner\SettingsDataListener;

class SliderImageEditType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $listener = new SettingsDataListener($builder->getFormFactory());
     //   $builder->addEventSubscriber($listener);
     //   if ($this->isNew) {
            $builder->add('file', 'file', ['label' => 'Slider image', 'required' => false,
                'attr' => ['class' => 'btn btn-primari']]);
    //    }
        $builder->add('name', 'text', [
            'required' => true, 'label' => 'Image name', 'attr' => ['class' => 'form-control input-lg',]])
            ->add('caption', 'text', [
                'required' => false, 'label' => 'Slide caption', 'attr' => ['class' => 'form-control input-lg',]])
            ->add('text', 'textarea', [
                'required' => false, 'attr' => ['class' => 'form-control input-lg']])
            ->add('textColor','choice',['choices' => SliderImage::getColors(),'required'=>true])

        ->add('route', 'text', [
        'required' => false, 'label' => 'Slide route', 'attr' => ['class' => 'form-control input-lg']]);

        $builder ->add('textPageLink', 'entity', array(
            'class' => 'VswSystem\CmsBundle\Entity\TextContentPage',
            'property' => 'name',
            'required' => false
        ));


        //   ->add('Upload', 'submit', ['attr' => ['class' => 'btn btn-primary btn-lg']]);
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'VswSystem\CmsBundle\Entity\SliderImage',
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'admin_files_slide_edit';
    }


}
