<?php

namespace VswSystem\CatalogBundle\Catalog\Service;

use VswSystem\CoreBundle\VswService\AbstractEntityService;

/**
 * Class CatalogItemService
 * @package VswSystem\CatalogBundle\Catalog\Service
 */
class CategoryService extends AbstractEntityService
{
    public function getCategorySorted($settings)
    {
        if (!$settings['field'] || !$settings['direction']) {
            return $this->findAll();
        }
        return $this->getRepository()->findBy([], [$settings['field'] => $settings['direction']]);
    }
} 