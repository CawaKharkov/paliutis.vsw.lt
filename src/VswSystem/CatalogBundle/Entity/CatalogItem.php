<?php

namespace VswSystem\CatalogBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\Validator\Constraints as Assert;
use VswSystem\CmsBundle\Entity\PageImage;
use VswSystem\CmsBundle\Entity\Traits\IdentificationalEntity;
use VswSystem\CmsBundle\Entity\Traits\NamedEntity;
use VswSystem\CmsBundle\Entity\Traits\TaggedEntity;
use Symfony\Component\Validator\Constraints\NotBlank;

/**
 * CatalogItem
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="VswSystem\CatalogBundle\Entity\Repository\CatalogItemRepository")
 * @ORM\HasLifecycleCallbacks()
 * @Gedmo\Loggable()
 */
class CatalogItem
{

    use IdentificationalEntity;
    use NamedEntity;
    use TimestampableEntity;
  //  use TitledEntity;
    use TaggedEntity;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255, nullable=true)
     * @Gedmo\Translatable
     * @NotBlank()
     */
    protected $title;

    /**
     * @var string
     *
     * @ORM\OneToOne(targetEntity="VswSystem\CmsBundle\Entity\Alias",cascade={"persist"})
     * @Gedmo\Versioned
     */
    protected $alias;

    /**
     * @var string
     *
     * @ORM\ManyToMany(targetEntity="Category",cascade={"persist"})
     */
    protected $category;


    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", length=255, nullable=true)
     * @Gedmo\Translatable
     * @Assert\NotBlank()
     */
    protected $description;

    /**
     * @var string
     *
     * @ORM\Column(name="content", type="text", length=255, nullable=true)
     * @Gedmo\Translatable
     * @Assert\NotBlank()
     */
    protected $content;

    /**
     * @var string
     *
     * @ORM\Column(name="size", type="string", length=255, nullable=true)
     * Assert\NotBlank()
     */
    protected $size;


    /**
     * @var string
     *
     * @ORM\Column(name="sizeGb", type="string", length=255, nullable=true)
     */
    protected $sizeGb;

    /**
     * @ORM\ManyToMany(targetEntity="ItemImage",cascade={"persist"})
     */
    protected $images;

    /**
     * @var integer
     *
     * @ORM\Column(name="showImages", type="boolean")
     */
    protected $showImages;


    /**
     * @var string
     *
     * @ORM\Column(name="attributesInner", type="simple_array",nullable=true)
     * Assert\NotBlank()
     */
    protected $attributesInner = [];

    protected $atrInnerIcons = [1 => ['img' => 'icon-up-pvc.png', 'title' => 'Polivinyl chloride'],
        2 => ['img' => 'icon-up-pvc-tpe.png', 'title' => 'Polivinyl chloride/ Thermoplastic elastomer'],
        3 => ['img' => 'icon-sol-pvc.png', 'title' => 'Polivinyl chloride'],
        4 => ['img' => 'icon-sol-pvc-tpe.png', 'title' => 'Polivinyl chloride/Thermoplastic elastomer'],
        5 => ['img' => 'icon-sol-pvc-nt.png', 'title' => 'Polivinyl chloride/ Nitrilrubber'],
        6 => ['img' => 'icon-inn-tricot.png', 'title' => 'Tricot lining/ Trikotažinis pamušalas'],
        7 => ['img' => 'icon-sol-tpe.png', 'title' => 'Thermoplast elastomer/ Termoplastinis elastomeras'],
        8 => ['img' => 'icon-up--tpe.png', 'title' => 'Thermoplast elastomer/ Termoplastinis elastomeras'],
        ];


    //  protected $icons = [1 => 'icon-pvc.png', 2 => 'icon-tpe.png', 3 => 'icon-nt.png', 4 => 'icon-4.png'];
    protected $atrIcons = [1 => ['img' => 'icon-batas1.png', 'title' => 'Water resistant/ Neįgeria ir nepraleidžia vandens'],
        2 => ['img' => 'icon-batas2.png', 'title' => 'Antistatic/ Antistatinis'],
        3 => ['img' => 'icon-batas3.png', 'title' => 'Steel toe caps/ Metalinė noselė'],
        4 => ['img' => 'icon-batas4.png', 'title' => 'Oils and hydracarbons resistant sole/ Atsparus tepalams ir benzinuiv'],
        5 => ['img' => 'icon-batas5.png', 'title' => 'Warm pile/ šiltas įdėklas(kojinė)'],
        6 => ['img' => 'icon-batas6.png', 'title' => 'Energy absorbing heel/ Energijos absorbcija kulno srityje'],
        7 => ['img' => 'icon-batas7.png', 'title' => 'Steel midsoles/ Metalinis pado intarpas(vidpadis)']];

    /**
     * @var string
     *
     * @ORM\Column(name="attributes", type="simple_array",nullable=true)
     * Assert\NotBlank()
     */
    protected $attributes;


    /**
     * @var integer
     *
     * @ORM\Column(name="position", type="smallint")
     */
    protected $imagePosition = 1;
    protected static $imagePositions = [0 => 'TOP', 3 => 'BOTTOM'];


    public function __construct()
    {
        $this->images = new ArrayCollection();
        $this->category = new ArrayCollection();

    }


    public function addImage(PageImage $image)
    {
        $this->images[] = $image;
        return $this;
    }

    public function removeImage(PageImage $image)
    {
        $this->images->removeElement($image);
        return $this;
    }

    public function getImages()
    {
        return $this->images;
    }


    /**
     * @param int $imagePosition
     */
    public function setImagePosition($imagePosition)
    {
        $this->imagePosition = $imagePosition;
    }

    /**
     * @return int
     */
    public function getImagePosition()
    {
        return $this->imagePosition;
    }

    /**
     * @param array $imagePositions
     */
    public static function setImagePositions($imagePositions)
    {
        self::$imagePositions = $imagePositions;
    }

    /**
     * @return array
     */
    public static function getImagePositions()
    {
        return self::$imagePositions;
    }

    public function setTranslatableLocale($locale)
    {
        $this->locale = $locale;
    }


    /**
     * @param int $showImages
     */
    public function setShowImages($showImages)
    {
        $this->showImages = $showImages;
    }

    /**
     * @return int
     */
    public function getShowImages()
    {
        return $this->showImages;
    }

    /**
     * @return string
     */
    public function getAlias()
    {
        return $this->alias;
    }

    /**
     * @param string $alias
     */
    public function setAlias($alias)
    {
        $this->alias = $alias;
    }

    /**
     * @return string
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @param string $category
     */
    public function setCategory($category)
    {
        $this->category = $category;
    }

    public function addCategory($category)
    {
        if (!$this->category->contains($category)) {
            $this->category[] = $category;
        }
        return $this;
    }

    public function removeCategory($category)
    {
        $this->category->removeElement($category);
        return $this;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return string
     */
    public function getSize()
    {
        return $this->size;
    }

    /**
     * @param string $size
     */
    public function setSize($size)
    {
        $this->size = $size;
    }

    /**
     * @return string
     */
    /* public function getLining()
     {
         if($this->lining){
         return $this->icons[$this->lining];}
     }

     /**
      * @param string $lining
      */
    /* public function setLining($lining)
     {
         $this->lining = $lining;
     }

     /**
      * @return string
      */
    /*  public function getOutsole()
      {
          if($this->outsole){
          return $this->icons[$this->outsole];}
      }

      /**
       * @param string $outsole
       */
    /*  public function setOutsole($outsole)
      {
          $this->outsole = $outsole;
      }

      /**
       * @return string
       */
    /*   public function getUpper()
       {
           if ($this->upper) {
               return $this->icons[$this->upper];
           }
       }

       /**
        * @param string $upper
        */
    /*   public function setUpper($upper)
       {
           $this->upper = $upper;
       }

     /**
        * @return string
        */
    public function getAttributes()
    {
        return $this->attributes;
    }


    /**
     * @return string
     */
    public function getAttributesIcons()
    {
        if ($this->attributes) {
            $atr = [];

            foreach ($this->attributes as $attribute) {
                if (!empty($attribute)) {
                    $atr[] = $this->atrIcons[$attribute];
                }

            }

            return $atr;
        }
    }

    /**
     * @param string $attributes
     */
    public function setAttributes($attributes)
    {
        $this->attributes = $attributes;
    }

    /**
     * @return string
     */
    public function getAttributesInner()
    {
        return $this->attributesInner;
    }


    /**
     * @return string
     */
    public function getAttributesInnerIcons()
    {
        if ($this->attributesInner) {
            $atr = [];

            foreach ($this->attributesInner as $attribute) {
                if (!empty($attribute)) {
                    $atr[] = $this->atrInnerIcons[$attribute];
                }

            }

            return $atr;
        }
    }

    /**
     * @param string $attributes
     */
    public function setAttributesInner($attributesInner)
    {
        $this->attributesInner = $attributesInner;
    }


    /**
     * @return string
     */
    public function getSizeGb()
    {
        return $this->sizeGb;
    }

    /**
     * @param string $sizeGb
     */
    public function setSizeGb($sizeGb)
    {
        $this->sizeGb = $sizeGb;
    }

    /**
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param string $content
     */
    public function setContent($content)
    {
        $this->content = $content;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }



}
