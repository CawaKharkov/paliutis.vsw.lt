<?php

namespace VswSystem\CatalogBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use VswSystem\CmsBundle\Entity\ContentImage;
use VswSystem\CmsBundle\Entity\Traits\IdentificationalEntity;
use VswSystem\CmsBundle\Entity\Traits\NamedEntity;
use VswSystem\CmsBundle\Entity\Traits\TitledEntity;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Constraints\NotBlank;


/**
 * Category
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="VswSystem\CatalogBundle\Entity\Repository\CategoryRepository")
 * @ORM\HasLifecycleCallbacks()
 * @Gedmo\Loggable()
 */
class Category
{
    use IdentificationalEntity;
    use NamedEntity;
    use TimestampableEntity;
   // use TitledEntity;



    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255, nullable=true)
     * @Gedmo\Translatable
     * @NotBlank()
     */
    protected $title;


    /**
     * @var ContentImage
     *
     * @ORM\OneToOne(targetEntity="VswSystem\CmsBundle\Entity\ContentImage", cascade={"all"},orphanRemoval=true)
     */
    protected  $cover;

    /**
     * @var ContentImage
     *
     * @ORM\OneToOne(targetEntity="VswSystem\CmsBundle\Entity\ContentImage", cascade={"all"},orphanRemoval=true)
     */
    protected  $icon;


    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", length=255, nullable=true)
     * @Gedmo\Translatable
     * @Assert\NotBlank()
     */
    protected $description;

    /**
     * Set cover
     *
     * @param ContentImage $cover
     * @return ContentBlock
     */
    public function setCover(ContentImage $cover)
    {
        $this->cover = $cover;
        return $this;
    }

    /**
     * Remove cover image
     */
    public function removeCover()
    {
        $this->cover = null;
    }

    /**
     * Get cover
     *
     * @return CoverImage
     */
    public function getCover()
    {
        return $this->cover;
    }

    /**
     * @return ContentImage
     */
    public function getIcon()
    {
        return $this->icon;
    }

    /**
     * @param ContentImage $icon
     */
    public function setIcon($icon)
    {
        $this->icon = $icon;
    }

    /**
     * Remove icon image
     */
    public function removeIcon()
    {
        $this->icon = null;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }


}
