<?php

namespace VswSystem\CatalogBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use VswSystem\CmsBundle\Entity\AbstractEntity\File;

/**
 * ItemImage
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="VswSystem\CatalogBundle\Entity\Repository\ItemImageRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class ItemImage extends File
{
    protected $uploadPath = 'uploads/images/itemImages';

}
