<?php

namespace VswSystem\CatalogBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;


class CategoryEditType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder->add('title', 'text', [
            'required' => true, 'attr' => ['class' => 'form-input input-lg']])
            ->add('name', 'text', [
                'required' => true, 'attr' => ['class' => 'form-input input-lg']]);
        $builder->add('cover', 'admin_files_content_image_edit', [
            //    'horizontal_input_wrapper_class' => 'col-lg-4',
        ]);
        $builder->add('icon', 'admin_files_content_image_edit', [
            //    'horizontal_input_wrapper_class' => 'col-lg-4',
            'required' => true
        ])
            ->add('description', 'textarea', [
                'required' => false, 'attr' => ['class' => 'form-control input-lg tinymce',
                    'rows' => 20, 'data-theme' => 'advanced']]);

        $builder->add('Save', 'submit', ['attr' => ['class' => 'btn btn-primary btn-lg']]);
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'VswSystem\CatalogBundle\Entity\Category',
            // 'validation_groups' => ['edit_block'],
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'admin_catalog_category_edit';
    }

}
