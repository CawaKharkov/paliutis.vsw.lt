<?php

namespace VswSystem\CatalogBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use VswSystem\CatalogBundle\Entity\CatalogItem;


class CatalogItemEditType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('images', 'collection', ['type' => 'admin_files_catalog_item_image_edit',
            'allow_add' => true, 'allow_delete' => true, 'prototype' => true,
            'widget_add_btn' => ['label' => "Add image"],
            'show_legend' => false, // dont show another legend of subform
            'delete_empty' => true,
            'prototype' => true,
            'options' => array( // options for collection fields
                'label_render' => false,
                'widget_remove_btn' => array('label' => "Remove this image",
                    "icon" => "pencil",
                    'attr' => array('class' => 'btn btn-danger')),
                'horizontal_input_wrapper_class' => "col-lg-8",
            )])
            ->add('alias', 'admin_alias_edit', [
                'required' => true, 'attr' => ['type' => (new \ReflectionClass($builder->getData()))->getShortName()]])
            ->add('description', 'textarea', [
                'required' => false, 'attr' => ['class' => 'form-control input-lg tinymce',
                    'rows' => 20, 'data-theme' => 'advanced']])
            ->add('content', 'textarea', [
                'required' => false, 'attr' => ['class' => 'form-control input-lg tinymce',
                    'rows' => 20, 'data-theme' => 'advanced']]);

        $builder->add('title', 'text', [
            'required' => false, 'attr' => ['class' => 'form-input input-lg']])
            ->add('name', 'text', [
                'required' => false, 'attr' => ['class' => 'form-input input-lg']])
            ->add('size', 'text', [
                'required' => false, 'attr' => ['class' => 'form-input input-lg']])
            ->add('sizeGb', 'text', [
                'required' => false, 'attr' => ['class' => 'form-input input-lg']])
            ->add('tags', 'text', [
                'required' => false, 'attr' => ['class' => 'form-control input-lg', 'data-role' => 'tagsinput']])
            ->add('category', 'entity', ['class' => 'VswSystemCatalogBundle:Category',
                'property' => 'title', 'required' => true,
                'expanded' => true,
                'multiple' => true])
            /*  ->add('upper', 'choice', ['choices' => [0 => 'PVC', 1 => 'TPE', 2 => 'NT', 3 => 'Tricot lining'], 'required' => true])
              ->add('outsole', 'choice', ['choices' => [0 => 'PVC', 1 => 'TPE', 2 => 'NT', 3 => 'Tricot lining'], 'required' => true])
              ->add('lining', 'choice', ['choices' => [0 => 'PVC', 1 => 'TPE', 2 => 'NT', 3 => 'Tricot lining'], 'required' => true])*/
            ->add('attributes', 'choice', ['choices' =>
                [1 => 'Water resistant', 2 => 'Antistatic', 3 => 'Steel toe cap',
                    4 => 'Oils and hydrocarbons resistant sloe', 5 => 'Warm pile',
                    6 => 'Energy absorbing heels', 7 => 'Steel midsole'],
                'expanded' => true,
                'multiple' => true,
                'required' => false])
            ->add('attributesInner', 'choice', ['choices' =>
                [1 => 'Upper PVC', 2 => 'Upper PVC\TPE', 7 => 'Upper TPE',  7 => 'Upper TPE',  3 => 'Outsole PVC',
                    4 => 'Outsole PVC\TPE', 5 => 'Outsole PVC\NT',8 => 'Outsole TPE', 8 => 'Outsole TPE',
                    6 => 'Inner tricot'],
                'expanded' => true,
                'multiple' => true,
                'required' => false])
            ->add('imagePosition', 'choice', ['choices' => CatalogItem::getImagePositions(), 'required' => true])
            ->add('showImages', 'checkbox', ['required' => false,]);
        $builder->add('Save', 'submit', ['attr' => ['class' => 'btn btn-primary btn-lg']]);
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'VswSystem\CatalogBundle\Entity\CatalogItem',
            // 'validation_groups' => ['edit_block'],
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'admin_catalog_item_edit';
    }

}
