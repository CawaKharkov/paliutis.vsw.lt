<?php

namespace VswSystem\CatalogBundle\Controller;

use VswSystem\CatalogBundle\Controller\AbstractController as CatalogAbstractController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;
use VswSystem\CmsBundle\Entity\TextContentPage as Page;

/**
 * @Route("/admin/item")
 */
class AdminItemController extends CatalogAbstractController
{

    /**
     * @Route("/" , name="admin_catalog_items")
     * @Security("has_role('ROLE_ADMIN')")
     * @Template()
     */
    public function indexAction(Request $request)
    {
        $sortField = $request->get('sort', null);
        $sortDirection = $request->get('direction', null);

        $items = $this->getItemService()->getItemsSorted(['field' => $sortField, 'direction' => $sortDirection]);;

        $paginator = $this->get('knp_paginator');

        $pagination = $paginator->paginate(
            $items,
            $request->query->get('page', 1) /*page number*/,
            10/*limit per page*/
        );

        return ['items' => $items, 'pagination' => $pagination];
    }

    /**
     * @Route("/create" , name="catalog_item_create")
     * @Security("has_role('ROLE_ADMIN')")
     * @Template()
     */
    public function createAction(Request $request)
    {
        $itemForm = $this->createForm('admin_catalog_item_edit',
            $this->getItemService()->create()
            , ['show_legend' => false]);
        $itemForm->handleRequest($request);

        $isCreated = false;
        if ($itemForm->isValid()) {
            $item = $this->getItemService()->save($itemForm->getData(), true);
            $this->addFlash(
                'success', 'Item: ' . $item->getName() . ' have been created!');
            $isCreated = true;
        }

        return $isCreated
            ? $this->redirectToRoute('admin_catalog_items')
            : ['itemForm' => $itemForm->createView()];
    }

    /**
     * @Route("/edit/{id}" , name="catalog_item_edit")
     * @ParamConverter("item", class="VswSystemCatalogBundle:CatalogItem")
     * @Security("has_role('ROLE_ADMIN')")
     * @Template()
     */
    public function editAction(Request $request, $item)
    {
        $itemForm = $this->createForm('admin_catalog_item_edit', $item, ['show_legend' => false]);
        $itemForm->handleRequest($request);

        $isCreated = false;
        if ($itemForm->isValid()) {
            $item = $this->getItemService()->save($itemForm->getData(), true);
            $this->addFlash(
                'success', 'Item: ' . $item->getName() . ' have been updated!');
            $isCreated = true;
        }

        /*return $isCreated
            ? $this->redirect($this->generateUrl('admin_text_page'))
            : ['pageForm' => $pageForm->createView()];*/
        return ['itemForm' => $itemForm->createView()];
    }


    /**
     * @Route("/remove/{id}" , name="catalog_item_remove")
     * @Security("has_role('ROLE_SUPER_ADMIN')")
     * @ParamConverter("page", class="VswSystemCmsBundle:TextContentPage")
     * @Template()
     */
    public function removeAction(Request $request, Page $page)
    {
        $this->getPagesManager()->remove($page);
        $this->addFlash(
            'info', 'Page ' . $page->getName() . ' have been removed!');
        return $this->redirect($request->headers->get('referer'));
    }

}
