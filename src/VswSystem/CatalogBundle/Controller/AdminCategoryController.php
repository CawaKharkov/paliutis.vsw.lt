<?php

namespace VswSystem\CatalogBundle\Controller;

use VswSystem\CatalogBundle\Controller\AbstractController as CatalogAbstractController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;
use VswSystem\CmsBundle\Entity\TextContentPage as Page;

/**
 * @Route("/admin/category")
 */
class AdminCategoryController extends CatalogAbstractController
{

    /**
     * @Route("/" , name="admin_category")
     * @Security("has_role('ROLE_ADMIN')")
     * @Template()
     */
    public function indexAction(Request $request)
    {
        $sortField = $request->get('sort', null);
        $sortDirection = $request->get('direction', null);

        $categories = $this->getCategoryService()->getCategorySorted(['field' => $sortField, 'direction' => $sortDirection]);

        $paginator = $this->get('knp_paginator');

        $pagination = $paginator->paginate(
            $categories,
            $request->query->get('page', 1) /*page number*/,
            10/*limit per page*/
        );

        return ['categories' => $categories, 'pagination' => $pagination];
    }

    /**
     * @Route("/create" , name="admin_category_create")
     * @Security("has_role('ROLE_ADMIN')")
     * @Template()
     */
    public function createAction(Request $request)
    {
        $categoryFrom = $this->createForm('admin_catalog_category_edit',$this->getCategoryService()->create(), ['show_legend' => false]);
        $categoryFrom->handleRequest($request);

        $isCreated = false;
        if ($categoryFrom->isValid()) {
            $category = $this->getCategoryService()->save($categoryFrom->getData(), true);
            $this->addFlash(
                'success', 'Category: ' . $category->getName() . ' have been created!');
            $isCreated = true;
        }

        return $isCreated
            ? $this->redirectToRoute('admin_category')
            : ['categoryFrom' => $categoryFrom->createView()];
    }

    /**
     * @Route("/edit/{id}" , name="admin_category_edit")
     * @ParamConverter("category", class="VswSystemCatalogBundle:Category")
     * @Security("has_role('ROLE_ADMIN')")
     * @Template()
     */
    public function editAction(Request $request, $category)
    {
        $categoryFrom = $this->createForm('admin_catalog_category_edit', $category, ['show_legend' => false]);
        $categoryFrom->handleRequest($request);

        $isCreated = false;
        if ($categoryFrom->isValid()) {
            $category = $this->getCategoryService()->save($categoryFrom->getData(), true);
            $this->addFlash(
                'success', 'Category: ' . $category->getName() . ' have been updated!');
            $isCreated = true;
        }

        /*return $isCreated
            ? $this->redirect($this->generateUrl('admin_text_page'))
            : ['pageForm' => $pageForm->createView()];*/
        return ['categoryFrom' => $categoryFrom->createView()];
    }


    /**
     * @Route("/remove/{id}" , name="admin_category_remove")
     * @Security("has_role('ROLE_SUPER_ADMIN')")
     * @ParamConverter("category", class="VswSystemCatalogBundle:Category")
     * @Template()
     */
    public function removeAction(Request $request, $category)
    {
        $this->getCategoryService()->remove($category,true);
        $this->addFlash(
            'info', 'Category ' . $category->getName() . ' have been removed!');
        return $this->redirect($request->headers->get('referer'));
    }

}
