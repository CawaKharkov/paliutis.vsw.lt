<?php

namespace VswSystem\CatalogBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use VswSystem\CatalogBundle\Controller\AbstractController as CatalogAbstractController;

/**
 * @Route("/category")
 */
class CategoryController extends CatalogAbstractController
{

    /**
     * @Route("/" , name="catalog_category")
     * @Security("has_role('ROLE_ADMIN')")
     * @Template()
     */
    public function indexAction(Request $request)
    {
        $sortField = $request->get('sort', null);
        $sortDirection = $request->get('direction', null);

        $pages = $this->getPagesManager()->getTextPagesSorted(['field' => $sortField, 'direction' => $sortDirection]);

        $paginator = $this->get('knp_paginator');

        $pagination = $paginator->paginate(
            $pages,
            $request->query->get('page', 1) /*page number*/,
            10/*limit per page*/
        );

        return ['pages' => $pages, 'pagination' => $pagination];
    }

    /**
     * @Route("/{name}" , name="category_view")
     * @ParamConverter("category", class="VswSystemCatalogBundle:Category", options={"mapping": {"name": "name"}})
     * @Template()
     */
    public function viewAction(Request $request, $category)
    {
        $sortField = $request->get('sort', null);
        $sortDirection = $request->get('direction', null);

        $items = $this->getItemService()->getRepository()->findAllByCategory($category->getName());

        $paginator = $this->get('knp_paginator');

        $pagination = $paginator->paginate(
            $items,
            $request->query->get('page', 1) /*page number*/,
            10/*limit per page*/
        );

        return ['category' => $category, 'items' => $items];
    }
}
