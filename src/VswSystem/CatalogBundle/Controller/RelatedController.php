<?php

namespace VswSystem\CatalogBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use VswSystem\CatalogBundle\Controller\AbstractController as CatalogAbstractController;

/**
 * @Route("/related")
 */
class RelatedController extends CatalogAbstractController
{

    /**
     * @Route("/{id}" , name="related_items")
     * @Template()
     */
    public function indexAction(Request $request)
    {
        return [];
    }

}
