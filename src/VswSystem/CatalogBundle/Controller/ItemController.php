<?php

namespace VswSystem\CatalogBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use VswSystem\CatalogBundle\Controller\AbstractController as CatalogAbstractController;

/**
 * @Route("/item")
 */
class ItemController extends CatalogAbstractController
{

    /**
     * @Route("/" , name="catalog_items")
     * @Security("has_role('ROLE_ADMIN')")
     * @Template()
     */
    public function indexAction(Request $request)
    {
        $sortField = $request->get('sort', null);
        $sortDirection = $request->get('direction', null);

        $pages = $this->getPagesManager()->getTextPagesSorted(['field' => $sortField, 'direction' => $sortDirection]);

        $paginator = $this->get('knp_paginator');

        $pagination = $paginator->paginate(
            $pages,
            $request->query->get('page', 1) /*page number*/,
            10/*limit per page*/
        );

        return ['pages' => $pages, 'pagination' => $pagination];
    }

    /**
     * @Route("/{alias}" , name="item_view")
     * @ParamConverter("item", class="VswSystemCatalogBundle:CatalogItem",  options={"repository_method" = "findByAlias"})
     * @Template()
     */
    public function viewAction(Request $request, $item)
    {
        $new_path = null;
        $em = $this->getDoctrine()->getEntityManager();
        $connection = $em->getConnection();
        $statement = $connection->prepare("SELECT category_id FROM catalogitem_category WHERE catalogitem_id = :id LIMIT 1");
        $statement->bindValue('id', $item->getId());
        $statement->execute();
        $results = $statement->fetchAll();
        
        $em = $this->getDoctrine()->getEntityManager();
        $connection = $em->getConnection();
        $statement = $connection->prepare("SELECT ContentImage.path FROM ContentImage LEFT JOIN Category ON Category.icon_id = ContentImage.id WHERE Category.id = :id LIMIT 1");
        $statement->bindValue('id', $results[0]['category_id']);
        $statement->execute();
        $results = $statement->fetchAll();
        if ($results) $new_path =  'uploads/images/contentImages/'.$results[0]['path'];
       // var_dump($new_path);
        
       // die();
        return ['item' => $item, 'icon_path' => $new_path];
    } 


    /**
     * @Route("/new" , name="view_new_utems")
     * @Template()
     */
    public function newAction(Request $request)
    {
        return [];
    }

}
