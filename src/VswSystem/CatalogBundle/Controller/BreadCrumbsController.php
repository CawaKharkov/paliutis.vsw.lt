<?php

namespace VswSystem\CatalogBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use VswSystem\CatalogBundle\Controller\AbstractController as CatalogAbstractController;

/**
 * @Route("/breadcrumbs")
 */
class BreadCrumbsController extends CatalogAbstractController
{

    /**
     * @Route("/" , name="breadcrumbs")
     * @Template()
     */
    public function indexAction(Request $request)
    {
        return [];
    }


}
