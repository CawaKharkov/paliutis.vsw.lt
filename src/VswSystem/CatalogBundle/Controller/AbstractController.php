<?php

namespace VswSystem\CatalogBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;


abstract class AbstractController extends Controller
{

    /**
     * @return \VswSystem\CatalogBundle\Catalog\Service\CatalogItemService
     */
    protected function getItemService()
    {
        return $this->get('vswsystem.orm.service.ctalog.item');
    }

    /**
     * @return \VswSystem\CatalogBundle\Catalog\Service\CategoryService
     */
    protected function getCategoryService()
    {
        return $this->get('vswsystem.orm.service.ctalog.category');
    }


}
