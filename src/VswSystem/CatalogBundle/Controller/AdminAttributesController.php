<?php

namespace VswSystem\CatalogBundle\Controller;

use VswSystem\CatalogBundle\Controller\AbstractController as CatalogAbstractController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;
use VswSystem\CmsBundle\Entity\TextContentPage as Page;

/**
 * @Route("/admin/attributes")
 */
class AdminAttributesController extends CatalogAbstractController
{

    /**
     * @Route("/" , name="admin_catalog_attributes")
     * @Security("has_role('ROLE_ADMIN')")
     * @Template()
     */
    public function indexAction(Request $request)
    {
        $sortField = $request->get('sort', null);
        $sortDirection = $request->get('direction', null);

        $pages = $this->getPagesManager()->getTextPagesSorted(['field' => $sortField, 'direction' => $sortDirection]);

        $paginator = $this->get('knp_paginator');

        $pagination = $paginator->paginate(
            $pages,
            $request->query->get('page', 1) /*page number*/,
            10/*limit per page*/
        );

        return ['pages' => $pages, 'pagination' => $pagination];
    }

    /**
     * @Route("/create" , name="catalog_item_create")
     * @Security("has_role('ROLE_ADMIN')")
     * @Template()
     */
    public function createAction(Request $request)
    {
        $pageForm = $this->createForm('admin_text_page_edit', new Page(), ['show_legend' => false]);
        $pageForm->handleRequest($request);

        $isCreated = false;
        if ($pageForm->isValid()) {
            $page = $this->getPagesManager()->save($pageForm->getData(), true);
            $this->addFlash(
                'success', 'Page: ' . $page->getName() . ' have been created!');
            $isCreated = true;
        }

        return $isCreated
            ? $this->redirectToRoute('admin_text_page')
            : ['pageForm' => $pageForm->createView()];
    }

    /**
     * @Route("/edit/{id}" , name="catalog_item_edit")
     * @ParamConverter("page", class="VswSystemCmsBundle:TextContentPage")
     * @Security("has_role('ROLE_ADMIN')")
     * @Template()
     */
    public function editAction(Request $request, $page)
    {
        $pageForm = $this->createForm('admin_text_page_edit', $page, ['show_legend' => false]);
        $pageForm->handleRequest($request);

        $isCreated = false;
        if ($pageForm->isValid()) {
            $page = $this->getPagesManager()->save($pageForm->getData(), true);
            $this->addFlash(
                'success', 'Page: ' . $page->getName() . ' have been updated!');
            $isCreated = true;
        }

        /*return $isCreated
            ? $this->redirect($this->generateUrl('admin_text_page'))
            : ['pageForm' => $pageForm->createView()];*/
        return ['pageForm' => $pageForm->createView()];
    }


    /**
     * @Route("/remove/{id}" , name="catalog_item_remove")
     * @Security("has_role('ROLE_SUPER_ADMIN')")
     * @ParamConverter("page", class="VswSystemCmsBundle:TextContentPage")
     * @Template()
     */
    public function removeAction(Request $request, Page $page)
    {
        $this->getPagesManager()->remove($page);
        $this->addFlash(
            'info', 'Page ' . $page->getName() . ' have been removed!');
        return $this->redirect($request->headers->get('referer'));
    }

}
