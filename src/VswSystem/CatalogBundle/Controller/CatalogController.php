<?php

namespace VswSystem\CatalogBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use VswSystem\CatalogBundle\Controller\AbstractController as CatalogAbstractController;

/**
 * @Route("/")
 */
class CatalogController extends CatalogAbstractController
{

    /**
     * @Route("/" , name="catalog")
     * @Template()
     */
    public function indexAction(Request $request)
    {
        $categories = $this->getCategoryService()->findAll();
        $audit = $this->get('vswsystem.orm.service.text_page')->getPageByAlias('auditai');

        return ['items' => $categories, 'audit' => $audit];
    }


}
