<?php

namespace VswSystem\CatalogBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use VswSystem\CatalogBundle\Controller\AbstractController as CatalogAbstractController;

/**
 * @Route("/featured")
 */
class FeaturedController extends CatalogAbstractController
{

    /**
     * @Route("/" , name="featured_items")
     * @Template()
     */
    public function indexAction(Request $request)
    {
        return [];
    }

    /**
     * @Route("/features" , name="our_features")
     * @Template()
     */
    public function featuresAction(Request $request)
    {
        return [];
    }


}
