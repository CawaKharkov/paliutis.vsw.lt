<?php

namespace VswSystem\CoreBundle\Annotation\Listner;

use Doctrine\Common\Annotations\Reader; //This thing read annotations
use Symfony\Component\HttpKernel\Event\FilterControllerEvent; //Use essential kernel component
use Symfony\Component\HttpFoundation\Response; // For example I will throw 403, if access denied
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use VswSystem\CoreBundle\Annotation\FormAnnotation;

/**
 * Description of FormAnnotationListner
 *
 * @author cawa
 */
class FormAnnotationListner
{

    private $reader;
    protected $formFactory;
    protected $securityContext;

    public function __construct($reader, $formFactory, $securityContext)
    {
        $this->reader = $reader; //get annotations reader
        $this->formFactory = $formFactory;
        $this->securityContext = $securityContext;
    }

    /**
     * This event will fire during any controller call
     */
    public function onKernelController(FilterControllerEvent $event)
    {

        if (!is_array($controller = $event->getController())) { //return if no controller
            return;
        }

        $object = new \ReflectionObject($controller[0]); // get controller
        $method = $object->getMethod($controller[1]); // get method
        foreach ($this->reader->getMethodAnnotations($method) as $configuration) {
            //Start of annotations reading
            if ($configuration instanceof FormAnnotation) {//Found our annotation
                
            }
        }
    }

}
