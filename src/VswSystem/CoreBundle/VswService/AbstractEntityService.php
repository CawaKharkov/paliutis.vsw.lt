<?php
/**
 * Created by PhpStorm.
 * User: cawa
 * Date: 15.09.14
 * Time: 23:06
 */

namespace VswSystem\CoreBundle\VswService;


use Doctrine\Common\Persistence\ObjectRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping\Entity;
use Object;
use VswSystem\CoreBundle\VswService\Interfaces\EntityServiceInterface;

abstract class AbstractEntityService implements EntityServiceInterface
{
    protected $entity;
    protected $repository;
    protected $em;

    /**
     * Set DI
     * @param string $entity
     * @param ObjectRepository $repository
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * Creates the entity
     * @return Object
     */
    public function create()
    {
        $entityName = $this->getEntity()->getName();
        return new $entityName;
    }

    /**
     * Get entity by id
     * @param $id
     * @return Object
     */
    public function get($id)
    {
        return $this->getManager()->find($this->getEntity()->getName(), $id);
    }

    /**
     * Find entities by criterias
     * @param $field
     * @param $value
     * @return Object[]
     */
    public function find($field, $value)
    {
        /**
         * @TODO implement DQL
         */
       return $this->getRepository()->findOneBy([$field=>$value]);
    }


    /**
     * Get entity list
     * @return array
     */
    public function findAll()
    {
        return $this->getRepository()->findAll();
    }

    /**
     * Get entity list array
     * @return array
     */
    public function findAllArray()
    {
        return $this->getRepository()->findAllArray();
    }

    /**
     * Remove entity
     * @param Entity
     */
    public function remove($entity, $flush = false)
    {
        $this->getManager()->remove($entity);

        if ($flush) {
            $this->getManager()->flush();
        }
    }


    /**
     * Save entity in databse
     * @param Entity $entity
     * @return Entity
     */
    public function save($entity, $flush = false)
    {
        $this->getManager()->persist($entity);

        if ($flush) {
            $this->getManager()->flush();
        }

        return $entity;
    }

    /**
     * @return \Doctrine\ORM\EntityManager
     */
    public function getManager()
    {
        return $this->em;
    }


    /**
     * Get entity repository
     * @return ObjectRepository
     */
    public function getRepository()
    {
        return $this->repository;
    }

    /**
     * @return string
     */
    public function getEntity()
    {
        return $this->getManager()->getClassMetadata($this->entity);
    }

    /**
     * @param mixed $entity
     */
    public function setEntity($entity)
    {
        $this->entity = $entity;
    }

    /**
     * @param mixed $repository
     */
    public function setRepository(ObjectRepository $repository)
    {
        $this->repository = $repository;
    }


}