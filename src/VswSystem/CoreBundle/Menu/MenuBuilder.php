<?php

namespace VswSystem\CoreBundle\Menu;

use Doctrine\ORM\EntityManagerInterface;
use Knp\Menu\FactoryInterface;
use Symfony\Component\DependencyInjection\ContainerAware;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Security\Core\SecurityContextInterface;

class MenuBuilder extends ContainerAware
{

    protected $factory;
    protected $securityContext = null;

    /**
     *
     * @var \VswSystem\CoreBundle\Manager\NavigationManager
     */
    protected $manager = null;

    /**
     * @param FactoryInterface $factory
     */
    public function __construct(FactoryInterface $factory)
    {
        $this->factory = $factory;
    }

    public function createMainMenu(RequestStack $requestStack)
    {
        $request = $requestStack->getCurrentRequest();
        $menu = $this->factory->createItem('root');

        $menu->addChild('Home', ['route' => 'homepage', 'icon' => 'home']);

        $this->addUserMenu($menu);

        return $menu;
    }

    public function createCampaignMenu(RequestStack $requestStack)
    {
        $request = $requestStack->getCurrentRequest();
        $menu = $this->factory->createItem('root', ['navbar' => true]);

        $menu->addChild('Home', ['route' => 'homepage', 'icon' => 'home']);
        $menu->addChild('Create campaign', ['route' => 'campaign_create', 'icon' => 'plus']);

        $this->addUserMenu($menu);

        return $menu;
    }

    public function createAdminMenu(RequestStack $requestStack)
    {
        $request = $requestStack->getCurrentRequest();
        $menu = $this->factory->createItem('root', ['navbar' => true]);

        //$menu->addChild('Home', ['route' => 'admin', 'icon' => 'home']);

        $userMenu = $menu->addChild('Users', ['icon' => 'user',
            'dropdown' => true,
            'caret' => true]);
        $userMenu->addChild('System users', ['route' => 'admin_user', 'icon' => 'user']);
        $userMenu->addChild('Subscriber', ['route' => 'admin_user_subscribers', 'icon' => 'envelope']);

        $nav = $menu->addChild('Navigation', ['route' => 'admin_menu', 'icon' => 'list',
            'dropdown' => true,
            'caret' => true]);
        $nav->addChild('Menus', ['route' => 'admin_menu', 'icon' => 'list-alt']);
        $nav->addChild('Links', ['route' => 'admin_navigation', 'icon' => 'link']);

        $catalog = $menu->addChild('Catalog', ['icon' => 'shopping-cart',
            'dropdown' => true,
            'caret' => true]);
        $catalog->addChild('Items', ['route' => 'admin_catalog_items', 'icon' => 'th-list']);
        $catalog->addChild('Categories', ['route' => 'admin_category', 'icon' => 'th-large']);
        $catalog->addChild('Attributes', ['route' => 'admin_catalog_attributes', 'icon' => 'tags']);


        $nav = $menu->addChild('Content', ['icon' => 'th',
            'dropdown' => true,
            'caret' => true]);
        $nav->addChild('Content blocks', ['route' => 'admin_content_block', 'icon' => 'tasks']);
        $nav->addChild('Event blocks', ['route' => 'admin_event_block', 'icon' => 'star']);
        $nav->addChild('Questions', ['route' => 'admin_questions', 'icon' => 'question-sign']);
        $nav->addChild('Layout', ['route' => 'admin_layout', 'icon' => 'plus']);
        $nav->addChild('Layout blocks', ['route' => 'admin_layout_block', 'icon' => 'plus']);
        $nav->addChild('Slider', ['route' => 'admin_slider', 'icon' => 'picture']);
        $nav->addChild('Gallery images', ['route' => 'admin_gallery_group', 'icon' => 'picture']);

        $pages = $menu->addChild('Pages', ['icon' => 'book',
            'dropdown' => true,
            'caret' => true]);
        $pages->addChild('All pages', ['route' => 'admin_pages', 'icon' => 'list']);
        $pages->addChild('Text pages', ['route' => 'admin_text_page', 'icon' => 'file']);
        $pages->addChild('Gallery pages', ['route' => 'admin_gallery_page', 'icon' => 'picture']);
        $pages->addChild('Faq\Conact pages', ['route' => 'admin_faq_page', 'icon' => 'question-sign']);
        $pages->addChild('News\Events pages', ['route' => 'admin_news_page', 'icon' => 'volume-up']);

        $settings = $menu->addChild('Settings', ['icon' => 'wrench',
        'dropdown' => true,
        'caret' => true]);
        $settings->addChild('Settings values', ['route' => 'admin_settings', 'icon' => 'wrench']);
        $settings->addChild('Settings groups', ['route' => 'admin_settings_groups', 'icon' => 'wrench']);

        $widgets = $menu->addChild('Widgets', ['icon' => 'book',
            'dropdown' => true,
            'caret' => true]);
        $widgets->addChild('Hall of fame', ['route' => 'admin_widget_hall', 'icon' => 'list']);
//        $widgets->addChild('Fast registration', ['route' => 'admin_widget_fast_registration', 'icon' => 'list']);
        $widgets->addChild('Poll', ['route' => 'admin_widget_poll', 'icon' => 'list']);
        //$this->addUserMenu($menu);

        $log = $menu->addChild('Logs', ['icon' => 'align-justify',
            'dropdown' => true,
            'caret' => true]);
        $log->addChild('Page logs', ['route' => 'admin_log_pages', 'icon' => 'list']);
        //$this->addUserMenu($menu);

        return $menu;
    }

    public function createLangMenu()
    {
        $langMenu = $this->factory->createItem('root', ['pull-right' => true, 'navbar' => true]);

        $profile = $langMenu->addChild('User:' . $this->securityContext->getToken()->getUser()->getUserName(), ['icon' => 'briefcase',
            'dropdown' => true,
            'caret' => true]);
        $profile->addChild('User profile', ['route' => 'admin_user_profile', 'icon' => 'heart-empty']);
        //$profile->addChild('Logout', ['route' => '_user_logout', 'icon' => 'log-out']);

        $dropdown = $langMenu->addChild('Language', ['dropdown' => true,
            'caret' => true, 'icon' => 'th-large']);

        $dropdown->addChild('EN', ['route' => 'lang_en', 'icon' => 'map-marker',
            'routeParameters' => ['lang' => 'en']]);
        $dropdown->addChild('LT', ['route' => 'lang_lt', 'icon' => 'map-marker',
            'routeParameters' => ['lang' => 'lt']]);

        //$langMenu->addChild('Visit public', ['icon' => 'home', 'route' => 'homepage'])
        //    ->setLinkAttributes(['target' => '_blank']);

        return $langMenu;
    }

    public function setSecurityContext(SecurityContextInterface $sc = null)
    {
        $this->securityContext = $sc;
    }

    public function setManager(EntityManagerInterface $manager = null)
    {
        $this->manager = $manager;
    }

    public function addUserMenu($menu)
    {
        /* if ($this->securityContext->isGranted('ROLE_ADMIN') ||
          $this->securityContext->isGranted('ROLE_SUPER_ADMIN') ||
          $this->securityContext->isGranted('ROLE_MODERATOR')) {
          $menu->addChild('Admin panel', ['route' => 'admin', 'icon' => 'registration-mark']);
          } */

        if (!$this->securityContext->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            $menu->addChild('Login', ['route' => '_user_login_page', 'icon' => 'log-in']);
            $menu->addChild('SignUp', ['route' => '_user_signup', 'icon' => 'registration-mark']);
        }

        if ($this->securityContext->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            $menu->addChild('Logout', ['route' => '_user_logout', 'icon' => 'log-out']);
        }
    }

}
