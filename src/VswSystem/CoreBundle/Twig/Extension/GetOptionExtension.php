<?php

namespace VswSystem\CoreBundle\Twig\Extension;


use VswSystem\CoreBundle\Manager\AbstractManager;

class GetOptionExtension extends \Twig_Extension
{

    protected $systemManager;

    /**
     * Constructor method
     *
     * @param AbstractManager $manager
     */
    public function __construct(AbstractManager $manager)
    {
        $this->systemManager = $manager;
    }

    /**
     * @return \VswSystem\CmsBundle\Manager\AbstractManager
     */
    public function getSystemManager()
    {
        return $this->systemManager;
    }



    /**
     * Returns the name of the extension.
     *
     * @return string The extension name
     */
    public function getName()
    {
        return 'get_option_extension';
    }

    /**
     * Declare functions
     * @return array
     */
    public function getFunctions()
    {
        return array(
            'getOption' => new \Twig_Function_Method($this, 'getOption'),
        );
    }

    public function getOption($name)
    {
        $setting = $this->getSystemManager()->getSettingValue($name);
        if(is_array($setting)){
            return $setting['settingValue'];
        }
    }
}