<?php

namespace VswSystem\CoreBundle\Manager;

use VswSystem\CmsBundle\Entity\Page;

/**
 * Description of ContentManager
 *
 * @author cawa
 */
class ContentManager extends AbstractManager
{

    /**
     * @var block entity
     */
    protected $blockEntity;
    /**
     * @var content block entity
     */
    protected $contentBlockEntity;


    protected $eventBlockEntity;

    protected $questionEntity;

    /**
     * @param mixed $blockEntity
     */
    public function setBlockEntity($blockEntity)
    {
        $this->blockEntity = $blockEntity;
    }

    /**
     * @return string block entity
     */
    public function getBlockEntity()
    {
        return $this->blockEntity;
    }

    /**
     * @param mixed $contentBlockEntity
     */
    public function setContentBlockEntity($contentBlockEntity)
    {
        $this->contentBlockEntity = $contentBlockEntity;
    }

    /**
     * @return mixed
     */
    public function getContentBlockEntity()
    {
        return $this->contentBlockEntity;
    }

    /**
     * @param mixed $eventBlockEntity
     */
    public function setEventBlockEntity($eventBlockEntity)
    {
        $this->eventBlockEntity = $eventBlockEntity;
    }

    /**
     * @param mixed $questionEntity
     */
    public function setQuestionEntity($questionEntity)
    {
        $this->questionEntity = $questionEntity;
    }

    /**
     * @return mixed
     */
    public function getQuestionEntity()
    {
        return $this->questionEntity;
    }


    /**
     * @return mixed
     */
    public function getEventBlockEntity()
    {
        return $this->eventBlockEntity;
    }


    /**
     * @return \Doctrine\Common\Persistence\ObjectRepository
     */
    public function getEventBlockRepository()
    {
        return $this->getRepository($this->getEventBlockEntity());
    }


    /**
     * @return \Doctrine\Common\Persistence\ObjectRepository
     */
    public function getBlockRepository()
    {
        return $this->getRepository($this->getBlockEntity());
    }

    /**
     * @return \Doctrine\Common\Persistence\ObjectRepository
     */
    public function getContentBlockRepository()
    {
        return $this->getRepository($this->getContentBlockEntity());
    }


    /**
     * @return array
     */
    public function getContentBlocks()
    {
        return $this->getContentBlockRepository()->findAll();
    }


    /**
     * @param $settings
     * @return array
     */
    public function getContentBlocksSorted($settings)
    {
        if (!$settings['field'] || !$settings['direction']) {
            return $this->getContentBlocks();
        }
        return $this->getContentBlockRepository()->findBy([], [$settings['field'] => $settings['direction']]);
    }

    /**
     * @param $settings
     * @return array
     */
    public function geMainBlocks($count)
    {
        $blocks = $this->getContentBlockRepository()->findBy(['isMainPage' => 1], ['createdAt' => 'DESC'], $count);
        return $blocks;
    }

    /**
     * @return ContentBlock
     */
    public function getContentBlock($id)
    {
        return $this->getContentBlockRepository()->find($id);
    }


    /**
     * @return array of blocks
     */
    public function getBlocks()
    {
        return $this->getBlockRepository()->findAll();
    }


    public function getEventBlocks()
    {
        return $this->getEventBlockRepository()->findAll();
    }

    public function getNotPostedEvents()
    {
        return $this->getEventBlockRepository()->getNotPostedEvents();
    }

    /**
     * @param $settings
     * @return array
     */
    public function getEventBlocksSorted($settings)
    {
        if (!$settings['field'] || !$settings['direction']) {
            return $this->getEventBlocks();
        }
        return $this->getEventBlockRepository()->findBy([], [$settings['field'] => $settings['direction']]);
    }


    public function getLastEvents($count)
    {
        return $this->getEventBlockRepository()->findBy([], ['id' => 'ASC'], $count);
    }

    public function getMainEvents($count)
    {
        return $this->getEventBlockRepository()->findBy(['isMainPage' => 1], ['eventTime' => 'DESC'], $count);
    }

    public function getWideBlocks($count, $type)
    {
        switch ($type) {
            case 'content':
                $repo = $this->getContentBlockRepository();
                break;
            case 'events':
                $repo = $this->getEventBlockRepository();
                break;
        }
        return $repo->findBy(['blockSize' => 2], ['createdAt' => 'DESC'], $count);
    }

    public function getQuestionRepository()
    {
        return $this->getRepository($this->getQuestionEntity());
    }

    public function getQuestions()
    {
        return $this->getQuestionRepository()->findAll();
    }

    public function getLastQuestions($count)
    {
        return $this->getQuestionRepository()->findBy([], ['createdAt' => 'DESC'], $count);
    }
}
