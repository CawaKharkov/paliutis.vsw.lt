<?php

namespace VswSystem\CoreBundle\Manager;

use VswSystem\CoreBundle\Entity\Page;

/**
 * Description of PagesManager
 *
 * @author cawa
 */
class PagesManager extends AbstractManager
{

    protected $textPageEntity;
    protected $faqPageEntity;
    protected $galleryPageEntity;
    protected $newsPageEntity;
    protected $aliasEntity;
    protected $types = [];

    public function getTextPageRepository()
    {
        return $this->getRepository($this->textPageEntity);
    }

    public function setTextPageEntity($entity)
    {
        $this->textPageEntity = $entity;
    }

    public function getTextPages()
    {
        return $this->getTextPageRepository()->findAll();
    }

    public function getTextPagesSorted($settings = null)
    {
        if (!$settings['field'] || !$settings['direction']) {
            return $this->getTextPages();
        }
        return $this->getTextPageRepository()->findBy([], [$settings['field'] => $settings['direction']]);
    }

    /**
     * @return mixed
     */
    public function getFaqPageEntity()
    {
        return $this->faqPageEntity;
    }

    /**
     * @param mixed $faqPageEntity
     */
    public function setFaqPageEntity($faqPageEntity)
    {
        $this->faqPageEntity = $faqPageEntity;
    }

    public function getAliasRepsitory()
    {
        return $this->getRepository($this->aliasEntity);
    }

    public function setAliasEntity($entity)
    {
        $this->aliasEntity = $entity;
    }

    public function getFaqPageRepository()
    {
        return $this->getRepository($this->getFaqPageEntity());
    }

    public function getFaqPages()
    {
        return $this->getFaqPageRepository()->findAll();
    }

    /**
     * @param mixed $newsPageEntity
     */
    public function setNewsPageEntity($newsPageEntity)
    {
        $this->newsPageEntity = $newsPageEntity;
    }

    /**
     * @return mixed
     */
    public function getNewsPageEntity()
    {
        return $this->newsPageEntity;
    }

    public function getNewsPagerepository()
    {
        return $this->getRepository($this->getNewsPageEntity());
    }

    public function getNewsPages()
    {
        return $this->getNewsPagerepository()->findAll();
    }

    public function getPageByAlias($alias)
    {

        $alisEntity = $this->getAliasRepsitory()->findOneBy(['alias' => $alias]);
        if (!$alisEntity) {
            return false;
        }
   //     if ($page = $this->getCacheObject()->fetch('page_' . $alisEntity->getAlias())) {
            //return unserialize($page);
   //     }

        switch ($alisEntity->getType()) {
            case'TextContentPage':
                $page = $this->getTextPageRepository()->findOneBy(['alias' => $alisEntity]);
                break;

            case'GalleryContentPage':
                $page = $this->getGalleryPageRepository()->findOneBy(['alias' => $alisEntity]);
                break;

            case'FaqContentPage':
                $page = $this->getFaqPageRepository()->findOneBy(['alias' => $alisEntity]);
                break;

            case'NewsContentPage':
                $page = $this->getNewsPagerepository()->findOneBy(['alias' => $alisEntity]);
                break;
        }
//        $this->getCacheObject()->save('page_' . $alisEntity->getAlias(), serialize($page));
        return $page;
    }

    public function setGalleryPageEntity($entity)
    {
        $this->galleryPageEntity = $entity;
    }

    public function getGalleryPageRepository()
    {
        return $this->getRepository($this->galleryPageEntity);
    }

    /**
     * @return \VswSystem\CmsBundle\Entity\GalleryContentPage[]
     */
    public function getGalleryPages()
    {
        return $this->getGalleryPageRepository()->findAll();
    }

    public function getTypeByAlias($alias)
    {
        $alias = $this->getAliasRepsitory()->findOneBy(['alias' => $alias]);
        if (!$alias) {
            $type = false;
        } else {
            $type = $alias->getType();
        }
        return $type;
    }
}
