<?php

namespace VswSystem\CoreBundle\Manager;

/**
 * Description of MediaManager
 *
 * @author cawa
 */
class MediaManager extends AbstractManager
{

    protected $sliderEntity;
    protected $galleryGroupEntity;

    public function getSliders()
    {
        return $this->getSliderRepository()->findAll();
    }

    public function getSliderRepository()
    {
        return $this->getRepository($this->sliderEntity);
    }

    public function setSliderEntity($sliderEntity)
    {
        $this->sliderEntity = $sliderEntity;
    }

    /**
     * @param mixed $galleryGroupEntity
     */
    public function setGalleryGroupEntity($galleryGroupEntity)
    {
        $this->galleryGroupEntity = $galleryGroupEntity;
    }

    /**
     * @return mixed
     */
    public function getGalleryGroupEntity()
    {
        return $this->galleryGroupEntity;
    }

    public function getGalleryGroupReoisitory()
    {
        return $this->getRepository($this->getGalleryGroupEntity());
    }

    public function getGalleriesGroups()
    {
        return $this->getGalleryGroupReoisitory()->findAll();
    }

    public function getGalleriesGroupsSorted($settings = null)
    {
        if (!$settings['field'] || !$settings['direction']) {
            return $this->getGalleriesGroups();
        }
        return $this->getGalleryGroupReoisitory()->findBy([], [$settings['field'] => $settings['direction']]);
    }

    public function getGalleriesGroupByAlias($alias)
    {
        if(!$alias){
            return;
        }
        return $this->getGalleryGroupReoisitory()->findOneBy(['alias'=> $alias]);
    }

    public function getGalleriesGroupById($id)
    {
        if(!$id)
        {
            return null;
        }
        return $this->getGalleryGroupReoisitory()->find($id);
    }

    public function getMainSlider()
    {
        return $this->getSliderRepository()->findOneBy(['name' => 'mainSlider']);
    }

}
