<?php

namespace VswSystem\CoreBundle\Manager;

/**
 * Description of UserManager
 *
 * @author cawa
 */
class NavigationManager extends AbstractManager
{

    protected $navigationEntity;
    protected $menuEntity;

    public function getNavigationRepository()
    {
        return $this->getRepository($this->navigationEntity);
    }

    public function setNavigationEntity($entity)
    {
        $this->navigationEntity = $entity;
    }

    public function setMenuEntity($entity)
    {
        $this->menuEntity = $entity;
    }

    public function getMainMenu()
    {
        return $this->getRepository($this->menuEntity)->findBy(['name' => 'mainMenu']);
    }

    public function getMenuRepository()
    {
        return $this->getRepository($this->menuEntity);
    }

    public function getMenuList()
    {

        return $this->getRepository($this->menuEntity)->findAll();
    }

    public function getNavigationList()
    {
        return $this->getRepository($this->navigationEntity)->findBy([], ['position' => 'ASC']);
    }

    public function getNavigationListSorted($settings = null)
    {

        if (!$settings['field'] || !$settings['direction']) {
            return $this->getNavigationList();
        }
        return $this->getNavigationRepository()->findBy([], [$settings['field'] => $settings['direction']]);
    }


    public function getMenuByName($name)
    {
        $query = $this->createQuery('SELECT menu FROM VswSystemCmsBundle:Menu menu WHERE menu.name = :name');
        $query->setParameters(['name' => $name]);
        $query->useResultCache(true, 3600, 'getMenuByName');
        $reslut = $query->getSingleResult();
        return  $reslut;
    }

}
