<?php

namespace VswSystem\CoreBundle\Manager;

/**
 * Description of SystemManager
 *
 * @author cawa
 */
class SystemManager extends AbstractManager
{

    protected $settingsEntity;
    protected $settingsGroupEntity;

    /**
     * @param mixed $settingsEntity
     */
    public function setSettingsEntity($settingsEntity)
    {
        $this->settingsEntity = $settingsEntity;
    }

    /**
     * @return mixed
     */
    public function getSettingsEntity()
    {
        return $this->settingsEntity;
    }

    public function getSettingsRepository()
    {
        return $this->getRepository($this->getSettingsEntity());
    }

    public function getSettings()
    {
        return $this->getSettingsRepository()->findAll(); //findSettingsWithoutGroups();
    }

    public function getSettingValue($name)
    {
        $query = $this->createQuery('SELECT setting.settingValue FROM
        VswSystemAdminBundle:SettingsValue setting WHERE setting.settingName = :name');

        $query->setParameters(['name' => $name]);
      //  $query->useResultCache(true, 3600, 'getSetting' . $name);

        return $query->getOneOrNullResult();
        //return $this->getSettingsRepository()->findOneBy(['settingName' => $name])->getSettingValue();
    }

    /**
     * @return mixed
     */
    public function getSettingsGroupEntity()
    {
        return $this->settingsGroupEntity;
    }

    /**
     * @param mixed $settingsGroupEntity
     */
    public function setSettingsGroupEntity($settingsGroupEntity)
    {
        $this->settingsGroupEntity = $settingsGroupEntity;
    }

    public function getSettingsGroupRepository()
    {
        return $this->getRepository($this->getSettingsGroupEntity());
    }

    public function getSettingsGroups()
    {
        return $this->getSettingsGroupRepository()->findAll();
    }

    public function getSettingsGroup($name)
    {
        return $this->getSettingsGroupRepository()->findOneBy(['name' => $name]);
    }
}