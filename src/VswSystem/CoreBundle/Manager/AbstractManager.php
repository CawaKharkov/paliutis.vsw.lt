<?php

namespace VswSystem\CoreBundle\Manager;

use Doctrine\ORM\Decorator\EntityManagerDecorator;
use Psr\Log\LoggerInterface;
use VswSystem\CmsBundle\Entity\AbstractEntity\AbstractContentPage;

/**
 * Description of AbstractManager
 *
 * @author cawa
 */
abstract class AbstractManager extends EntityManagerDecorator
{

    protected $cacheObject;
    protected $serializer;
    protected $logObject;

    /**
     * @param mixed $cacheObject
     */
    public function setCacheObject($cacheObject)
    {
        $this->cacheObject = $cacheObject;
    }

    /**
     * @return \Doctrine\Common\Cache\PhpFileCache
     */
    public function getCacheObject()
    {
        return $this->cacheObject;
    }

    /**
     * @param mixed $serializer
     */
    public function setSerializer($serializer)
    {
        $this->serializer = $serializer;
    }

    /**
     * @return \Symfony\Component\Serializer\Serializer
     */
    public function getSerializer()
    {
        return $this->serializer;
    }

    /**
     * @return mixed
     */
    public function getLogObject()
    {
        return $this->logObject;
    }


    /**
     * @param LoggerInterface $logObject
     */
    public function setLogObject(LoggerInterface $logObject)
    {
        $this->logObject = $logObject;
    }




    public function save($entity, $flush = false)
    {

        $this->persist($entity);
        if ($flush) {
            $this->flush();
        }
        if($entity instanceof AbstractContentPage){
            $this->getCacheObject()->save('page_' . $entity->getAlias()->getAlias(), serialize($entity));
        }

        //$this->logObject->info('I just got the logger');
        //$this->logObject->error('An error occurred');

        return $entity;
    }

    public function remove($entity)
    {

        parent::remove($entity);
        $this->flush();
    }


    public function mergeSave($entity, $flush = false)
    {
        $this->merge($entity);
        if ($flush) {
            $this->flush();
        }
        return $entity;
    }
}
