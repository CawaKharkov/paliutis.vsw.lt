<?php

namespace VswSystem\CoreBundle\Manager;

/**
 * Description of UserManager
 *
 * @author cawa
 */
class UserManager extends AbstractManager
{

    protected $userEntity;
    protected $donorEntity;

    /**
     * @param mixed $userEntity
     */
    public function setUserEntity($userEntity)
    {
        $this->userEntity = $userEntity;
    }

    /**
     * @return mixed
     */
    public function getUserEntity()
    {
        return $this->userEntity;
    }

    public function getUserRepository()
    {
        return $this->getRepository($this->getUserEntity());
    }


    public function getUserList()
    {
        return $this->getUserRepository()->findAll();
    }

    /**
     * @param mixed $donorEntity
     */
    public function setDonorEntity($donorEntity)
    {
        $this->donorEntity = $donorEntity;
    }

    /**
     * @return mixed
     */
    public function getDonorEntity()
    {
        return $this->donorEntity;
    }

    public function getDonorRepository()
    {
        return $this->getRepository($this->getDonorEntity());
    }

    public function getDonors()
    {
        return $this->getDonorRepository()->findAll();
    }

    public function getDonorsSorted($settings = null)
    {

        if ((!$settings['field'] || !$settings['direction']) &&
            (!$settings['filterField'] || !$settings['filterValue'])
        ) {
            return $this->getDonors();
        }

        if ($settings['filterField'] && $settings['filterValue']) {
            return $this->getDonorRepository()->getDonorsNameLike($settings['filterValue']);
        }

        return $this->getDonorRepository()->findBy([],
            [$settings['field'] => $settings['direction']]);
    }
}