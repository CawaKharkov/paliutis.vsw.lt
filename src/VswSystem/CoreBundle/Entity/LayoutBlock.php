<?php

namespace VswSystem\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use VswSystem\CmsBundle\Entity\Traits\GetShortText;
use VswSystem\CmsBundle\Entity\Traits\IdentificationalEntity;
use VswSystem\CmsBundle\Entity\Traits\NamedEntity;
use VswSystem\CmsBundle\Entity\Traits\TitledEntity;
use VswSystem\CoreBundle\Interfaces\ContentEntityInterface;

/**
 * LayoutBlock
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="VswSystem\CoreBundle\Entity\Repository\LayoutBlockRepository")
 */
class LayoutBlock implements ContentEntityInterface
{
    use IdentificationalEntity;
    use NamedEntity;
    use TitledEntity;
    use TimestampableEntity;
    use GetShortText;


    /**
     * @var integer
     *
     * @ORM\Column(name="contentType", type="smallint")
     */
    protected $contentType;
    protected static  $contentTypes = [0=> 'CONTENT_BLOCK',1=> 'EVENT_BLOCK',2 => 'TEXT',3 => 'IMAGE', '999' => 'FLASH'];


    /**
     * @var \VswSystem\CmsBundle\Entity\ContentBlock
     * @ORM\OneToOne(targetEntity="VswSystem\CmsBundle\Entity\ContentBlock")
     */
    protected $contentBlock;


    /**
     * @var \VswSystem\CmsBundle\Entity\EventBlock
     * @ORM\OneToOne(targetEntity="VswSystem\CmsBundle\Entity\EventBlock")
     */
    protected $eventBlock;


    /**
     * @var string
     *
     * @ORM\Column(name="text", type="text", nullable=true)
     */
    protected $text;

    /**
     * @var string
     *
     * @ORM\Column(name="url", type="text", nullable=true)
     */
    protected $url;

    /**
     * @var ContentImage
     *
     * @ORM\OneToOne(targetEntity="\VswSystem\CmsBundle\Entity\ContentImage", cascade={"all"})
     */
    protected  $cover;

    /**
     * @var ContentImage
     *
     * @ORM\OneToOne(targetEntity="\VswSystem\CmsBundle\Entity\ContentImage", cascade={"all"})
     */
    protected  $swf;
    
     /**
     * @var integer
     *
     * @ORM\Column(name="showImages", type="boolean")
     */
    protected $showImages = 0;


    /**
     * @return int
     */
    public function getContentType()
    {
        return $this->contentType;
    }

    /**
     * @param int $contentType
     */
    public function setContentType($contentType)
    {
        $this->contentType = $contentType;
    }


    /**
     * Set text
     *
     * @param string $text
     * @return LayoutBlock
     */
    public function setText($text)
    {
        $this->text = $text;

        return $this;
    }

    /**
     * Get text
     *
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param string $url
     */
    public function setUrl($url)
    {
        $this->url = $url;
    }



    /**
     * @return array
     */
    public static function getContentTypes()
    {
        return self::$contentTypes;
    }

    /**
     * @return \VswSystem\CmsBundle\Entity\ContentBlock
     */
    public function getContentBlock()
    {
        return $this->contentBlock;
    }

    /**
     * @param \VswSystem\CmsBundle\Entity\ContentBlock $contentBlock
     */
    public function setContentBlock($contentBlock)
    {
        $this->contentBlock = $contentBlock;
    }

    /**
     * @return \VswSystem\CmsBundle\Entity\EventBlock
     */
    public function getEventBlock()
    {
        return $this->eventBlock;
    }

    /**
     * @param \VswSystem\CmsBundle\Entity\EventBlock $eventBlock
     */
    public function setEventBlock($eventBlock)
    {
        $this->eventBlock = $eventBlock;
    }

    /**
     * @return ContentImage
     */
    public function getCover()
    {
        return $this->cover;
    }

    /**
     * @param ContentImage $cover
     */
    public function setCover($cover)
    {
        $this->cover = $cover;
    }

    /**
     * @return ContentImage
     */
    public function getSwf()
    {
        return $this->swf;
    }

    /**
     * @param ContentImage $swf
     */
    public function setSwf($swf)
    {
        $this->swf = $swf;
    }

    /**
     * @param int $showImages
     */
    public function setVisible($showImages)
    {
        $this->showImages = $showImages;
    }

    /**
     * @return int
     */
    public function getVisible()
    {
        return (bool)$this->showImages;
    }

}
