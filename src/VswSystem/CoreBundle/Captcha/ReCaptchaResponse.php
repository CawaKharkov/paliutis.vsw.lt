<?php

namespace VswSystem\CoreBundle\Captcha;


class ReCaptchaResponse
{
    public $success;
    public $errorCodes;
}