<?php

namespace VswSystem\CoreBundle\Page\Service;

use VswSystem\CoreBundle\VswService\AbstractEntityService;

/**
 * Class AliasService
 * @package VswSystem\CoreBundle\Page\Service
 */
class AliasService extends AbstractEntityService
{

    /**
     * Get page type by given alias
     * @param $alias
     * @return string
     */
    public function getType($alias)
    {
        $alias = $this->getRepository()->findOneBy(['alias' => $alias]);
        if(!$alias){
            return;
        }
        return $alias->getType();
    }

} 