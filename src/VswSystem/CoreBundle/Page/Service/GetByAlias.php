<?php
/**
 * Created by PhpStorm.
 * User: cawa
 * Date: 18.09.14
 * Time: 23:51
 */

namespace VswSystem\CoreBundle\Page\Service;

/**
 * Class GetByAlias
 * Find page by alias
 * @package VswSystem\CoreBundle\Page\Service
 *
 */
trait GetByAlias
{
    public function getPageByAlias($alias)
    {
        return $this->getRepository()->findByAlias($alias);
    }
} 