<?php

namespace VswSystem\CoreBundle\Page\Service;

use VswSystem\CoreBundle\VswService\AbstractEntityService;

/**
 * Class FaqPageService
 * @package VswSystem\CoreBundle\Page\Service
 */
class FaqPageService extends AbstractEntityService
{
   use GetByAlias;

} 