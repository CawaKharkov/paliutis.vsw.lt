<?php

namespace VswSystem\CoreBundle\Page\Service;

use VswSystem\CoreBundle\VswService\AbstractEntityService;

/**
 * Class GalleryPageService
 * @package VswSystem\CoreBundle\Page\Service
 */
class GalleryPageService extends AbstractEntityService
{
   use GetByAlias;

} 