<?php

namespace VswSystem\CoreBundle\Page\Service;

use VswSystem\CoreBundle\VswService\AbstractEntityService;

/**
 * Class NewsPageService
 * @package VswSystem\CoreBundle\Page\Service
 */
class NewsPageService extends AbstractEntityService
{
   use GetByAlias;

} 