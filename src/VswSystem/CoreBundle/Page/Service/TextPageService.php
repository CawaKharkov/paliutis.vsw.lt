<?php

namespace VswSystem\CoreBundle\Page\Service;

use VswSystem\CoreBundle\VswService\AbstractEntityService;

/**
 * Class PageService
 * @package VswSystem\CoreBundle\Page\Service
 */
class TextPageService extends AbstractEntityService
{
   use GetByAlias;

} 