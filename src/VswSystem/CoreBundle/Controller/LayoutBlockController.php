<?php

namespace VswSystem\CoreBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use VswSystem\AdminBundle\Controller\AbstractController;
use VswSystem\SecurityBundle\Entity\User;

/**
 * @Route("/layout")
 */
class LayoutBlockController extends AbstractController
{
    /**
     * @Route("/",name="aside_block")
     * @Template()
     */
    public function asideAction()
    {
        $widget = $this->getWidgetManager()->getHallWidget(null,'mainHall');
        $asideImage = $this->get('vswsystem.orm.service.layout_block')->find('name','aside_image1');
        $asideImage2 = $this->get('vswsystem.orm.service.layout_block')->find('name','aside_image2');


        return ['widget' => $widget,'asideImage' => $asideImage, 'asideImage2' => $asideImage2];
    }


    /**
     * @Route("/{name}",name="topSlide_block")
     * @Template()
     */
    public function topSlideAction($name = null)
    {
        $categories = $this->getCategoryService()->findAll();
        $slideImages = $this->getMediaManager()->getGalleriesGroupById(2);
        return [
            'name' => $name,
            'categories' => $categories,
            'slider' => $slideImages

        ];
    }

    /**
     * @Route("/audit",name="audit_block")
     * @Template()
     */
    public function auditAction()
    {
        $auditImages = $this->getMediaManager()->getGalleriesGroupById(3);
        return [
            'auditImages' => $auditImages
        ];
    }

    /**
     * @Route("/widget",name="widget_block")
     * @Template()
     */
    public function widgetAction()
    {
        $widget = $this->getWidgetManager()->getHallWidget(null,'main');
        return [
            'widget' => $widget
        ];
    }


    /**
     * @Route("/{wide}",name="aside_sertificate_block")
     * @Template()
     */
    public function asideSertificateAction($wide = 4, Request $request)
    {
        $month = new \DateTime('1 month');
        $registred = false;
        if(new \DateTime($request->cookies->get('asideregistration')) <= $month and
        $request->cookies->get('asideregistration')!==null){
            $registred = true;
        }
        $signupForm = $this->createForm('signup', new User(),['show_legend'=>false]);
        return ['signupForm'=>$signupForm->createView(),'wide'=>$wide, 'registred' => $registred];
    }

}
