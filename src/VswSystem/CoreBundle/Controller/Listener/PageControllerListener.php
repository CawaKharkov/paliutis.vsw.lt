<?php

namespace VswSystem\CoreBundle\Controller\Listener;


use Symfony\Bundle\FrameworkBundle\Controller\ControllerResolver;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use VswSystem\CmsBundle\Controller\PageController;

/**
 * Listen to /page/alias and redirect to controller action
 * Class PageControllerListener
 * @package VswSystem\CoreBundle\Controller
 */
class PageControllerListener
{

    protected $resolver;
    protected $aliasRepo;

    function __construct(ControllerResolver $resolver, $aliasRepo)
    {
        $this->resolver = $resolver;
        $this->aliasRepo = $aliasRepo;
    }


    public function onKernelController(FilterControllerEvent $event)
    {
        $controller = $event->getController();


        /*
         * $controller passed can be either a class or a Closure. This is not usual in Symfony but it may happen.
         * If it is a class, it comes in array format
         */
        if (!is_array($controller)) {
            return;
        }


        if ($controller[0] instanceof PageController && $controller[1] == 'viewAction') {

            $request = new Request();
            $alias = $this->aliasRepo->findOneBy(['alias' => $event->getRequest()->attributes->get('alias')]);

            $action = '';

            if(!$alias || !$alias->getType()){
                throw new NotFoundHttpException('No such page!');
            }

            switch ($alias->getType()) {
                case 'TextContentPage':
                    $action = 'textPage';
                    break;

                case 'GalleryContentPage':
                    $action = 'galleryPage';
                    break;

                case 'FaqContentPage':
                    $action = 'faqPage';
                    break;

                case 'NewsContentPage':
                    $action = 'newsPage';
                    break;


            }

            $request->attributes->set('_controller', 'VswSystemCmsBundle:Page:' . $action);
            $event->setController($this->resolver->getController($request));
            //  $event->setController('VswSystem\CmsBundle\Controller\GalleryController');
        }
    }
}



