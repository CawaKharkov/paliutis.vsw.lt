$(document).ready(function () {

    $(".fancybox").fancybox({});

    var isMobile = {
        Android: function () {
            return navigator.userAgent.match(/Android/i);
        },
        BlackBerry: function () {
            return navigator.userAgent.match(/BlackBerry/i);
        },
        iOS: function () {
            return navigator.userAgent.match(/iPhone|iPad|iPod/i);
        },
        Opera: function () {
            return navigator.userAgent.match(/Opera Mini/i);
        },
        Windows: function () {
            return navigator.userAgent.match(/IEMobile/i);
        },
        any: function () {
            return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
        }
    };

    if (location.pathname != '/' && !isMobile.any()) {
        window.scrollTo(0, 580);
    }


    $('#subscribe').on('submit', function () {
        $.ajax({
            type: "POST",
            url: $(this).attr('action'),
            data: $(this).serialize()

        }).done(function () {
            $('#subscribe').hide('fast');
            $('#subscribeText').show('fast');

        }).error(function (data,error) {
         //  console.log(data);
           console.log(error);
            location.reload();
        });
        return false;
    });


});
