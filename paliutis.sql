-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Mar 03, 2015 at 08:34 PM
-- Server version: 5.5.41-0ubuntu0.14.10.1
-- PHP Version: 5.6.6-1+deb.sury.org~utopic+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `paliutis`
--

-- --------------------------------------------------------

--
-- Table structure for table `AccessToken`
--

CREATE TABLE IF NOT EXISTS `AccessToken` (
`id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `expires_at` int(11) DEFAULT NULL,
  `scope` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `acl_classes`
--

CREATE TABLE IF NOT EXISTS `acl_classes` (
`id` int(10) unsigned NOT NULL,
  `class_type` varchar(200) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `acl_entries`
--

CREATE TABLE IF NOT EXISTS `acl_entries` (
`id` int(10) unsigned NOT NULL,
  `class_id` int(10) unsigned NOT NULL,
  `object_identity_id` int(10) unsigned DEFAULT NULL,
  `security_identity_id` int(10) unsigned NOT NULL,
  `field_name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ace_order` smallint(5) unsigned NOT NULL,
  `mask` int(11) NOT NULL,
  `granting` tinyint(1) NOT NULL,
  `granting_strategy` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `audit_success` tinyint(1) NOT NULL,
  `audit_failure` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `acl_object_identities`
--

CREATE TABLE IF NOT EXISTS `acl_object_identities` (
`id` int(10) unsigned NOT NULL,
  `parent_object_identity_id` int(10) unsigned DEFAULT NULL,
  `class_id` int(10) unsigned NOT NULL,
  `object_identifier` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `entries_inheriting` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `acl_object_identity_ancestors`
--

CREATE TABLE IF NOT EXISTS `acl_object_identity_ancestors` (
  `object_identity_id` int(10) unsigned NOT NULL,
  `ancestor_id` int(10) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `acl_security_identities`
--

CREATE TABLE IF NOT EXISTS `acl_security_identities` (
`id` int(10) unsigned NOT NULL,
  `identifier` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `username` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `Alias`
--

CREATE TABLE IF NOT EXISTS `Alias` (
`id` int(11) NOT NULL,
  `alias` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `Alias`
--

INSERT INTO `Alias` (`id`, `alias`, `type`) VALUES
(3, 'make-up-page', 'GalleryContentPage'),
(4, 'make-up', 'GalleryImageGroup'),
(6, 'news', 'NewsContentPage'),
(7, '900P-vyriski-batai', 'CatalogItem'),
(8, '900PS-vyriski-spalvoti-batai', 'CatalogItem'),
(15, 'kontaktai', 'TextContentPage'),
(16, 'naujienos', 'NewsContentPage'),
(17, 'apie_mus', 'TextContentPage'),
(20, '400P_saugieji_batai', 'CatalogItem'),
(21, 'auditai', 'TextContentPage'),
(22, '401P-saugieji-vyriski-batai', 'CatalogItem'),
(23, '902P-vaikiski-batai', 'CatalogItem'),
(24, 'zveju-batai', 'CatalogItem'),
(25, 'akcija', 'TextContentPage'),
(26, '701P-moteriški-vyriški-batai(kaliosai)', 'CatalogItem'),
(27, '902PS-vaikiški-spalvoti-batai', 'CatalogItem'),
(28, '100P-moteriski-batai', 'CatalogItem'),
(29, '200P-moteriški-trumpaauliai-batai', 'CatalogItem'),
(30, '200PS-moteriški-spalvoti-trumpaauliai-batai', 'CatalogItem'),
(31, '300P-vyriški-batai', 'CatalogItem'),
(32, '300PS-vyriški-batai', 'CatalogItem'),
(33, '101P-moteriški-batai', 'CatalogItem'),
(34, 'AMBER-vaikiški-batai', 'CatalogItem'),
(35, '905P', 'CatalogItem'),
(36, 'Žvejų-kombinezonai', 'CatalogItem'),
(37, '901P-moteriški-batai', 'CatalogItem'),
(38, '901PS-moteriški-spalvoti-batai', 'CatalogItem'),
(39, '100PS-moteriški-spalvoti-ilgaauliai-batai', 'CatalogItem'),
(40, '200PM-moteriški-trumpaauliai-batai', 'CatalogItem');

-- --------------------------------------------------------

--
-- Table structure for table `AuthCode`
--

CREATE TABLE IF NOT EXISTS `AuthCode` (
`id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `redirect_uri` longtext COLLATE utf8_unicode_ci NOT NULL,
  `expires_at` int(11) DEFAULT NULL,
  `scope` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `Block`
--

CREATE TABLE IF NOT EXISTS `Block` (
`id` int(11) NOT NULL,
  `comment` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type` smallint(6) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `Campaign`
--

CREATE TABLE IF NOT EXISTS `Campaign` (
`id` int(11) NOT NULL,
  `organizator_id` int(11) DEFAULT NULL,
  `driver_id` int(11) DEFAULT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `departureTime` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `starts` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `finish` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `returnTime` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `campaignTime` smallint(6) NOT NULL,
  `donorsAmount` smallint(6) NOT NULL,
  `fakt` int(11) NOT NULL,
  `m` smallint(6) NOT NULL,
  `n` smallint(6) NOT NULL,
  `notes` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `start` tinyint(1) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  `contactPerson_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `campaign_personnel`
--

CREATE TABLE IF NOT EXISTS `campaign_personnel` (
  `campaign_id` int(11) NOT NULL,
  `personnel_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `CatalogItem`
--

CREATE TABLE IF NOT EXISTS `CatalogItem` (
`id` int(11) NOT NULL,
  `showImages` tinyint(1) NOT NULL,
  `position` smallint(6) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `alias_id` int(11) DEFAULT NULL,
  `tags` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  `description` tinytext COLLATE utf8_unicode_ci,
  `size` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `attributes` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:simple_array)',
  `content` tinytext COLLATE utf8_unicode_ci,
  `sizeGb` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `attributesInner` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:simple_array)'
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `CatalogItem`
--

INSERT INTO `CatalogItem` (`id`, `showImages`, `position`, `name`, `title`, `alias_id`, `tags`, `createdAt`, `updatedAt`, `description`, `size`, `attributes`, `content`, `sizeGb`, `attributesInner`) VALUES
(1, 1, 0, '900P', '900P', 7, 'vyriški guminiai batai, vyriški botai, botai,batai', '0000-00-00 00:00:00', '2015-02-06 11:22:53', '<p>&nbsp;Vyri&scaron;ki ilgaauliai batai</p>', '39 - 48', '1', '<p>Vyri&scaron;ki ilgaauliai batai</p>', '5 - 14', '1,5,6'),
(2, 1, 0, '900PS', '900PS', 8, NULL, '2014-11-04 12:54:49', '2015-02-06 10:05:46', '<p>Vyri&scaron;ki spalvoti ilgaauliai batai</p>', '39 - 48', '1', '<p>Vyri&scaron;ki spalvoti ilgaauliai batai</p>', '5 - 14', '1,5,6'),
(5, 0, 0, '400P', '400P', 20, NULL, '2014-11-19 10:36:26', '2015-02-06 10:16:55', '<p>Vyri&scaron;ki saugieji batai</p>', '39 - 47', '1,2,3,4,6,7', '<p>Vyri&scaron;ki saugieji batai</p>\r\n<p>EN ISO 20345:2011 - S5</p>', '5 - 13', '1,5,6'),
(6, 0, 0, '401P', '401P', 22, NULL, '2015-01-18 14:28:44', '2015-02-06 10:17:38', '<p>Vyri&scaron;ki pa&scaron;iltinti saugieji batai</p>', '39 - 47', '1,3,4,5,6,7', '<p>Vyri&scaron;ki pa&scaron;iltinti saugieji batai</p>\r\n<p>EN ISO 20345:2011 - SB - P</p>', '5 - 13', '1,5,6'),
(7, 0, 0, '902P', '902P', 23, NULL, '2015-01-18 14:42:20', '2015-02-06 10:16:19', '<p>Vaiki&scaron;ki batai</p>', '27 - 34', '1', '<p>Vaiki&scaron;ki batai</p>', '9 - 2', '1,3,6'),
(8, 0, 0, 'Žvejų batai', 'Žvejų batai', 24, NULL, '2015-01-18 14:47:53', '2015-02-06 11:03:52', '<p>Žvejų batai 900PZ; žvejų batai 400PZ PREMIUM</p>', '39 - 48', '1', '<p>Žvejų batai 900PZ; žvejų batai 400PZ PREMIUM</p>', '5 - 14', '1,3,6'),
(9, 0, 0, '701P', '701P', 26, NULL, '2015-01-20 15:24:28', '2015-02-06 10:35:48', '<p>Moteri&scaron;ki - vyri&scaron;ki batai (kalio&scaron;ai)</p>', '36 - 46', '1,5', '<p>Moteri&scaron;ki - vyri&scaron;ki batai (kalio&scaron;ai)</p>', '3 - 12', '2,4,6'),
(10, 0, 0, '902PS', '902PS', 27, NULL, '2015-01-23 15:03:12', '2015-02-01 19:57:06', '<p>Vaiki&scaron;ki spalvoti batai</p>', '27 - 34', '1', '<p>Vaiki&scaron;ki spalvoti batai</p>', '9 - 2', '1,3,6'),
(11, 0, 0, '100P', '100P', 28, NULL, '2015-01-23 15:32:15', '2015-01-23 15:32:15', '<p>Moteri&scaron;ki ilgaauliai batai</p>', '36 - 42', '1', '<p>Moteri&scaron;ki ilgaauliai batai</p>', '3 - 9', '1,3,6'),
(12, 0, 0, '200P', '200P', 29, NULL, '2015-01-23 15:40:41', '2015-02-06 11:54:09', '<p>Moteri&scaron;ki trumpaauliai batai</p>', '37 - 41', '1', '<p>Moteri&scaron;ki trumpaauliai batai</p>', '4 - 8', '1,3,6'),
(13, 0, 0, '200PS', '200PS', 30, NULL, '2015-01-23 15:43:41', '2015-02-06 11:49:25', '<p>Moteri&scaron;ki spalvoti trumpaauliai batai</p>', '37 - 41', '1', '<p>Moteri&scaron;ki spalvoti trumpaauliai batai</p>', '4 - 8', '1,3,6'),
(14, 0, 0, '300P', '300P', 31, NULL, '2015-01-23 15:46:05', '2015-02-01 22:02:21', '<p>Vyri&scaron;ki batai</p>', '42 - 46', '1', '<p>Vyri&scaron;ki batai</p>', '8 - 12', '1,3,6'),
(15, 0, 0, '300PS', '300PS', 32, NULL, '2015-01-23 16:06:42', '2015-02-01 22:07:37', '<p>Vyri&scaron;ki spalvoti batai</p>', '42 - 46', '1', '<p>Vyri&scaron;ki spalvoti batai</p>', '8 - 12', '1,3,6'),
(16, 0, 0, '101P', '101P', 33, NULL, '2015-01-23 16:08:32', '2015-01-23 16:08:32', '<p>Moteri&scaron;ki ilgaauliai batai</p>', '36 - 42', '1', '<p>Moteri&scaron;ki ilgaauliai batai</p>', '3 - 9', '1,3,6'),
(17, 0, 0, 'AMBER', 'AMBER', 34, NULL, '2015-01-23 16:21:37', '2015-01-23 16:21:37', '<p>Vaiki&scaron;ki batai</p>', '21 - 35', '1', '<p>Vaiki&scaron;ki batai</p>', '4 - 3', '1,3,6'),
(18, 0, 0, '905P', '905P', 35, NULL, '2015-01-23 16:28:23', '2015-01-26 22:41:58', '<p>Vyri&scaron;ki batai</p>', '40 - 44', '1', '<p>Vyri&scaron;ki batai</p>', '6 - 10', '1,3,6'),
(19, 0, 0, 'Žvejų kombinezonai', 'Žvejų kombinezonai', 36, NULL, '2015-02-06 10:58:57', '2015-02-06 11:02:26', '<p>Žvejų kombinezonai 900P-KOMB; žvejų kombinezonai 400P-KOMB PREMIUM</p>', '40 - 47', '1', '<p>Žvejų kombinezonai 900P-KOMB; žvejų kombinezonai 400P-KOMB PREMIUM</p>', '6-13', '1,3,6'),
(20, 0, 0, '901P', '901P', 37, NULL, '2015-02-06 11:26:21', '2015-02-06 11:27:13', '<p>Moteri&scaron;ki ilgaauliai batai</p>', '36-41', '1', '<p>Moteri&scaron;ki ilgaauliai batai</p>', '3-8', '1,3,6'),
(21, 0, 0, '901PS', '901PS', 38, NULL, '2015-02-06 11:39:36', '2015-02-06 11:39:36', '<p>Moteri&scaron;ki spalvoti ilgaauliai batai</p>', '36-41', '1', '<p>Moteri&scaron;ki spalvoti ilgaauliai batai</p>', '3-8', '1,3,6'),
(22, 0, 0, '100PS', '100PS', 39, NULL, '2015-02-06 11:47:22', '2015-02-06 11:47:22', '<p>Moteri&scaron;ki spalvoti ilgaauliai batai</p>', '36-42', '1', '<p>Moteri&scaron;ki spalvoti ilgaauliai batai</p>', '3-9', '1,3,6'),
(23, 0, 0, '200PM', '200PM', 40, NULL, '2015-02-06 11:56:18', '2015-02-06 11:56:18', '<p>Moteri&scaron;ki trumpaauliai batai su manžetu</p>', '37-41', '1', '<p>Moteri&scaron;ki trumpaauliai batai su manžetu</p>', '4-8', '1,3,6');

-- --------------------------------------------------------

--
-- Table structure for table `catalogitem_category`
--

CREATE TABLE IF NOT EXISTS `catalogitem_category` (
  `catalogitem_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `catalogitem_category`
--

INSERT INTO `catalogitem_category` (`catalogitem_id`, `category_id`) VALUES
(1, 1),
(2, 1),
(5, 1),
(6, 1),
(6, 5),
(7, 3),
(8, 6),
(9, 1),
(9, 2),
(10, 3),
(11, 2),
(12, 2),
(13, 2),
(14, 1),
(15, 1),
(16, 2),
(17, 3),
(18, 1),
(19, 6),
(20, 2),
(21, 2),
(22, 2),
(23, 2);

-- --------------------------------------------------------

--
-- Table structure for table `catalogitem_itemimage`
--

CREATE TABLE IF NOT EXISTS `catalogitem_itemimage` (
  `catalogitem_id` int(11) NOT NULL,
  `itemimage_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `catalogitem_itemimage`
--

INSERT INTO `catalogitem_itemimage` (`catalogitem_id`, `itemimage_id`) VALUES
(1, 1),
(1, 19),
(1, 20),
(2, 38),
(5, 26),
(5, 27),
(5, 28),
(6, 29),
(6, 30),
(6, 31),
(9, 24),
(9, 33),
(10, 34),
(10, 35),
(10, 36),
(10, 37),
(14, 39),
(15, 40),
(18, 32);

-- --------------------------------------------------------

--
-- Table structure for table `Category`
--

CREATE TABLE IF NOT EXISTS `Category` (
`id` int(11) NOT NULL,
  `cover_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `icon_id` int(11) DEFAULT NULL,
  `description` tinytext COLLATE utf8_unicode_ci
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `Category`
--

INSERT INTO `Category` (`id`, `cover_id`, `name`, `createdAt`, `updatedAt`, `title`, `icon_id`, `description`) VALUES
(1, 4, 'man', '2014-11-04 00:00:00', '2015-01-18 14:12:07', 'Vyriški', 12, '<p>Vyri&scaron;ki guminiai batai, skirti tiek darbui, tiek ir laisvalaikiui.</p>'),
(2, 9, 'woman', '2014-11-04 12:26:56', '2014-11-19 10:14:05', 'Moteriški', 14, '<p>"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dol'),
(3, 10, 'kids', '2014-11-04 12:42:13', '2014-11-19 10:14:16', 'Vaikiški', 15, '<p>"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dol'),
(5, 11, 'work', '2014-11-04 12:43:46', '2014-11-19 10:14:27', 'Darbiniai', 16, '<p>"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dol'),
(6, 8, 'other-boots', '2014-11-04 12:44:12', '2015-02-05 20:56:05', 'Kiti batai', 13, '<p>KTII&nbsp;GUMINIAI BATAI (ŽVEJAMS) IR EVA GUMINIAI BATAI - TAI LENKIJOS GAMINTOJŲ BATAI, KURIŲ I&Scaron;SKIRTINES ATSTOVAVIMO TEISES LIETUVOJE TURI MŪSŲ ĮMONĖ.</p>');

-- --------------------------------------------------------

--
-- Table structure for table `Client`
--

CREATE TABLE IF NOT EXISTS `Client` (
`id` int(11) NOT NULL,
  `random_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `redirect_uris` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:array)',
  `secret` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `allowed_grant_types` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:array)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `ContentBlock`
--

CREATE TABLE IF NOT EXISTS `ContentBlock` (
`id` int(11) NOT NULL,
  `cover_id` int(11) DEFAULT NULL,
  `shortText` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `blockSize` smallint(6) DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `content` longtext COLLATE utf8_unicode_ci NOT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  `tags` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `isPosted` tinyint(1) NOT NULL,
  `isMainPage` tinyint(1) NOT NULL,
  `mainPageBlock` smallint(6) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `ContentBlock`
--

INSERT INTO `ContentBlock` (`id`, `cover_id`, `shortText`, `blockSize`, `title`, `content`, `createdAt`, `updatedAt`, `tags`, `isPosted`, `isMainPage`, `mainPageBlock`) VALUES
(1, 2, 'asdsad', 0, 'sadasdasdasd', '<p>asdasdasdasd</p>', '2014-10-05 14:35:28', '2014-11-14 11:36:33', 'sdasd', 0, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `ContentImage`
--

CREATE TABLE IF NOT EXISTS `ContentImage` (
`id` int(11) NOT NULL,
  `path` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `ContentImage`
--

INSERT INTO `ContentImage` (`id`, `path`, `name`) VALUES
(2, 'image54312cd1c0b78_a2d17.png', 'image5465cd1b6a071'),
(3, 'image54574d7badfef_04ade.jpeg', 'image54574d7badfef'),
(4, 'vyr batai katalogui_e9e96.png', 'image54bba310c4b8e'),
(5, 'image546a1936ecfc6_8776c.jpeg', 'image546a1936ecfc6'),
(6, 'hoffis_logo_288c4.jpeg', 'image54ce7d8488b0b'),
(7, 'Regeneracija_logo_8331f.jpeg', 'image54ce7d84885ca'),
(8, 'image546a15d126f9c_5c360.jpeg', 'image54d3bcb49ff9b'),
(9, 'image546a149083e69_3f1bf.jpeg', 'image546c51462253d'),
(10, 'image546a14ad8debb_fc4e5.jpeg', 'image546c515181b94'),
(11, 'image546a151f38e2d_d2562.jpeg', 'image546c515c39615'),
(12, 'image5465fc3518e3a_98b26.png', 'image54bba310c4d67'),
(13, 'image5465fc7446af7_987bb.png', 'image54d3bcb4a018e'),
(14, 'image5465fc7236850_fb60f.png', 'image546c514622733'),
(15, 'image5465fc731fd28_e48bd.png', 'image546c515181d5c'),
(16, 'image5465fc73a396e_56b22.png', 'image546c515c397d2'),
(17, 'image54c777fda731d_0f18f.jpeg', 'image54d3b3ac85203'),
(18, 'image54c88e75a1480_c07c5.jpeg', 'image54d8eb017f28c'),
(19, 'image54bbc9fe8e7fc_9a06a.jpeg', 'image54d4a6539103c'),
(20, 'camminare_logo_original_97bfb.jpeg', 'image54ce82744a67e'),
(21, 'ajgroup_small_b50b3.jpeg', 'image54ce82744a24a'),
(24, 'image54ce7d848515a_581d7.jpeg', 'image54ce7d848515a'),
(25, 'automeistrelis_logo_b4994.jpeg', 'automeistrelis_logo'),
(26, 'image54d7f4855b819_17614.png', 'image54d7f4855b819'),
(27, NULL, 'image54d7f4855e7b9');

-- --------------------------------------------------------

--
-- Table structure for table `ContentPdf`
--

CREATE TABLE IF NOT EXISTS `ContentPdf` (
`id` int(11) NOT NULL,
  `path` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `CoverImage`
--

CREATE TABLE IF NOT EXISTS `CoverImage` (
`id` int(11) NOT NULL,
  `path` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `Donor`
--

CREATE TABLE IF NOT EXISTS `Donor` (
`id` int(11) NOT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `city` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `donationTime` datetime DEFAULT NULL,
  `bloodGroup` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bloodRh` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `EntityLogMessage`
--

CREATE TABLE IF NOT EXISTS `EntityLogMessage` (
`id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `text` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `entity` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `params` longtext COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `EventBlock`
--

CREATE TABLE IF NOT EXISTS `EventBlock` (
`id` int(11) NOT NULL,
  `cover_id` int(11) DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `content` longtext COLLATE utf8_unicode_ci NOT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  `tags` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `isPosted` tinyint(1) NOT NULL,
  `isMainPage` tinyint(1) NOT NULL,
  `eventTime` datetime DEFAULT NULL,
  `showEventDate` tinyint(1) NOT NULL,
  `shortText` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `EventBlock`
--

INSERT INTO `EventBlock` (`id`, `cover_id`, `title`, `content`, `createdAt`, `updatedAt`, `tags`, `isPosted`, `isMainPage`, `eventTime`, `showEventDate`, `shortText`) VALUES
(1, 4, 'Nauji žvejų batai ir kombinezonai PREMIUM', '<p style="text-align: justify;">UAB "PALIŪTIS" trejus metus sėkmingai vykdo bendrą projektą su Lenkijos žvejų batų ir bridkelnių gamintojais "PROS". Suderinus specialią receptūrą ir spalvą, mes gaminame auk&scaron;tos kokybes batus mod 900P ir 400P. Lenkų firma prilydo vir&scaron;utinę dalį ir gaunamas galutinis produktas: žveju batai ir bridkelnės, kurie sulig kiekvienais metais vis sėkmingiau parduodami ne tik Lenkijoje, bet ir Lietuvoje, Latvijoje, Estijoje, Vokietijoje, Italijoje, Čekijoje, Rumunijoje.</p>\r\n<p style="text-align: justify;">UAB "PALIŪTIS" - "PROS" atstovai Lietuvoje, pristato 2010 metų "PROS"</p>\r\n<p style="text-align: justify;">naujienas: "PREMIUM" serijos žvejų batus ir kombinezonus. &Scaron;ie gaminiai i&scaron;siskiria itin kokybi&scaron;komis medžiagomis, nauja spalvine gama ir dizainu.</p>\r\n<p style="text-align: justify;">Žvejų batų ir bridkelnių vir&scaron;utinė dalis pagaminta i&scaron; ypatingai atsparaus mechaniniams poveikiams, neperpučiamo ir neper&scaron;lampamo atestuoto "PLAVITEX"</p>\r\n<p style="text-align: justify;">PVC audinio. Visi "PREMIUM" serijos gaminiai turi papildomą sustiprinimą ties keliais. Tai gerokai sumažina pradūrimo galimybes klūpant ant kelių ir dedant masalus ar ruo&scaron;iant įrangą. Patys batai platūs ir patogūs, lengvai užsimaunami ir nuimami. Gilus rantuotas padas gerai sukimba su pavirsiumi ir ženkliai sumažina paslydimo riziką. Storasienės aulo sienelės, bei storas padas puikiai apsaugo tiek koją, tiek ir patį gaminį nuo pradūrimo.</p>\r\n<p style="text-align: justify;">"PREMIUM" serijos gaminiai - oficiali Lietuvos karpininkų rinktinės, dalyvausiančios &scaron;ių metų pasaulio pirmenybėse Anglijoje aprangos dalis.</p>\r\n<p style="text-align: justify;">Daugiau apie turimus guminius batus galite sužinoti mūsų tinklalapyje:</p>\r\n<p style="text-align: justify;"><a href="http://www.paliutis.lt/">www.paliutis.lt</a></p>', '2014-11-03 22:40:57', '2014-11-17 17:51:53', NULL, 1, 1, NULL, 0, NULL),
(2, 5, 'Pradėtas vykdyti "UAB PALIŪTIS eksporto plėtra Vokietijos rinkoje" projektas, dalyvavimas GDS parodoje', '<p style="text-align: justify;">UAB PALIŪTIS dalyvavęs ir gavęs V&scaron;Į Lietuvos verslo paramos agentūros, dalinai Europos Sąjungos finansuojamą konkursą &ndash; projektas &bdquo;UAB PALIŪTIS eksporto plėtra Vokietijos rinkoje&ldquo;, 2010 m. kovo 12 - 14 dienomis dalyvavo ir reprezentavo savo produkciją Diuseldorfe (Vokietijoje) vykusioje Global Shoes ir GDS avalynės mugėje.</p>\r\n<p style="text-align: justify;">Susipažinti su 2010/11 metų sezono naujovėmis į parodą užsuko apie 30.000 avalynės srities specialistų i&scaron; viso pasaulio. Devyniuose dideliuose paviljonuose buvo apie 1120 dalyvių i&scaron; 40 &scaron;alių. (GDS informacija)</p>\r\n<p style="text-align: justify;">Dalyviai su dideliu džiaugsmu ir optimizmu pastebėjo &scaron;iemet i&scaron;augusią guminės avalynės, kaip svarbiausio aprangos ir stiliaus elemento, paklausą.</p>\r\n<p style="text-align: justify;">UAB PALIŪTIS parodos metu pristatė savo naują produkciją, užmezgė tiesioginius kontaktus su įvairių &scaron;alių didmeninių ir mažmeninių įmonių atstovais, aptarė bendradarbiavimo sąlygas ir terminus, susitiko su senais partneriais, i&scaron;samiau susipažino su europieti&scaron;kos kokybės ir dizaino reikalavimais.</p>\r\n<p style="text-align: justify;">Mums, kaip dalyviams &scaron;i paroda &ndash; dar vienas laiptelis gaminant europinio lygio produkciją pagal standartus, ją sėkmingai realizuojant ne tik Lietuvos, bet ir besiplečiančioje Europos rinkoje.</p>', '2014-11-03 22:41:36', '2014-11-17 17:51:53', NULL, 1, 1, NULL, 1, NULL),
(3, 17, 'Dalyvavimas tarptautinėje parodoje', '<p>&Scaron;.m. vasario mėn. 14 - 18 d. Paliūtis UAB dalyvaus tarptautinėje avalynės parodoje MICAM , kuri vyks Milane (Italijoje). &nbsp;Paliūtis stendas bus įsikūręs 7 salėje. Stendo numeris L28. Daugiau informacijos apie parodą&nbsp;<a href="http://www.micamonline.com">www.micamonline.com</a></p>', '2014-11-16 20:42:46', '2015-01-27 17:10:46', NULL, 1, 1, '2015-01-27 00:00:00', 1, NULL),
(4, 18, 'NAUJIENA!!! Naujas vaikiškų batų modelis 130P', '<p style="text-align: justify;">PALIŪTIS UAB Jums pristato naują savo gaminį, vaiki&scaron;kų batų modelį 130P. Naujo i&scaron;skirtinio dizaino, kokybi&scaron;ki, patogūs, nepralaidūs vandeniui, pagaminti i&scaron; PVC.</p>\r\n<p style="text-align: justify;">Batų dydžiai: 27 - 39.</p>\r\n<p style="text-align: justify;">&Scaron;io modelio batus jau pamėgo Anglijos rinka, tikimės jie bus mėgstami ir Lietuvoje.</p>\r\n<p style="text-align: justify;">&nbsp;</p>', '2014-11-17 17:49:42', '2015-02-09 19:14:51', NULL, 1, 1, '2014-12-30 00:00:00', 1, NULL),
(5, 19, 'Paliūtis UAB tęsia tradicijas ir kasmet dalyvauja tarptautinėse parodose.', '<p>Paliūtis UAB tęsia tradicijas ir kasmet dalyvauja tarptautinėse parodose.</p>\r\n<p>&Scaron;iais metais, sausio mėnesį įmonė su savo gaminta produkcija dalyvavo tarptautinėje batų parodoje "Exporivaschuh" (Riva del Garda, Italija).</p>\r\n<p>Parodos metu naujus lankytojus supažindinome su savo veikla, pristatėme naujus produktus, užmezgėme naudingus kontaktus, susitikome su senais partneriais, susipažinome su mados tendencijomis, kurios vyraus ateityje.</p>', '2015-01-18 17:04:06', '2015-02-06 13:32:41', NULL, 1, 1, '2015-01-06 00:00:00', 1, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `ext_log_entries`
--

CREATE TABLE IF NOT EXISTS `ext_log_entries` (
`id` int(11) NOT NULL,
  `action` varchar(8) COLLATE utf8_unicode_ci NOT NULL,
  `logged_at` datetime NOT NULL,
  `object_id` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `object_class` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `version` int(11) NOT NULL,
  `data` longtext COLLATE utf8_unicode_ci COMMENT '(DC2Type:array)',
  `username` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=66 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `ext_log_entries`
--

INSERT INTO `ext_log_entries` (`id`, `action`, `logged_at`, `object_id`, `object_class`, `version`, `data`, `username`) VALUES
(1, 'update', '2014-11-02 21:34:32', '1', 'VswSystem\\CmsBundle\\Entity\\TextContentPage', 1, 'a:1:{s:5:"title";s:5:"test1";}', 'cawa'),
(2, 'create', '2014-11-02 22:10:42', '2', 'VswSystem\\CmsBundle\\Entity\\Menu', 1, 'a:1:{s:4:"name";s:7:"testSub";}', 'cawa'),
(6, 'create', '2014-11-03 11:33:15', '3', 'VswSystem\\CmsBundle\\Entity\\Menu', 1, 'a:1:{s:4:"name";s:10:"gallerySub";}', 'cawa'),
(8, 'create', '2014-11-03 11:40:02', '1', 'VswSystem\\CmsBundle\\Entity\\GalleryContentPage', 1, 'a:2:{s:5:"title";s:10:"Визаж";s:5:"alias";a:1:{s:2:"id";i:3;}}', 'cawa'),
(9, 'create', '2014-11-03 22:01:52', '2', 'VswSystem\\CmsBundle\\Entity\\TextContentPage', 1, 'a:2:{s:5:"title";s:5:"test2";s:5:"alias";a:1:{s:2:"id";i:5;}}', 'cawa'),
(10, 'create', '2014-11-03 22:40:57', '1', 'VswSystem\\CmsBundle\\Entity\\NewsContentPage', 1, 'a:2:{s:5:"title";s:4:"News";s:5:"alias";a:1:{s:2:"id";i:6;}}', 'cawa'),
(11, 'update', '2014-11-04 08:36:02', '1', 'VswSystem\\CmsBundle\\Entity\\TextContentPage', 2, 'a:1:{s:5:"title";s:4:"test";}', 'cawa'),
(12, 'create', '2014-11-04 11:07:50', '1', 'VswSystem\\CatalogBundle\\Entity\\CatalogItem', 1, 'a:2:{s:5:"alias";a:1:{s:2:"id";i:7;}s:8:"category";a:1:{s:2:"id";i:1;}}', 'cawa'),
(13, 'create', '2014-11-04 12:26:56', '2', 'VswSystem\\CatalogBundle\\Entity\\Category', 1, 'N;', 'cawa'),
(14, 'create', '2014-11-04 12:42:13', '3', 'VswSystem\\CatalogBundle\\Entity\\Category', 1, 'N;', 'cawa'),
(15, 'create', '2014-11-04 12:42:28', '4', 'VswSystem\\CatalogBundle\\Entity\\Category', 1, 'N;', 'cawa'),
(16, 'remove', '2014-11-04 12:43:24', '4', 'VswSystem\\CatalogBundle\\Entity\\Category', 2, 'N;', 'cawa'),
(17, 'create', '2014-11-04 12:43:46', '5', 'VswSystem\\CatalogBundle\\Entity\\Category', 1, 'N;', 'cawa'),
(18, 'create', '2014-11-04 12:44:12', '6', 'VswSystem\\CatalogBundle\\Entity\\Category', 1, 'N;', 'cawa'),
(19, 'create', '2014-11-04 12:54:49', '2', 'VswSystem\\CatalogBundle\\Entity\\CatalogItem', 1, 'a:2:{s:5:"alias";a:1:{s:2:"id";i:8;}s:8:"category";a:1:{s:2:"id";i:2;}}', 'cawa'),
(20, 'create', '2014-11-13 16:41:18', '3', 'VswSystem\\CmsBundle\\Entity\\TextContentPage', 1, 'a:2:{s:5:"title";s:8:"Apie mus";s:5:"alias";a:1:{s:2:"id";i:9;}}', 'cawa'),
(21, 'create', '2014-11-13 16:41:42', '4', 'VswSystem\\CmsBundle\\Entity\\TextContentPage', 1, 'a:2:{s:5:"title";s:8:"Apie mus";s:5:"alias";a:1:{s:2:"id";i:10;}}', 'cawa'),
(22, 'create', '2014-11-13 16:42:08', '5', 'VswSystem\\CmsBundle\\Entity\\TextContentPage', 1, 'a:2:{s:5:"title";s:8:"Apie mus";s:5:"alias";a:1:{s:2:"id";i:11;}}', 'cawa'),
(23, 'create', '2014-11-13 16:43:03', '6', 'VswSystem\\CmsBundle\\Entity\\TextContentPage', 1, 'a:2:{s:5:"title";s:8:"apie_mus";s:5:"alias";a:1:{s:2:"id";i:12;}}', 'cawa'),
(24, 'create', '2014-11-13 16:43:37', '7', 'VswSystem\\CmsBundle\\Entity\\TextContentPage', 1, 'a:2:{s:5:"title";s:7:"apiemus";s:5:"alias";a:1:{s:2:"id";i:13;}}', 'cawa'),
(25, 'create', '2014-11-13 16:43:49', '8', 'VswSystem\\CmsBundle\\Entity\\TextContentPage', 1, 'a:2:{s:5:"title";s:1:"1";s:5:"alias";a:1:{s:2:"id";i:14;}}', 'cawa'),
(26, 'remove', '2014-11-13 16:44:07', '2', 'VswSystem\\CmsBundle\\Entity\\TextContentPage', 2, 'N;', 'cawa'),
(27, 'create', '2014-11-16 20:35:54', '9', 'VswSystem\\CmsBundle\\Entity\\TextContentPage', 1, 'a:2:{s:5:"title";s:9:"kontaktai";s:5:"alias";a:1:{s:2:"id";i:15;}}', 'cawa'),
(28, 'create', '2014-11-16 20:42:46', '2', 'VswSystem\\CmsBundle\\Entity\\NewsContentPage', 1, 'a:2:{s:5:"title";s:9:"naujienos";s:5:"alias";a:1:{s:2:"id";i:16;}}', 'cawa'),
(29, 'create', '2014-11-16 20:43:47', '10', 'VswSystem\\CmsBundle\\Entity\\TextContentPage', 1, 'a:2:{s:5:"title";s:8:"apie_mus";s:5:"alias";a:1:{s:2:"id";i:17;}}', 'cawa'),
(30, 'update', '2014-11-17 09:14:22', '10', 'VswSystem\\CmsBundle\\Entity\\TextContentPage', 2, 'a:1:{s:5:"title";s:8:"Apie mus";}', 'cawa'),
(31, 'update', '2014-11-17 17:27:48', '2', 'VswSystem\\CmsBundle\\Entity\\NewsContentPage', 2, 'a:1:{s:5:"title";s:9:"Naujienos";}', 'cawa'),
(34, 'update', '2014-11-19 10:32:43', '1', 'VswSystem\\CatalogBundle\\Entity\\CatalogItem', 2, 'a:1:{s:8:"category";a:1:{s:2:"id";i:3;}}', 'cawa'),
(35, 'create', '2014-11-19 10:36:26', '5', 'VswSystem\\CatalogBundle\\Entity\\CatalogItem', 1, 'a:2:{s:5:"alias";a:1:{s:2:"id";i:20;}s:8:"category";a:1:{s:2:"id";i:6;}}', 'cawa'),
(37, 'update', '2014-11-19 10:39:10', '5', 'VswSystem\\CatalogBundle\\Entity\\CatalogItem', 2, 'a:1:{s:8:"category";a:1:{s:2:"id";i:1;}}', 'cawa'),
(39, 'update', '2014-11-19 10:41:52', '1', 'VswSystem\\CatalogBundle\\Entity\\CatalogItem', 3, 'a:1:{s:8:"category";a:1:{s:2:"id";i:1;}}', 'cawa'),
(40, 'update', '2015-01-17 22:25:07', '5', 'VswSystem\\CatalogBundle\\Entity\\CatalogItem', 3, 'a:1:{s:8:"category";a:1:{s:2:"id";i:5;}}', 'cawa'),
(41, 'create', '2015-01-18 12:57:53', '11', 'VswSystem\\CmsBundle\\Entity\\TextContentPage', 1, 'a:2:{s:5:"title";s:7:"auditai";s:5:"alias";a:1:{s:2:"id";i:21;}}', 'cawa'),
(42, 'create', '2015-01-18 14:28:44', '6', 'VswSystem\\CatalogBundle\\Entity\\CatalogItem', 1, 'a:2:{s:5:"alias";a:1:{s:2:"id";i:22;}s:8:"category";a:1:{s:2:"id";i:5;}}', 'cawa'),
(43, 'create', '2015-01-18 14:42:20', '7', 'VswSystem\\CatalogBundle\\Entity\\CatalogItem', 1, 'a:2:{s:5:"alias";a:1:{s:2:"id";i:23;}s:8:"category";a:1:{s:2:"id";i:3;}}', 'cawa'),
(44, 'create', '2015-01-18 14:47:53', '8', 'VswSystem\\CatalogBundle\\Entity\\CatalogItem', 1, 'a:2:{s:5:"alias";a:1:{s:2:"id";i:24;}s:8:"category";a:1:{s:2:"id";i:6;}}', 'cawa'),
(45, 'create', '2015-01-18 21:06:58', '12', 'VswSystem\\CmsBundle\\Entity\\TextContentPage', 1, 'a:2:{s:5:"title";s:6:"akcija";s:5:"alias";a:1:{s:2:"id";i:25;}}', 'cawa'),
(46, 'update', '2015-01-20 14:07:12', '9', 'VswSystem\\CmsBundle\\Entity\\TextContentPage', 2, 'a:1:{s:5:"title";s:9:"Kontaktai";}', 'Ilona'),
(47, 'update', '2015-01-20 14:43:19', '2', 'VswSystem\\CatalogBundle\\Entity\\CatalogItem', 2, 'a:1:{s:8:"category";a:1:{s:2:"id";i:1;}}', 'Ilona'),
(48, 'create', '2015-01-20 15:24:28', '9', 'VswSystem\\CatalogBundle\\Entity\\CatalogItem', 1, 'a:2:{s:5:"alias";a:1:{s:2:"id";i:26;}s:8:"category";a:1:{s:2:"id";i:1;}}', 'Ilona'),
(49, 'create', '2015-01-23 15:03:12', '10', 'VswSystem\\CatalogBundle\\Entity\\CatalogItem', 1, 'a:1:{s:5:"alias";a:1:{s:2:"id";i:27;}}', 'Ilona'),
(50, 'create', '2015-01-23 15:32:15', '11', 'VswSystem\\CatalogBundle\\Entity\\CatalogItem', 1, 'a:1:{s:5:"alias";a:1:{s:2:"id";i:28;}}', 'Ilona'),
(51, 'create', '2015-01-23 15:40:41', '12', 'VswSystem\\CatalogBundle\\Entity\\CatalogItem', 1, 'a:1:{s:5:"alias";a:1:{s:2:"id";i:29;}}', 'Ilona'),
(52, 'create', '2015-01-23 15:43:41', '13', 'VswSystem\\CatalogBundle\\Entity\\CatalogItem', 1, 'a:1:{s:5:"alias";a:1:{s:2:"id";i:30;}}', 'Ilona'),
(53, 'create', '2015-01-23 15:46:05', '14', 'VswSystem\\CatalogBundle\\Entity\\CatalogItem', 1, 'a:1:{s:5:"alias";a:1:{s:2:"id";i:31;}}', 'Ilona'),
(54, 'create', '2015-01-23 16:06:42', '15', 'VswSystem\\CatalogBundle\\Entity\\CatalogItem', 1, 'a:1:{s:5:"alias";a:1:{s:2:"id";i:32;}}', 'Ilona'),
(55, 'create', '2015-01-23 16:08:32', '16', 'VswSystem\\CatalogBundle\\Entity\\CatalogItem', 1, 'a:1:{s:5:"alias";a:1:{s:2:"id";i:33;}}', 'Ilona'),
(56, 'create', '2015-01-23 16:21:37', '17', 'VswSystem\\CatalogBundle\\Entity\\CatalogItem', 1, 'a:1:{s:5:"alias";a:1:{s:2:"id";i:34;}}', 'Ilona'),
(57, 'create', '2015-01-23 16:28:23', '18', 'VswSystem\\CatalogBundle\\Entity\\CatalogItem', 1, 'a:1:{s:5:"alias";a:1:{s:2:"id";i:35;}}', 'Ilona'),
(58, 'create', '2015-01-28 10:05:13', '7', 'VswSystem\\CatalogBundle\\Entity\\Category', 1, 'N;', 'GiedriusB'),
(59, 'remove', '2015-02-05 20:49:53', '7', 'VswSystem\\CatalogBundle\\Entity\\Category', 2, 'N;', 'cawa'),
(60, 'create', '2015-02-06 10:58:57', '19', 'VswSystem\\CatalogBundle\\Entity\\CatalogItem', 1, 'a:1:{s:5:"alias";a:1:{s:2:"id";i:36;}}', 'Ilona'),
(61, 'create', '2015-02-06 11:26:21', '20', 'VswSystem\\CatalogBundle\\Entity\\CatalogItem', 1, 'a:1:{s:5:"alias";a:1:{s:2:"id";i:37;}}', 'Ilona'),
(62, 'create', '2015-02-06 11:39:36', '21', 'VswSystem\\CatalogBundle\\Entity\\CatalogItem', 1, 'a:1:{s:5:"alias";a:1:{s:2:"id";i:38;}}', 'Ilona'),
(63, 'create', '2015-02-06 11:47:22', '22', 'VswSystem\\CatalogBundle\\Entity\\CatalogItem', 1, 'a:1:{s:5:"alias";a:1:{s:2:"id";i:39;}}', 'Ilona'),
(64, 'create', '2015-02-06 11:56:18', '23', 'VswSystem\\CatalogBundle\\Entity\\CatalogItem', 1, 'a:1:{s:5:"alias";a:1:{s:2:"id";i:40;}}', 'Ilona'),
(65, 'create', '2015-02-09 19:25:17', '4', 'VswSystem\\CmsBundle\\Entity\\Menu', 1, 'a:1:{s:4:"name";s:8:"footMenu";}', 'cawa');

-- --------------------------------------------------------

--
-- Table structure for table `ext_translations`
--

CREATE TABLE IF NOT EXISTS `ext_translations` (
`id` int(11) NOT NULL,
  `locale` varchar(8) COLLATE utf8_unicode_ci NOT NULL,
  `object_class` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `field` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `foreign_key` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8_unicode_ci
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `ext_translations`
--

INSERT INTO `ext_translations` (`id`, `locale`, `object_class`, `field`, `foreign_key`, `content`) VALUES
(1, 'ru', 'VswSystem\\CmsBundle\\Entity\\Navigation', 'title', '1', 'Тест'),
(2, 'ru', 'VswSystem\\CmsBundle\\Entity\\Navigation', 'subTitle', '1', 'тест'),
(3, 'ru', 'VswSystem\\CmsBundle\\Entity\\Navigation', 'title', '2', 'Подменю'),
(4, 'ru', 'VswSystem\\CmsBundle\\Entity\\Navigation', 'subTitle', '2', NULL),
(5, 'ru', 'VswSystem\\CmsBundle\\Entity\\Navigation', 'title', '3', 'Новости'),
(6, 'ru', 'VswSystem\\CmsBundle\\Entity\\Navigation', 'subTitle', '3', 'читать'),
(7, 'ru', 'VswSystem\\CmsBundle\\Entity\\Navigation', 'title', '4', 'Галлерея'),
(8, 'ru', 'VswSystem\\CmsBundle\\Entity\\Navigation', 'subTitle', '4', 'смотреть'),
(9, 'ru', 'VswSystem\\CmsBundle\\Entity\\Navigation', 'title', '5', 'Визаж'),
(10, 'ru', 'VswSystem\\CmsBundle\\Entity\\Navigation', 'subTitle', '5', NULL),
(11, 'ru', 'VswSystem\\CmsBundle\\Entity\\Navigation', 'title', '6', 'Главная'),
(12, 'ru', 'VswSystem\\CmsBundle\\Entity\\Navigation', 'subTitle', '6', 'начало'),
(13, 'en_US', 'VswSystem\\CmsBundle\\Entity\\EventBlock', 'title', '4', NULL),
(14, 'en_US', 'VswSystem\\CmsBundle\\Entity\\EventBlock', 'content', '4', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `FaqContentPage`
--

CREATE TABLE IF NOT EXISTS `FaqContentPage` (
`id` int(11) NOT NULL,
  `alias_id` int(11) DEFAULT NULL,
  `poll_id` int(11) DEFAULT NULL,
  `showImages` tinyint(1) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  `shareable` tinyint(1) NOT NULL,
  `tags` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `paginatable` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `faqcontentpage_block`
--

CREATE TABLE IF NOT EXISTS `faqcontentpage_block` (
  `faqcontentpage_id` int(11) NOT NULL,
  `block_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `faqcontentpage_questioncontent`
--

CREATE TABLE IF NOT EXISTS `faqcontentpage_questioncontent` (
  `faqcontentpage_id` int(11) NOT NULL,
  `questioncontent_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `GalleryContentPage`
--

CREATE TABLE IF NOT EXISTS `GalleryContentPage` (
`id` int(11) NOT NULL,
  `alias_id` int(11) DEFAULT NULL,
  `poll_id` int(11) DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  `shareable` tinyint(1) NOT NULL,
  `tags` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `GalleryContentPage`
--

INSERT INTO `GalleryContentPage` (`id`, `alias_id`, `poll_id`, `title`, `name`, `createdAt`, `updatedAt`, `shareable`, `tags`) VALUES
(1, 3, NULL, 'Визаж', 'Визаж', '2014-11-03 11:40:02', '2014-11-03 11:49:02', 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `gallerycontentpage_block`
--

CREATE TABLE IF NOT EXISTS `gallerycontentpage_block` (
  `gallerycontentpage_id` int(11) NOT NULL,
  `block_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `gallerycontentpage_galleryimagegroup`
--

CREATE TABLE IF NOT EXISTS `gallerycontentpage_galleryimagegroup` (
  `gallerycontentpage_id` int(11) NOT NULL,
  `galleryimagegroup_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `gallerycontentpage_galleryimagegroup`
--

INSERT INTO `gallerycontentpage_galleryimagegroup` (`gallerycontentpage_id`, `galleryimagegroup_id`) VALUES
(1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `GalleryImageGroup`
--

CREATE TABLE IF NOT EXISTS `GalleryImageGroup` (
`id` int(11) NOT NULL,
  `cover_id` int(11) DEFAULT NULL,
  `alias_id` int(11) DEFAULT NULL,
  `position` smallint(6) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `GalleryImageGroup`
--

INSERT INTO `GalleryImageGroup` (`id`, `cover_id`, `alias_id`, `position`, `name`, `title`) VALUES
(1, 3, 4, 0, 'Визаж', 'Визаж');

-- --------------------------------------------------------

--
-- Table structure for table `galleryimagegroup_gallerypageimage`
--

CREATE TABLE IF NOT EXISTS `galleryimagegroup_gallerypageimage` (
  `galleryimagegroup_id` int(11) NOT NULL,
  `gallerypageimage_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `galleryimagegroup_gallerypageimage`
--

INSERT INTO `galleryimagegroup_gallerypageimage` (`galleryimagegroup_id`, `gallerypageimage_id`) VALUES
(1, 1),
(1, 2);

-- --------------------------------------------------------

--
-- Table structure for table `GalleryPageImage`
--

CREATE TABLE IF NOT EXISTS `GalleryPageImage` (
`id` int(11) NOT NULL,
  `caption` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `path` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `GalleryPageImage`
--

INSERT INTO `GalleryPageImage` (`id`, `caption`, `path`, `name`) VALUES
(1, 'Photo1', 'photo1_3d6b5.png', 'photo1'),
(2, 'Photo2', 'photo2_adb28.png', 'photo2');

-- --------------------------------------------------------

--
-- Table structure for table `HallOfFameWidget`
--

CREATE TABLE IF NOT EXISTS `HallOfFameWidget` (
`id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `HallOfFameWidget`
--

INSERT INTO `HallOfFameWidget` (`id`, `name`) VALUES
(1, 'main');

-- --------------------------------------------------------

--
-- Table structure for table `halloffamewidget_partner`
--

CREATE TABLE IF NOT EXISTS `halloffamewidget_partner` (
  `halloffamewidget_id` int(11) NOT NULL,
  `partner_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `halloffamewidget_partner`
--

INSERT INTO `halloffamewidget_partner` (`halloffamewidget_id`, `partner_id`) VALUES
(1, 1),
(1, 2),
(1, 3);

-- --------------------------------------------------------

--
-- Table structure for table `ItemImage`
--

CREATE TABLE IF NOT EXISTS `ItemImage` (
`id` int(11) NOT NULL,
  `path` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `ItemImage`
--

INSERT INTO `ItemImage` (`id`, `path`, `name`) VALUES
(1, 'pagrindinė nuotrauka_e55a6.jpeg', 'pagrindinė nuotrauka'),
(2, 'kamufliažas/žalia_39f10.jpeg', 'kamufliažas/žalia'),
(4, 'pagrindinė nuotrauka_21371.jpeg', 'pagrindinė nuotrauka'),
(5, 'asdasd_c1d62.jpeg', 'asdasd'),
(6, 'asdasd_f7905.jpeg', 'asdasd'),
(7, 'padas_23629.jpeg', 'padas'),
(8, 'padas_229e8.jpeg', 'padas'),
(9, 'antras batas_7beb7.jpeg', 'antras batas'),
(10, 'antra_31439.jpeg', 'antra'),
(11, 'padas_a268d.jpeg', 'padas'),
(12, 'pagrindinė nuotrauka_2eca2.jpeg', 'pagrindinė nuotrauka'),
(13, 'vidus_ca73d.jpeg', 'vidus'),
(14, 'padas_144bc.jpeg', 'padas'),
(15, 'pagrindinė nuotrauka_bfdec.jpeg', 'pagrindinė nuotrauka'),
(16, 'antra_5da6e.jpeg', 'antra'),
(17, 'padas_d4396.jpeg', 'padas'),
(18, 'pagrindinė nuotrauka_e24b9.jpeg', 'pagrindinė nuotrauka'),
(19, 'žalias_5e851.jpeg', 'žalias'),
(20, 'padas_9ded5.jpeg', 'padas'),
(21, NULL, 'pilka/pilka'),
(22, NULL, 't.mėlyna/pilka'),
(23, NULL, 'akvamarinas/akvamarinas'),
(24, 'pagrindinė nuotrauka_61679.jpeg', 'pagrindinė nuotrauka'),
(25, NULL, 'mėlyna/mėlyna'),
(26, 'pagrindinė nuotrauka_dba67.jpeg', 'pagrindinė nuotrauka'),
(27, 'antra/zalias_00bbc.jpeg', 'antra/zalias'),
(28, 'padas_10b25.jpeg', 'padas'),
(29, 'pagrindinė nuotrauka_6c1b5.jpeg', 'pagrindinė nuotrauka'),
(30, 'aulas_b967f.jpeg', 'aulas'),
(31, 'padas_45d66.jpeg', 'padas'),
(32, 'pagrindinė nuotrauka_3af8f.jpeg', 'pagrindinė nuotrauka'),
(33, 'su užrašu_8f866.jpeg', 'su užrašu'),
(34, 'pagrindinė nuotrauka_f3800.jpeg', 'pagrindinė nuotrauka'),
(35, 'mėlyni spalvoti_c2a7f.jpeg', 'mėlyni spalvoti'),
(36, 'raudoni_taškeliais_a890d.jpeg', 'raudoni_taškeliais'),
(37, 'žali_krokodiliniai_72b44.jpeg', 'žali_krokodiliniai'),
(38, 'pagrindinė nuotrauka_8e651.jpeg', 'pagrindinė nuotrauka'),
(39, 'pagrindinė nuotrauka_a0397.jpeg', 'pagrindinė nuotrauka'),
(40, 'pagrindinė nuotrauka_eff19.jpeg', 'pagrindinė nuotrauka');

-- --------------------------------------------------------

--
-- Table structure for table `LayoutBlock`
--

CREATE TABLE IF NOT EXISTS `LayoutBlock` (
`id` int(11) NOT NULL,
  `contentType` smallint(6) NOT NULL,
  `text` longtext COLLATE utf8_unicode_ci,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  `contentBlock_id` int(11) DEFAULT NULL,
  `eventBlock_id` int(11) DEFAULT NULL,
  `cover_id` int(11) DEFAULT NULL,
  `swf_id` int(11) DEFAULT NULL,
  `url` longtext COLLATE utf8_unicode_ci
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `LayoutBlock`
--

INSERT INTO `LayoutBlock` (`id`, `contentType`, `text`, `name`, `title`, `createdAt`, `updatedAt`, `contentBlock_id`, `eventBlock_id`, `cover_id`, `swf_id`, `url`) VALUES
(1, 3, '<p>http://localhost:84/admin/layout/block/createhttp://localhost:84/admin/layout/block/createhttp://localhost:84/admin/layout/block/createhttp://localhost:84/admin/layout/block/create</p>', 'modal_ad', 'Modal', '2015-02-09 01:44:41', '2015-02-09 01:44:41', NULL, NULL, 26, 27, 'http://localhost:84/admin/layout/block/create');

-- --------------------------------------------------------

--
-- Table structure for table `Menu`
--

CREATE TABLE IF NOT EXISTS `Menu` (
`id` int(11) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `type` smallint(6) NOT NULL,
  `position` smallint(6) NOT NULL,
  `lft` int(11) NOT NULL,
  `lvl` int(11) NOT NULL,
  `rgt` int(11) NOT NULL,
  `root` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `connectorElement_id` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `Menu`
--

INSERT INTO `Menu` (`id`, `parent_id`, `type`, `position`, `lft`, `lvl`, `rgt`, `root`, `name`, `connectorElement_id`) VALUES
(1, NULL, 0, 4, 1, 0, 6, 1, 'mainMenu', NULL),
(2, 1, 1, 4, 2, 1, 3, 1, 'testSub', 1),
(3, 1, 0, 4, 4, 1, 5, 1, 'gallerySub', 4),
(4, NULL, 0, 4, 1, 0, 2, 4, 'footMenu', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `menu_navigation`
--

CREATE TABLE IF NOT EXISTS `menu_navigation` (
  `menu_id` int(11) NOT NULL,
  `navigation_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `menu_navigation`
--

INSERT INTO `menu_navigation` (`menu_id`, `navigation_id`) VALUES
(1, 1),
(1, 3),
(1, 4),
(1, 6),
(2, 2),
(3, 5);

-- --------------------------------------------------------

--
-- Table structure for table `MetaTags`
--

CREATE TABLE IF NOT EXISTS `MetaTags` (
`id` int(11) NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `keywords` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `author` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `Navigation`
--

CREATE TABLE IF NOT EXISTS `Navigation` (
`id` int(11) NOT NULL,
  `type` smallint(6) DEFAULT NULL,
  `route` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `position` int(11) DEFAULT NULL,
  `isHidden` tinyint(1) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `Navigation`
--

INSERT INTO `Navigation` (`id`, `type`, `route`, `title`, `position`, `isHidden`) VALUES
(1, 3, '/page/apie_mus', 'Apie mus', 0, 0),
(2, 3, '/test', 'Подменю', 2, 0),
(3, 3, '/page/naujienos', 'Naujienos', 17, 0),
(4, 3, '/page/kontaktai', 'Kontaktai', 27, 0),
(5, 3, '/page/make-up-page', 'Визаж', 11, 0),
(6, 3, '/catalog', 'Katalogas', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `NewsContentPage`
--

CREATE TABLE IF NOT EXISTS `NewsContentPage` (
`id` int(11) NOT NULL,
  `alias_id` int(11) DEFAULT NULL,
  `poll_id` int(11) DEFAULT NULL,
  `position` smallint(6) NOT NULL,
  `showImages` tinyint(1) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  `shareable` tinyint(1) NOT NULL,
  `tags` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `paginatable` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `NewsContentPage`
--

INSERT INTO `NewsContentPage` (`id`, `alias_id`, `poll_id`, `position`, `showImages`, `title`, `name`, `createdAt`, `updatedAt`, `shareable`, `tags`, `paginatable`) VALUES
(1, 6, NULL, 1, 1, 'News', 'News page', '2014-11-03 22:40:57', '2014-11-03 22:41:36', 1, NULL, 1),
(2, 16, NULL, 5, 1, 'Naujienos', 'Naujienos', '2014-11-16 20:42:46', '2015-01-23 13:29:44', 1, NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `newscontentpage_block`
--

CREATE TABLE IF NOT EXISTS `newscontentpage_block` (
  `newscontentpage_id` int(11) NOT NULL,
  `block_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `newscontentpage_eventblock`
--

CREATE TABLE IF NOT EXISTS `newscontentpage_eventblock` (
  `newscontentpage_id` int(11) NOT NULL,
  `eventblock_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `newscontentpage_eventblock`
--

INSERT INTO `newscontentpage_eventblock` (`newscontentpage_id`, `eventblock_id`) VALUES
(1, 1),
(1, 2),
(2, 3),
(2, 4),
(2, 5);

-- --------------------------------------------------------

--
-- Table structure for table `PageImage`
--

CREATE TABLE IF NOT EXISTS `PageImage` (
`id` int(11) NOT NULL,
  `path` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `PageImage`
--

INSERT INTO `PageImage` (`id`, `path`, `name`) VALUES
(1, 'asd_ff85d.png', 'asd'),
(2, 'asdasd_c1d1a.png', 'asdasd'),
(3, 'asdasd_4c33b.jpeg', 'asdasd');

-- --------------------------------------------------------

--
-- Table structure for table `Partner`
--

CREATE TABLE IF NOT EXISTS `Partner` (
`id` int(11) NOT NULL,
  `cover_id` int(11) DEFAULT NULL,
  `about` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `visible` tinyint(1) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  `link` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `Partner`
--

INSERT INTO `Partner` (`id`, `cover_id`, `about`, `visible`, `name`, `createdAt`, `updatedAt`, `link`) VALUES
(1, 6, 'Baldų gamyba, furnitūros pardavimas', 1, 'UAB Paliūčio baldai', '2014-11-04 08:48:55', '2015-02-01 21:20:21', 'http://www.paliuciobaldai.lt'),
(2, 7, 'Plastiko supirkimas ir perdirbimas', 1, 'UAB Regeneracija', '2014-11-04 08:48:55', '2015-02-01 21:24:30', 'http://www.regeneracija.lt'),
(3, 24, 'Liūtis', 1, 'UAB Liūtis', '2015-02-01 21:25:27', '2015-02-01 21:25:27', 'http://www.paliutis.lt');

-- --------------------------------------------------------

--
-- Table structure for table `PartnerLine`
--

CREATE TABLE IF NOT EXISTS `PartnerLine` (
`id` int(11) NOT NULL,
  `cover_id` int(11) DEFAULT NULL,
  `visible` tinyint(1) NOT NULL,
  `link` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `PartnerLine`
--

INSERT INTO `PartnerLine` (`id`, `cover_id`, `visible`, `link`, `createdAt`, `updatedAt`) VALUES
(1, 20, 1, 'http://www.camminare.pl', '2015-01-25 13:53:00', '2015-02-01 21:30:34'),
(2, 21, 1, 'http://www.ajgroup.pl', '2015-01-25 13:53:00', '2015-02-01 21:31:12'),
(3, 25, 1, 'http://www.automeistrelis.lt', '2015-02-01 21:46:33', '2015-02-01 21:46:33');

-- --------------------------------------------------------

--
-- Table structure for table `PartnerLineWidget`
--

CREATE TABLE IF NOT EXISTS `PartnerLineWidget` (
`id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `PartnerLineWidget`
--

INSERT INTO `PartnerLineWidget` (`id`, `name`) VALUES
(1, 'main');

-- --------------------------------------------------------

--
-- Table structure for table `partnerlinewidget_partnerline`
--

CREATE TABLE IF NOT EXISTS `partnerlinewidget_partnerline` (
  `partnerlinewidget_id` int(11) NOT NULL,
  `partnerline_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `partnerlinewidget_partnerline`
--

INSERT INTO `partnerlinewidget_partnerline` (`partnerlinewidget_id`, `partnerline_id`) VALUES
(1, 1),
(1, 2),
(1, 3);

-- --------------------------------------------------------

--
-- Table structure for table `Personnel`
--

CREATE TABLE IF NOT EXISTS `Personnel` (
`id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `duty` varchar(100) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `Poll`
--

CREATE TABLE IF NOT EXISTS `Poll` (
`id` int(11) NOT NULL,
  `question` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `Poll`
--

INSERT INTO `Poll` (`id`, `question`, `name`) VALUES
(1, 'What is your name?', 'NamePoll');

-- --------------------------------------------------------

--
-- Table structure for table `PollAnswer`
--

CREATE TABLE IF NOT EXISTS `PollAnswer` (
`id` int(11) NOT NULL,
  `poll_id` int(11) DEFAULT NULL,
  `answer` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `hits` smallint(6) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `PollAnswer`
--

INSERT INTO `PollAnswer` (`id`, `poll_id`, `answer`, `hits`) VALUES
(1, 1, 'Cawa', 1);

-- --------------------------------------------------------

--
-- Table structure for table `QuestionContent`
--

CREATE TABLE IF NOT EXISTS `QuestionContent` (
`id` int(11) NOT NULL,
  `image_id` int(11) DEFAULT NULL,
  `question` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `answer` longtext COLLATE utf8_unicode_ci NOT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `RefreshToken`
--

CREATE TABLE IF NOT EXISTS `RefreshToken` (
`id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `expires_at` int(11) DEFAULT NULL,
  `scope` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `SettingsGroup`
--

CREATE TABLE IF NOT EXISTS `SettingsGroup` (
`id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `SettingsValue`
--

CREATE TABLE IF NOT EXISTS `SettingsValue` (
`id` int(11) NOT NULL,
  `settingName` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `settingValue` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `defaultValue` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `SettingsValue`
--

INSERT INTO `SettingsValue` (`id`, `settingName`, `settingValue`, `defaultValue`) VALUES
(1, 'displayLine', '1', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `settings_groups`
--

CREATE TABLE IF NOT EXISTS `settings_groups` (
  `settingsgroup_id` int(11) NOT NULL,
  `settingsvalue_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `Slider`
--

CREATE TABLE IF NOT EXISTS `Slider` (
`id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `SliderImage`
--

CREATE TABLE IF NOT EXISTS `SliderImage` (
`id` int(11) NOT NULL,
  `caption` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `text` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `textColor` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `path` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `textPageLink_id` int(11) DEFAULT NULL,
  `route` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `slider_sliderimage`
--

CREATE TABLE IF NOT EXISTS `slider_sliderimage` (
  `slider_id` int(11) NOT NULL,
  `sliderimage_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `Subscriber`
--

CREATE TABLE IF NOT EXISTS `Subscriber` (
`id` int(11) NOT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `notified` tinyint(1) NOT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  `message` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `Subscriber`
--

INSERT INTO `Subscriber` (`id`, `email`, `name`, `notified`, `createdAt`, `updatedAt`, `message`) VALUES
(1, 'cawa123@mail.ru', 'paliutis.vsw.lt', 0, '2014-11-27 15:15:05', '2014-11-27 15:15:05', 'asdasdasdsad'),
(2, 'donoras.lietuvos@gmail.com', 'giedrius', 0, '2015-01-22 13:04:31', '2015-01-22 13:04:31', '                          Noriu donorams batų :)  '),
(3, 'donoras.lietuvos@gmail.com', 'giedrius', 0, '2015-01-22 13:04:34', '2015-01-22 13:04:34', '                          Noriu donorams batų :)  '),
(4, 'giedrius@blood.lt', 'Giedrius Bakučionis', 0, '2015-01-22 17:05:52', '2015-01-22 17:05:52', '                         Testas   '),
(5, 'giedrius@blood.lt', 'Giedrius Bakučionis', 0, '2015-01-22 17:08:46', '2015-01-22 17:08:46', '                   dar vienas testas         '),
(6, 'giedrius@mediz.lt', 'Giedrius Bakučionis', 0, '2015-01-23 15:15:48', '2015-01-23 15:15:48', 'Laba diena Sasha :)                            ');

-- --------------------------------------------------------

--
-- Table structure for table `TextContentPage`
--

CREATE TABLE IF NOT EXISTS `TextContentPage` (
`id` int(11) NOT NULL,
  `alias_id` int(11) DEFAULT NULL,
  `poll_id` int(11) DEFAULT NULL,
  `content` longtext COLLATE utf8_unicode_ci,
  `htmlContent` longtext COLLATE utf8_unicode_ci,
  `showImages` tinyint(1) NOT NULL,
  `position` smallint(6) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  `shareable` tinyint(1) NOT NULL,
  `tags` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `TextContentPage`
--

INSERT INTO `TextContentPage` (`id`, `alias_id`, `poll_id`, `content`, `htmlContent`, `showImages`, `position`, `title`, `name`, `createdAt`, `updatedAt`, `shareable`, `tags`) VALUES
(9, 15, NULL, '<table style="width: 100%; border-collapse: collapse;">\r\n<tbody>\r\n<tr>\r\n<td width="25%">\r\n<div><strong><span style="font-size: 12pt;">&ldquo;Paliūtis&rdquo; UAB</span></strong></div>\r\n</td>\r\n<td width="75%">\r\n<div>&nbsp;</div>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td>\r\n<div>Adresas</div>\r\n</td>\r\n<td>\r\n<div>Žirnių g. 6, LT-02120 Vilnius 30, Lietuva</div>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td>\r\n<div>Telefonas</div>\r\n</td>\r\n<td>\r\n<div>+370 5 215 22 65, +370 650 13333</div>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td>\r\n<div>Faksas</div>\r\n</td>\r\n<td>\r\n<div>+370 5 207 88 88</div>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td>\r\n<div>El. pa&scaron;tas</div>\r\n</td>\r\n<td>\r\n<div><a href="mailto:info@paliutis.lt">info@paliutis.lt</a></div>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<p>&nbsp;</p>\r\n<div>&nbsp;</div>\r\n<div>&nbsp;</div>\r\n<div>&nbsp;</div>\r\n<p>&nbsp;</p>\r\n<table style="width: 100%; border-collapse: collapse;">\r\n<tbody>\r\n<tr>\r\n<td>Pardavimų skyrius</td>\r\n<td>&nbsp;</td>\r\n<td><a href="mailto:uzsakymai@paliutis.lt;">uzsakymai@paliutis.lt</a>,&nbsp;+370 8 5 215 90 12;</td>\r\n</tr>\r\n<tr>\r\n<td>Prekybos direktorius</td>\r\n<td>&nbsp;</td>\r\n<td><a href="mailto:vilmantas@paliutis.lt;">vilmantas@paliutis.lt</a></td>\r\n</tr>\r\n<tr>\r\n<td>Eksporto direktorius</td>\r\n<td>&nbsp;</td>\r\n<td><a href="mailto:zilvinas@paliutis.lt;">zilvinas@paliutis.lt</a></td>\r\n</tr>\r\n<tr>\r\n<td>Buhalterija</td>\r\n<td>&nbsp;</td>\r\n<td>+370 8 5 215 22 64</td>\r\n</tr>\r\n<tr>\r\n<td>&nbsp;</td>\r\n<td>&nbsp;</td>\r\n<td>&nbsp;</td>\r\n</tr>\r\n<tr>\r\n<td>&nbsp;</td>\r\n<td>&nbsp;</td>\r\n<td>\r\n<div>&nbsp;</div>\r\n<div>&nbsp;</div>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<p>&nbsp;</p>', NULL, 0, 0, 'Kontaktai', 'Kontaktai', '2014-11-16 20:35:54', '2015-01-20 14:07:12', 0, NULL),
(10, 17, NULL, '<div style="text-align: justify;"><span style="font-size: 10pt;">Įmonė veikia nuo 1991, pavadinimu&nbsp;"Paliūtis" įregistruota 1995 metais&nbsp;ir yra privataus kapitalo įmonė.&nbsp;Per&nbsp;16 darbo metų įmonė&nbsp;smarkiai i&scaron;siplėtė - &scaron;iuo metu&nbsp;nuosavos gamybinės, sandėliavimo ir administracinės patalpos&nbsp;užima ~6000m<sup>2</sup>.&nbsp;</span></div>\r\n<div style="text-align: justify;"><span style="font-size: 10pt;">&nbsp;</span></div>\r\n<div style="text-align: justify;"><span style="font-size: 10pt;">Pagrindinė veiklos sritis - neper&scaron;lampamos avalynės (kitaip - guminė avalynė, guminiai batai, botai)&nbsp;&nbsp;&nbsp;ir laistymo&nbsp;žarnų&nbsp;i&scaron; PVC gamyba, taip pat&nbsp;pagal&nbsp; pirkėjo reikalavimus gaminame polimerinį plastikatą.&nbsp;Gamybai naudojamos ES aprobuotos žaliavos - mi&scaron;iniuose nėra &scaron;vino, kadmio, azo junginių.&nbsp;&nbsp;</span></div>\r\n<div style="text-align: justify;"><span style="font-size: 10pt;">&nbsp;</span></div>\r\n<div style="text-align: justify;"><span style="font-size: 10pt;">2007 gavus dalinę ES ir Lietuvos vyriausybės paramą įmonėje organizuotas beatliekinis žaliavų panaudojimo procesas - gamybinės atliekos po atitinkamo modifikavimo naujais italų firmos Bausano&amp;Figli&nbsp;&nbsp;įrengimais vėl panaudojamos gamyboje. </span></div>\r\n<div style="text-align: justify;"><span style="font-size: 10pt;">&nbsp;</span></div>\r\n<div style="text-align: justify;"><span style="font-size: 10pt;">Įmonės laboratorija glaudžiai bendradarbiauja su Asmeninių apsauginių priemonių (avalynės) atitikties įvertinimo centro Darbinės profesinės avalynės bandymų laboratorija&nbsp;Kaune, kuri aprobuoja gatavos avalynės atitikimą galiojantiems standartams. Pagal pirkėjų pageidavimus avalynė serifikuojama SATRA tecnologijų centre Anglijoje. &Scaron;iose laboratorijose atliekama pastovi ir pakartotina gaminamos produkcijos kokybės kontrolė. </span></div>\r\n<div style="text-align: justify;"><span style="font-size: 10pt;">&nbsp;</span></div>\r\n<div style="text-align: justify;"><span style="font-size: 10pt;"><span style="font-size: 10pt;">Bendradarbiaujame su didžiausiais prekybos tinklais, su&nbsp;įmonėmis&nbsp;prekiaujančiomis &nbsp;asmeninės apsaugos priemonėmis,&nbsp;su&nbsp;įmonėmis&nbsp;prekiaujančiomis&nbsp;sodo-daržo prekėmis, tačiau didžiąją dalį produkcijos parduodame&nbsp;kitose ES &scaron;alyse</span>.</span></div>', NULL, 0, 0, 'Apie mus', 'Apie mus', '2014-11-16 20:43:47', '2014-11-17 09:14:22', 0, NULL),
(11, 21, NULL, '<p>SATRA sertifikavimas</p>\r\n<p>Kažkoks auditas - rezultatai</p>\r\n<p>Dar vienas auditas - rezultatai</p>', NULL, 0, 0, 'auditai', 'Auditai', '2015-01-18 12:57:53', '2015-01-18 12:57:53', 0, NULL),
(12, 25, NULL, '<p>Batų 150P i&scaron;pardavimas.</p>\r\n<p>Kiekis - &nbsp;ne mažiau 50 vnt., kaina - 22 &euro;</p>\r\n<p>Kiekis - ne mažiau 100 vnt., kaina - 18 &euro;</p>\r\n<p>ir t.t. ir pan.</p>\r\n<p>kreiptis čia</p>', NULL, 0, 0, 'akcija', 'Akcija', '2015-01-18 21:06:58', '2015-01-18 21:06:58', 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `textcontentpage_block`
--

CREATE TABLE IF NOT EXISTS `textcontentpage_block` (
  `textcontentpage_id` int(11) NOT NULL,
  `block_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `textcontentpage_contentpdf`
--

CREATE TABLE IF NOT EXISTS `textcontentpage_contentpdf` (
  `textcontentpage_id` int(11) NOT NULL,
  `contentpdf_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `textcontentpage_pageimage`
--

CREATE TABLE IF NOT EXISTS `textcontentpage_pageimage` (
  `textcontentpage_id` int(11) NOT NULL,
  `pageimage_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `text_page_translations`
--

CREATE TABLE IF NOT EXISTS `text_page_translations` (
`id` int(11) NOT NULL,
  `object_id` int(11) DEFAULT NULL,
  `locale` varchar(8) COLLATE utf8_unicode_ci NOT NULL,
  `field` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `User`
--

CREATE TABLE IF NOT EXISTS `User` (
`id` int(11) NOT NULL,
  `profile_id` int(11) DEFAULT NULL,
  `username` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `role` int(11) NOT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `User`
--

INSERT INTO `User` (`id`, `profile_id`, `username`, `password`, `email`, `created`, `is_active`, `role`, `createdAt`, `updatedAt`) VALUES
(1, 1, 'cawa', '$2y$13$Pv2NiM9oE/lMPr9z9O24K.f9SOZr8aD7uG9ZCgsyNyFnid/Rf9aja', 'cawa123@mail.ru', '2014-10-05 14:32:41', 1, 3, '2014-10-05 14:32:41', '2014-10-05 14:32:41'),
(2, 2, 'Ilona', '$2y$13$L/wOKixw561XvCc0oHIRtuIF2CLLHGY0y8NtZdXspGj4GesIkS4I2', 'ilona@paliutis.lt', '2015-01-20 10:45:42', 1, 3, '2015-01-20 10:45:42', '2015-01-20 10:45:52'),
(3, 3, 'GiedriusB', '$2y$13$ugFjwPQaZUbTE2A53NRNpeNUt3Fj7Ei3bfenREDYT4/DQFBBmEpSO', 'giedrius@mediz.lt', '2015-01-22 17:06:42', 1, 3, '2015-01-22 17:06:42', '2015-01-22 17:06:53');

-- --------------------------------------------------------

--
-- Table structure for table `UserProfile`
--

CREATE TABLE IF NOT EXISTS `UserProfile` (
`id` int(11) NOT NULL,
  `avatar_id` int(11) DEFAULT NULL,
  `userPhone` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `surname` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bloodGroup` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bloodRh` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bloodAmount` smallint(6) DEFAULT NULL,
  `about` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `UserProfile`
--

INSERT INTO `UserProfile` (`id`, `avatar_id`, `userPhone`, `name`, `surname`, `bloodGroup`, `bloodRh`, `bloodAmount`, `about`, `createdAt`, `updatedAt`) VALUES
(1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2014-10-05 14:32:41', '2014-10-05 14:32:41'),
(2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2015-01-20 10:45:42', '2015-01-20 10:45:42'),
(3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2015-01-22 17:06:42', '2015-01-22 17:06:42');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `AccessToken`
--
ALTER TABLE `AccessToken`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `UNIQ_B39617F55F37A13B` (`token`), ADD KEY `IDX_B39617F519EB6921` (`client_id`), ADD KEY `IDX_B39617F5A76ED395` (`user_id`);

--
-- Indexes for table `acl_classes`
--
ALTER TABLE `acl_classes`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `UNIQ_69DD750638A36066` (`class_type`);

--
-- Indexes for table `acl_entries`
--
ALTER TABLE `acl_entries`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `UNIQ_46C8B806EA000B103D9AB4A64DEF17BCE4289BF4` (`class_id`,`object_identity_id`,`field_name`,`ace_order`), ADD KEY `IDX_46C8B806EA000B103D9AB4A6DF9183C9` (`class_id`,`object_identity_id`,`security_identity_id`), ADD KEY `IDX_46C8B806EA000B10` (`class_id`), ADD KEY `IDX_46C8B8063D9AB4A6` (`object_identity_id`), ADD KEY `IDX_46C8B806DF9183C9` (`security_identity_id`);

--
-- Indexes for table `acl_object_identities`
--
ALTER TABLE `acl_object_identities`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `UNIQ_9407E5494B12AD6EA000B10` (`object_identifier`,`class_id`), ADD KEY `IDX_9407E54977FA751A` (`parent_object_identity_id`);

--
-- Indexes for table `acl_object_identity_ancestors`
--
ALTER TABLE `acl_object_identity_ancestors`
 ADD PRIMARY KEY (`object_identity_id`,`ancestor_id`), ADD KEY `IDX_825DE2993D9AB4A6` (`object_identity_id`), ADD KEY `IDX_825DE299C671CEA1` (`ancestor_id`);

--
-- Indexes for table `acl_security_identities`
--
ALTER TABLE `acl_security_identities`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `UNIQ_8835EE78772E836AF85E0677` (`identifier`,`username`);

--
-- Indexes for table `Alias`
--
ALTER TABLE `Alias`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `AuthCode`
--
ALTER TABLE `AuthCode`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `UNIQ_F1D7D1775F37A13B` (`token`), ADD KEY `IDX_F1D7D17719EB6921` (`client_id`), ADD KEY `IDX_F1D7D177A76ED395` (`user_id`);

--
-- Indexes for table `Block`
--
ALTER TABLE `Block`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `Campaign`
--
ALTER TABLE `Campaign`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `UNIQ_E663708B176FE013` (`contactPerson_id`), ADD UNIQUE KEY `UNIQ_E663708BE09255E3` (`organizator_id`), ADD UNIQUE KEY `UNIQ_E663708BC3423909` (`driver_id`);

--
-- Indexes for table `campaign_personnel`
--
ALTER TABLE `campaign_personnel`
 ADD PRIMARY KEY (`campaign_id`,`personnel_id`), ADD KEY `IDX_6634EFA5F639F774` (`campaign_id`), ADD KEY `IDX_6634EFA51C109075` (`personnel_id`);

--
-- Indexes for table `CatalogItem`
--
ALTER TABLE `CatalogItem`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `UNIQ_ABCDF6CF5E564AE2` (`alias_id`);

--
-- Indexes for table `catalogitem_category`
--
ALTER TABLE `catalogitem_category`
 ADD PRIMARY KEY (`catalogitem_id`,`category_id`), ADD KEY `IDX_165896D5958AD94` (`catalogitem_id`), ADD KEY `IDX_165896D12469DE2` (`category_id`);

--
-- Indexes for table `catalogitem_itemimage`
--
ALTER TABLE `catalogitem_itemimage`
 ADD PRIMARY KEY (`catalogitem_id`,`itemimage_id`), ADD KEY `IDX_8F4FF6EC5958AD94` (`catalogitem_id`), ADD KEY `IDX_8F4FF6EC42B0E244` (`itemimage_id`);

--
-- Indexes for table `Category`
--
ALTER TABLE `Category`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `UNIQ_FF3A7B97922726E9` (`cover_id`), ADD UNIQUE KEY `UNIQ_FF3A7B9754B9D732` (`icon_id`);

--
-- Indexes for table `Client`
--
ALTER TABLE `Client`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ContentBlock`
--
ALTER TABLE `ContentBlock`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `UNIQ_CAACFE00922726E9` (`cover_id`);

--
-- Indexes for table `ContentImage`
--
ALTER TABLE `ContentImage`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ContentPdf`
--
ALTER TABLE `ContentPdf`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `CoverImage`
--
ALTER TABLE `CoverImage`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `Donor`
--
ALTER TABLE `Donor`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `EntityLogMessage`
--
ALTER TABLE `EntityLogMessage`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `UNIQ_F649D15EA76ED395` (`user_id`);

--
-- Indexes for table `EventBlock`
--
ALTER TABLE `EventBlock`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `UNIQ_4F698746922726E9` (`cover_id`);

--
-- Indexes for table `ext_log_entries`
--
ALTER TABLE `ext_log_entries`
 ADD PRIMARY KEY (`id`), ADD KEY `log_class_lookup_idx` (`object_class`), ADD KEY `log_date_lookup_idx` (`logged_at`), ADD KEY `log_user_lookup_idx` (`username`), ADD KEY `log_version_lookup_idx` (`object_id`,`object_class`,`version`);

--
-- Indexes for table `ext_translations`
--
ALTER TABLE `ext_translations`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `lookup_unique_idx` (`locale`,`object_class`,`field`,`foreign_key`), ADD KEY `translations_lookup_idx` (`locale`,`object_class`,`foreign_key`);

--
-- Indexes for table `FaqContentPage`
--
ALTER TABLE `FaqContentPage`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `UNIQ_A5E963EA5E564AE2` (`alias_id`), ADD UNIQUE KEY `UNIQ_A5E963EA3C947C0F` (`poll_id`);

--
-- Indexes for table `faqcontentpage_block`
--
ALTER TABLE `faqcontentpage_block`
 ADD PRIMARY KEY (`faqcontentpage_id`,`block_id`), ADD KEY `IDX_6FA8BEDF794881E5` (`faqcontentpage_id`), ADD KEY `IDX_6FA8BEDFE9ED820C` (`block_id`);

--
-- Indexes for table `faqcontentpage_questioncontent`
--
ALTER TABLE `faqcontentpage_questioncontent`
 ADD PRIMARY KEY (`faqcontentpage_id`,`questioncontent_id`), ADD KEY `IDX_5732D6D4794881E5` (`faqcontentpage_id`), ADD KEY `IDX_5732D6D4C0E9466B` (`questioncontent_id`);

--
-- Indexes for table `GalleryContentPage`
--
ALTER TABLE `GalleryContentPage`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `UNIQ_9BC560885E564AE2` (`alias_id`), ADD UNIQUE KEY `UNIQ_9BC560883C947C0F` (`poll_id`);

--
-- Indexes for table `gallerycontentpage_block`
--
ALTER TABLE `gallerycontentpage_block`
 ADD PRIMARY KEY (`gallerycontentpage_id`,`block_id`), ADD KEY `IDX_4D0F1C339285B697` (`gallerycontentpage_id`), ADD KEY `IDX_4D0F1C33E9ED820C` (`block_id`);

--
-- Indexes for table `gallerycontentpage_galleryimagegroup`
--
ALTER TABLE `gallerycontentpage_galleryimagegroup`
 ADD PRIMARY KEY (`gallerycontentpage_id`,`galleryimagegroup_id`), ADD KEY `IDX_4EF8728B9285B697` (`gallerycontentpage_id`), ADD KEY `IDX_4EF8728BD9B707AF` (`galleryimagegroup_id`);

--
-- Indexes for table `GalleryImageGroup`
--
ALTER TABLE `GalleryImageGroup`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `UNIQ_73FA86A5922726E9` (`cover_id`), ADD UNIQUE KEY `UNIQ_73FA86A55E564AE2` (`alias_id`);

--
-- Indexes for table `galleryimagegroup_gallerypageimage`
--
ALTER TABLE `galleryimagegroup_gallerypageimage`
 ADD PRIMARY KEY (`galleryimagegroup_id`,`gallerypageimage_id`), ADD KEY `IDX_47D2DCFD9B707AF` (`galleryimagegroup_id`), ADD KEY `IDX_47D2DCF43CD4BF` (`gallerypageimage_id`);

--
-- Indexes for table `GalleryPageImage`
--
ALTER TABLE `GalleryPageImage`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `HallOfFameWidget`
--
ALTER TABLE `HallOfFameWidget`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `halloffamewidget_partner`
--
ALTER TABLE `halloffamewidget_partner`
 ADD PRIMARY KEY (`halloffamewidget_id`,`partner_id`), ADD KEY `IDX_D1B9E1E0605B335` (`halloffamewidget_id`), ADD KEY `IDX_D1B9E1E09393F8FE` (`partner_id`);

--
-- Indexes for table `ItemImage`
--
ALTER TABLE `ItemImage`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `LayoutBlock`
--
ALTER TABLE `LayoutBlock`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `UNIQ_76206D73BBDBD894` (`contentBlock_id`), ADD UNIQUE KEY `UNIQ_76206D73E184A888` (`eventBlock_id`), ADD UNIQUE KEY `UNIQ_76206D73922726E9` (`cover_id`), ADD UNIQUE KEY `UNIQ_76206D73F0CB56EE` (`swf_id`);

--
-- Indexes for table `Menu`
--
ALTER TABLE `Menu`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `UNIQ_DD3795AD9C6C7337` (`connectorElement_id`), ADD KEY `IDX_DD3795AD727ACA70` (`parent_id`), ADD KEY `tree_idx` (`rgt`,`lft`,`root`), ADD KEY `lft_idx` (`lft`);

--
-- Indexes for table `menu_navigation`
--
ALTER TABLE `menu_navigation`
 ADD PRIMARY KEY (`menu_id`,`navigation_id`), ADD KEY `IDX_689771EDCCD7E912` (`menu_id`), ADD KEY `IDX_689771ED39F79D6D` (`navigation_id`);

--
-- Indexes for table `MetaTags`
--
ALTER TABLE `MetaTags`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `Navigation`
--
ALTER TABLE `Navigation`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `NewsContentPage`
--
ALTER TABLE `NewsContentPage`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `UNIQ_CDD5C0595E564AE2` (`alias_id`), ADD UNIQUE KEY `UNIQ_CDD5C0593C947C0F` (`poll_id`);

--
-- Indexes for table `newscontentpage_block`
--
ALTER TABLE `newscontentpage_block`
 ADD PRIMARY KEY (`newscontentpage_id`,`block_id`), ADD KEY `IDX_B57C921067F0D8A3` (`newscontentpage_id`), ADD KEY `IDX_B57C9210E9ED820C` (`block_id`);

--
-- Indexes for table `newscontentpage_eventblock`
--
ALTER TABLE `newscontentpage_eventblock`
 ADD PRIMARY KEY (`newscontentpage_id`,`eventblock_id`), ADD KEY `IDX_E94EFC9667F0D8A3` (`newscontentpage_id`), ADD KEY `IDX_E94EFC9618F2CADE` (`eventblock_id`);

--
-- Indexes for table `PageImage`
--
ALTER TABLE `PageImage`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `Partner`
--
ALTER TABLE `Partner`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `UNIQ_FE96078A922726E9` (`cover_id`);

--
-- Indexes for table `PartnerLine`
--
ALTER TABLE `PartnerLine`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `UNIQ_8B93A995922726E9` (`cover_id`);

--
-- Indexes for table `PartnerLineWidget`
--
ALTER TABLE `PartnerLineWidget`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `partnerlinewidget_partnerline`
--
ALTER TABLE `partnerlinewidget_partnerline`
 ADD PRIMARY KEY (`partnerlinewidget_id`,`partnerline_id`), ADD KEY `IDX_5F921A14CD3DED70` (`partnerlinewidget_id`), ADD KEY `IDX_5F921A1489833BBA` (`partnerline_id`);

--
-- Indexes for table `Personnel`
--
ALTER TABLE `Personnel`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `UNIQ_244D717DA76ED395` (`user_id`);

--
-- Indexes for table `Poll`
--
ALTER TABLE `Poll`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `PollAnswer`
--
ALTER TABLE `PollAnswer`
 ADD PRIMARY KEY (`id`), ADD KEY `IDX_7EA5DD9E3C947C0F` (`poll_id`);

--
-- Indexes for table `QuestionContent`
--
ALTER TABLE `QuestionContent`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `UNIQ_47D3FF203DA5256D` (`image_id`);

--
-- Indexes for table `RefreshToken`
--
ALTER TABLE `RefreshToken`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `UNIQ_7142379E5F37A13B` (`token`), ADD KEY `IDX_7142379E19EB6921` (`client_id`), ADD KEY `IDX_7142379EA76ED395` (`user_id`);

--
-- Indexes for table `SettingsGroup`
--
ALTER TABLE `SettingsGroup`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `SettingsValue`
--
ALTER TABLE `SettingsValue`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `settings_groups`
--
ALTER TABLE `settings_groups`
 ADD PRIMARY KEY (`settingsgroup_id`,`settingsvalue_id`), ADD KEY `IDX_FFD519023418900A` (`settingsgroup_id`), ADD KEY `IDX_FFD51902336CF2EF` (`settingsvalue_id`);

--
-- Indexes for table `Slider`
--
ALTER TABLE `Slider`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `SliderImage`
--
ALTER TABLE `SliderImage`
 ADD PRIMARY KEY (`id`), ADD KEY `IDX_B8DB64478DD15C45` (`textPageLink_id`);

--
-- Indexes for table `slider_sliderimage`
--
ALTER TABLE `slider_sliderimage`
 ADD PRIMARY KEY (`slider_id`,`sliderimage_id`), ADD KEY `IDX_8648826F2CCC9638` (`slider_id`), ADD KEY `IDX_8648826F3716E1BC` (`sliderimage_id`);

--
-- Indexes for table `Subscriber`
--
ALTER TABLE `Subscriber`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `TextContentPage`
--
ALTER TABLE `TextContentPage`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `UNIQ_BD501EBA5E564AE2` (`alias_id`), ADD UNIQUE KEY `UNIQ_BD501EBA3C947C0F` (`poll_id`);

--
-- Indexes for table `textcontentpage_block`
--
ALTER TABLE `textcontentpage_block`
 ADD PRIMARY KEY (`textcontentpage_id`,`block_id`), ADD KEY `IDX_9D1F145D4E3D2914` (`textcontentpage_id`), ADD KEY `IDX_9D1F145DE9ED820C` (`block_id`);

--
-- Indexes for table `textcontentpage_contentpdf`
--
ALTER TABLE `textcontentpage_contentpdf`
 ADD PRIMARY KEY (`textcontentpage_id`,`contentpdf_id`), ADD KEY `IDX_BB3744994E3D2914` (`textcontentpage_id`), ADD KEY `IDX_BB37449930E4715C` (`contentpdf_id`);

--
-- Indexes for table `textcontentpage_pageimage`
--
ALTER TABLE `textcontentpage_pageimage`
 ADD PRIMARY KEY (`textcontentpage_id`,`pageimage_id`), ADD KEY `IDX_519085EA4E3D2914` (`textcontentpage_id`), ADD KEY `IDX_519085EAB38D2F85` (`pageimage_id`);

--
-- Indexes for table `text_page_translations`
--
ALTER TABLE `text_page_translations`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `lookup_unique_idx` (`locale`,`object_id`,`field`), ADD KEY `IDX_E10E1893232D562B` (`object_id`);

--
-- Indexes for table `User`
--
ALTER TABLE `User`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `UNIQ_2DA17977F85E0677` (`username`), ADD UNIQUE KEY `UNIQ_2DA17977CCFA12B8` (`profile_id`);

--
-- Indexes for table `UserProfile`
--
ALTER TABLE `UserProfile`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `UNIQ_5417E0FA86383B10` (`avatar_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `AccessToken`
--
ALTER TABLE `AccessToken`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `acl_classes`
--
ALTER TABLE `acl_classes`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `acl_entries`
--
ALTER TABLE `acl_entries`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `acl_object_identities`
--
ALTER TABLE `acl_object_identities`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `acl_security_identities`
--
ALTER TABLE `acl_security_identities`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `Alias`
--
ALTER TABLE `Alias`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=41;
--
-- AUTO_INCREMENT for table `AuthCode`
--
ALTER TABLE `AuthCode`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `Block`
--
ALTER TABLE `Block`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `Campaign`
--
ALTER TABLE `Campaign`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `CatalogItem`
--
ALTER TABLE `CatalogItem`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT for table `Category`
--
ALTER TABLE `Category`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `Client`
--
ALTER TABLE `Client`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ContentBlock`
--
ALTER TABLE `ContentBlock`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `ContentImage`
--
ALTER TABLE `ContentImage`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT for table `ContentPdf`
--
ALTER TABLE `ContentPdf`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `CoverImage`
--
ALTER TABLE `CoverImage`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `Donor`
--
ALTER TABLE `Donor`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `EntityLogMessage`
--
ALTER TABLE `EntityLogMessage`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `EventBlock`
--
ALTER TABLE `EventBlock`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `ext_log_entries`
--
ALTER TABLE `ext_log_entries`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=66;
--
-- AUTO_INCREMENT for table `ext_translations`
--
ALTER TABLE `ext_translations`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `FaqContentPage`
--
ALTER TABLE `FaqContentPage`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `GalleryContentPage`
--
ALTER TABLE `GalleryContentPage`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `GalleryImageGroup`
--
ALTER TABLE `GalleryImageGroup`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `GalleryPageImage`
--
ALTER TABLE `GalleryPageImage`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `HallOfFameWidget`
--
ALTER TABLE `HallOfFameWidget`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `ItemImage`
--
ALTER TABLE `ItemImage`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=41;
--
-- AUTO_INCREMENT for table `LayoutBlock`
--
ALTER TABLE `LayoutBlock`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `Menu`
--
ALTER TABLE `Menu`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `MetaTags`
--
ALTER TABLE `MetaTags`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `Navigation`
--
ALTER TABLE `Navigation`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `NewsContentPage`
--
ALTER TABLE `NewsContentPage`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `PageImage`
--
ALTER TABLE `PageImage`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `Partner`
--
ALTER TABLE `Partner`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `PartnerLine`
--
ALTER TABLE `PartnerLine`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `PartnerLineWidget`
--
ALTER TABLE `PartnerLineWidget`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `Personnel`
--
ALTER TABLE `Personnel`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `Poll`
--
ALTER TABLE `Poll`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `PollAnswer`
--
ALTER TABLE `PollAnswer`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `QuestionContent`
--
ALTER TABLE `QuestionContent`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `RefreshToken`
--
ALTER TABLE `RefreshToken`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `SettingsGroup`
--
ALTER TABLE `SettingsGroup`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `SettingsValue`
--
ALTER TABLE `SettingsValue`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `Slider`
--
ALTER TABLE `Slider`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `SliderImage`
--
ALTER TABLE `SliderImage`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `Subscriber`
--
ALTER TABLE `Subscriber`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `TextContentPage`
--
ALTER TABLE `TextContentPage`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `text_page_translations`
--
ALTER TABLE `text_page_translations`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `User`
--
ALTER TABLE `User`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `UserProfile`
--
ALTER TABLE `UserProfile`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `acl_entries`
--
ALTER TABLE `acl_entries`
ADD CONSTRAINT `FK_46C8B8063D9AB4A6` FOREIGN KEY (`object_identity_id`) REFERENCES `acl_object_identities` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `FK_46C8B806DF9183C9` FOREIGN KEY (`security_identity_id`) REFERENCES `acl_security_identities` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `FK_46C8B806EA000B10` FOREIGN KEY (`class_id`) REFERENCES `acl_classes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `acl_object_identities`
--
ALTER TABLE `acl_object_identities`
ADD CONSTRAINT `FK_9407E54977FA751A` FOREIGN KEY (`parent_object_identity_id`) REFERENCES `acl_object_identities` (`id`);

--
-- Constraints for table `acl_object_identity_ancestors`
--
ALTER TABLE `acl_object_identity_ancestors`
ADD CONSTRAINT `FK_825DE2993D9AB4A6` FOREIGN KEY (`object_identity_id`) REFERENCES `acl_object_identities` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `FK_825DE299C671CEA1` FOREIGN KEY (`ancestor_id`) REFERENCES `acl_object_identities` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `CatalogItem`
--
ALTER TABLE `CatalogItem`
ADD CONSTRAINT `FK_ABCDF6CF5E564AE2` FOREIGN KEY (`alias_id`) REFERENCES `Alias` (`id`);

--
-- Constraints for table `catalogitem_category`
--
ALTER TABLE `catalogitem_category`
ADD CONSTRAINT `FK_165896D12469DE2` FOREIGN KEY (`category_id`) REFERENCES `Category` (`id`) ON DELETE CASCADE,
ADD CONSTRAINT `FK_165896D5958AD94` FOREIGN KEY (`catalogitem_id`) REFERENCES `CatalogItem` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `catalogitem_itemimage`
--
ALTER TABLE `catalogitem_itemimage`
ADD CONSTRAINT `FK_8F4FF6EC42B0E244` FOREIGN KEY (`itemimage_id`) REFERENCES `ItemImage` (`id`) ON DELETE CASCADE,
ADD CONSTRAINT `FK_8F4FF6EC5958AD94` FOREIGN KEY (`catalogitem_id`) REFERENCES `CatalogItem` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `Category`
--
ALTER TABLE `Category`
ADD CONSTRAINT `FK_FF3A7B9754B9D732` FOREIGN KEY (`icon_id`) REFERENCES `ContentImage` (`id`),
ADD CONSTRAINT `FK_FF3A7B97922726E9` FOREIGN KEY (`cover_id`) REFERENCES `ContentImage` (`id`);

--
-- Constraints for table `ContentBlock`
--
ALTER TABLE `ContentBlock`
ADD CONSTRAINT `FK_CAACFE00922726E9` FOREIGN KEY (`cover_id`) REFERENCES `ContentImage` (`id`);

--
-- Constraints for table `EntityLogMessage`
--
ALTER TABLE `EntityLogMessage`
ADD CONSTRAINT `FK_F649D15EA76ED395` FOREIGN KEY (`user_id`) REFERENCES `User` (`id`);

--
-- Constraints for table `EventBlock`
--
ALTER TABLE `EventBlock`
ADD CONSTRAINT `FK_4F698746922726E9` FOREIGN KEY (`cover_id`) REFERENCES `ContentImage` (`id`);

--
-- Constraints for table `FaqContentPage`
--
ALTER TABLE `FaqContentPage`
ADD CONSTRAINT `FK_A5E963EA3C947C0F` FOREIGN KEY (`poll_id`) REFERENCES `Poll` (`id`),
ADD CONSTRAINT `FK_A5E963EA5E564AE2` FOREIGN KEY (`alias_id`) REFERENCES `Alias` (`id`);

--
-- Constraints for table `faqcontentpage_block`
--
ALTER TABLE `faqcontentpage_block`
ADD CONSTRAINT `FK_6FA8BEDF794881E5` FOREIGN KEY (`faqcontentpage_id`) REFERENCES `FaqContentPage` (`id`) ON DELETE CASCADE,
ADD CONSTRAINT `FK_6FA8BEDFE9ED820C` FOREIGN KEY (`block_id`) REFERENCES `Block` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `faqcontentpage_questioncontent`
--
ALTER TABLE `faqcontentpage_questioncontent`
ADD CONSTRAINT `FK_5732D6D4794881E5` FOREIGN KEY (`faqcontentpage_id`) REFERENCES `FaqContentPage` (`id`) ON DELETE CASCADE,
ADD CONSTRAINT `FK_5732D6D4C0E9466B` FOREIGN KEY (`questioncontent_id`) REFERENCES `QuestionContent` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `GalleryContentPage`
--
ALTER TABLE `GalleryContentPage`
ADD CONSTRAINT `FK_9BC560883C947C0F` FOREIGN KEY (`poll_id`) REFERENCES `Poll` (`id`),
ADD CONSTRAINT `FK_9BC560885E564AE2` FOREIGN KEY (`alias_id`) REFERENCES `Alias` (`id`);

--
-- Constraints for table `gallerycontentpage_block`
--
ALTER TABLE `gallerycontentpage_block`
ADD CONSTRAINT `FK_4D0F1C339285B697` FOREIGN KEY (`gallerycontentpage_id`) REFERENCES `GalleryContentPage` (`id`) ON DELETE CASCADE,
ADD CONSTRAINT `FK_4D0F1C33E9ED820C` FOREIGN KEY (`block_id`) REFERENCES `Block` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `gallerycontentpage_galleryimagegroup`
--
ALTER TABLE `gallerycontentpage_galleryimagegroup`
ADD CONSTRAINT `FK_4EF8728B9285B697` FOREIGN KEY (`gallerycontentpage_id`) REFERENCES `GalleryContentPage` (`id`) ON DELETE CASCADE,
ADD CONSTRAINT `FK_4EF8728BD9B707AF` FOREIGN KEY (`galleryimagegroup_id`) REFERENCES `GalleryImageGroup` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `GalleryImageGroup`
--
ALTER TABLE `GalleryImageGroup`
ADD CONSTRAINT `FK_73FA86A55E564AE2` FOREIGN KEY (`alias_id`) REFERENCES `Alias` (`id`),
ADD CONSTRAINT `FK_73FA86A5922726E9` FOREIGN KEY (`cover_id`) REFERENCES `ContentImage` (`id`);

--
-- Constraints for table `galleryimagegroup_gallerypageimage`
--
ALTER TABLE `galleryimagegroup_gallerypageimage`
ADD CONSTRAINT `FK_47D2DCF43CD4BF` FOREIGN KEY (`gallerypageimage_id`) REFERENCES `GalleryPageImage` (`id`) ON DELETE CASCADE,
ADD CONSTRAINT `FK_47D2DCFD9B707AF` FOREIGN KEY (`galleryimagegroup_id`) REFERENCES `GalleryImageGroup` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `halloffamewidget_partner`
--
ALTER TABLE `halloffamewidget_partner`
ADD CONSTRAINT `FK_D1B9E1E0605B335` FOREIGN KEY (`halloffamewidget_id`) REFERENCES `HallOfFameWidget` (`id`) ON DELETE CASCADE,
ADD CONSTRAINT `FK_D1B9E1E09393F8FE` FOREIGN KEY (`partner_id`) REFERENCES `Partner` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `LayoutBlock`
--
ALTER TABLE `LayoutBlock`
ADD CONSTRAINT `FK_76206D73922726E9` FOREIGN KEY (`cover_id`) REFERENCES `ContentImage` (`id`),
ADD CONSTRAINT `FK_76206D73BBDBD894` FOREIGN KEY (`contentBlock_id`) REFERENCES `ContentBlock` (`id`),
ADD CONSTRAINT `FK_76206D73E184A888` FOREIGN KEY (`eventBlock_id`) REFERENCES `EventBlock` (`id`),
ADD CONSTRAINT `FK_76206D73F0CB56EE` FOREIGN KEY (`swf_id`) REFERENCES `ContentImage` (`id`);

--
-- Constraints for table `Menu`
--
ALTER TABLE `Menu`
ADD CONSTRAINT `FK_DD3795AD727ACA70` FOREIGN KEY (`parent_id`) REFERENCES `Menu` (`id`) ON DELETE CASCADE,
ADD CONSTRAINT `FK_DD3795AD9C6C7337` FOREIGN KEY (`connectorElement_id`) REFERENCES `Navigation` (`id`);

--
-- Constraints for table `menu_navigation`
--
ALTER TABLE `menu_navigation`
ADD CONSTRAINT `FK_689771ED39F79D6D` FOREIGN KEY (`navigation_id`) REFERENCES `Navigation` (`id`) ON DELETE CASCADE,
ADD CONSTRAINT `FK_689771EDCCD7E912` FOREIGN KEY (`menu_id`) REFERENCES `Menu` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `NewsContentPage`
--
ALTER TABLE `NewsContentPage`
ADD CONSTRAINT `FK_CDD5C0593C947C0F` FOREIGN KEY (`poll_id`) REFERENCES `Poll` (`id`),
ADD CONSTRAINT `FK_CDD5C0595E564AE2` FOREIGN KEY (`alias_id`) REFERENCES `Alias` (`id`);

--
-- Constraints for table `newscontentpage_block`
--
ALTER TABLE `newscontentpage_block`
ADD CONSTRAINT `FK_B57C921067F0D8A3` FOREIGN KEY (`newscontentpage_id`) REFERENCES `NewsContentPage` (`id`) ON DELETE CASCADE,
ADD CONSTRAINT `FK_B57C9210E9ED820C` FOREIGN KEY (`block_id`) REFERENCES `Block` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `newscontentpage_eventblock`
--
ALTER TABLE `newscontentpage_eventblock`
ADD CONSTRAINT `FK_E94EFC9618F2CADE` FOREIGN KEY (`eventblock_id`) REFERENCES `EventBlock` (`id`) ON DELETE CASCADE,
ADD CONSTRAINT `FK_E94EFC9667F0D8A3` FOREIGN KEY (`newscontentpage_id`) REFERENCES `NewsContentPage` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `Partner`
--
ALTER TABLE `Partner`
ADD CONSTRAINT `FK_FE96078A922726E9` FOREIGN KEY (`cover_id`) REFERENCES `ContentImage` (`id`);

--
-- Constraints for table `PartnerLine`
--
ALTER TABLE `PartnerLine`
ADD CONSTRAINT `FK_8B93A995922726E9` FOREIGN KEY (`cover_id`) REFERENCES `ContentImage` (`id`);

--
-- Constraints for table `partnerlinewidget_partnerline`
--
ALTER TABLE `partnerlinewidget_partnerline`
ADD CONSTRAINT `FK_5F921A1489833BBA` FOREIGN KEY (`partnerline_id`) REFERENCES `PartnerLine` (`id`) ON DELETE CASCADE,
ADD CONSTRAINT `FK_5F921A14CD3DED70` FOREIGN KEY (`partnerlinewidget_id`) REFERENCES `PartnerLineWidget` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `PollAnswer`
--
ALTER TABLE `PollAnswer`
ADD CONSTRAINT `FK_7EA5DD9E3C947C0F` FOREIGN KEY (`poll_id`) REFERENCES `Poll` (`id`);

--
-- Constraints for table `QuestionContent`
--
ALTER TABLE `QuestionContent`
ADD CONSTRAINT `FK_47D3FF203DA5256D` FOREIGN KEY (`image_id`) REFERENCES `ContentImage` (`id`);

--
-- Constraints for table `settings_groups`
--
ALTER TABLE `settings_groups`
ADD CONSTRAINT `FK_FFD51902336CF2EF` FOREIGN KEY (`settingsvalue_id`) REFERENCES `SettingsValue` (`id`) ON DELETE CASCADE,
ADD CONSTRAINT `FK_FFD519023418900A` FOREIGN KEY (`settingsgroup_id`) REFERENCES `SettingsGroup` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `SliderImage`
--
ALTER TABLE `SliderImage`
ADD CONSTRAINT `FK_B8DB64478DD15C45` FOREIGN KEY (`textPageLink_id`) REFERENCES `TextContentPage` (`id`);

--
-- Constraints for table `slider_sliderimage`
--
ALTER TABLE `slider_sliderimage`
ADD CONSTRAINT `FK_8648826F2CCC9638` FOREIGN KEY (`slider_id`) REFERENCES `Slider` (`id`) ON DELETE CASCADE,
ADD CONSTRAINT `FK_8648826F3716E1BC` FOREIGN KEY (`sliderimage_id`) REFERENCES `SliderImage` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `TextContentPage`
--
ALTER TABLE `TextContentPage`
ADD CONSTRAINT `FK_BD501EBA3C947C0F` FOREIGN KEY (`poll_id`) REFERENCES `Poll` (`id`),
ADD CONSTRAINT `FK_BD501EBA5E564AE2` FOREIGN KEY (`alias_id`) REFERENCES `Alias` (`id`);

--
-- Constraints for table `textcontentpage_block`
--
ALTER TABLE `textcontentpage_block`
ADD CONSTRAINT `FK_9D1F145D4E3D2914` FOREIGN KEY (`textcontentpage_id`) REFERENCES `TextContentPage` (`id`) ON DELETE CASCADE,
ADD CONSTRAINT `FK_9D1F145DE9ED820C` FOREIGN KEY (`block_id`) REFERENCES `Block` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `textcontentpage_contentpdf`
--
ALTER TABLE `textcontentpage_contentpdf`
ADD CONSTRAINT `FK_BB37449930E4715C` FOREIGN KEY (`contentpdf_id`) REFERENCES `ContentPdf` (`id`) ON DELETE CASCADE,
ADD CONSTRAINT `FK_BB3744994E3D2914` FOREIGN KEY (`textcontentpage_id`) REFERENCES `TextContentPage` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `textcontentpage_pageimage`
--
ALTER TABLE `textcontentpage_pageimage`
ADD CONSTRAINT `FK_519085EA4E3D2914` FOREIGN KEY (`textcontentpage_id`) REFERENCES `TextContentPage` (`id`) ON DELETE CASCADE,
ADD CONSTRAINT `FK_519085EAB38D2F85` FOREIGN KEY (`pageimage_id`) REFERENCES `PageImage` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `text_page_translations`
--
ALTER TABLE `text_page_translations`
ADD CONSTRAINT `FK_E10E1893232D562B` FOREIGN KEY (`object_id`) REFERENCES `TextContentPage` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `User`
--
ALTER TABLE `User`
ADD CONSTRAINT `FK_2DA17977CCFA12B8` FOREIGN KEY (`profile_id`) REFERENCES `UserProfile` (`id`);

--
-- Constraints for table `UserProfile`
--
ALTER TABLE `UserProfile`
ADD CONSTRAINT `FK_5417E0FA86383B10` FOREIGN KEY (`avatar_id`) REFERENCES `ContentImage` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
